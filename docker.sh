#!/bin/bash

container_registry=registry.gitlab.com

# docker image name without container registry and tagd
image_name=igagis/izdelie/build_image

# directory where this script resides
script_dir=$(dirname ${BASH_SOURCE[0]})

# concatenate all command line arguments to a single variable
args=""
while [[ $# > 0 ]] ; do
	args="$args $1"
	shift
done

if [ ! -z "$(ls /dev/ttyUSB0 2> /dev/null)" ]; then
	ttydev="--device=/dev/ttyUSB0"
else
	echo "info: no /dev/ttyUSB0 device found, not forwarding it to docker image"
fi

# check that python3 is installed
if ! python3 --version >> /dev/null 2>&1; then
	echo "error: python3 is not installed, the docker.sh script needs python3."
	exit 1
fi

# check that python3 yaml module is installed
if ! python3 -c "import yaml" >> /dev/null 2>&1; then
	echo "error: python3 yaml module is not installed, the docker.sh script needs python3."
	exit 1
fi

# get docker image tag from .gitlab-ci.yml file
image_tag=$(python3 -c "import yaml; print(yaml.load(open('$script_dir/.gitlab-ci.yml'), Loader=yaml.FullLoader)['variables']['image_tag'])")

image=$container_registry/$image_name:$image_tag

# check if image is available locally
if [ "$(docker image inspect $image > /dev/null 2>&1; echo $?)" -ne 0 ]; then
	echo "docker image is not found locally"
	echo "logging in to docker registry"

	# NOTE: the access token has only read_registry access
	echo xDsAc9Fz1ipY9QMGj4Ji | docker login $container_registry -u igagis --password-stdin
	
	docker pull $image
fi

# since we are working in mounted directory from the host system which is owned by host user we need to change to same user id and group id in docker container,
# thus use the `-u` option.
docker run -u $UID:`id -g` --rm -v $(pwd):/build -w /build -it $ttydev --group-add dialout --net=host --env="DISPLAY" --volume="$HOME/.Xauthority:/root/.Xauthority:rw" $image $args
