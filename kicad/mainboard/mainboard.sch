EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ESP8266
LIBS:pca9306
LIBS:MC34063A
LIBS:Type-C
LIBS:nixie_psu
LIBS:ams1117
LIBS:mainboard-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L GND #PWR01
U 1 1 5B5B8A59
P 5100 4500
F 0 "#PWR01" H 5100 4250 50  0001 C CNN
F 1 "GND" H 5100 4350 50  0000 C CNN
F 2 "" H 5100 4500 50  0000 C CNN
F 3 "" H 5100 4500 50  0000 C CNN
	1    5100 4500
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X06 P2
U 1 1 5B5B8E57
P 6200 3800
F 0 "P2" H 6200 4150 50  0000 C CNN
F 1 "CONN_01X06" V 6300 3800 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x06" H 6200 3800 50  0001 C CNN
F 3 "" H 6200 3800 50  0000 C CNN
	1    6200 3800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 5B5B8FC1
P 5950 4100
F 0 "#PWR02" H 5950 3850 50  0001 C CNN
F 1 "GND" H 5950 3950 50  0000 C CNN
F 2 "" H 5950 4100 50  0000 C CNN
F 3 "" H 5950 4100 50  0000 C CNN
	1    5950 4100
	1    0    0    -1  
$EndComp
Text Notes 6450 4150 1    60   ~ 0
to programmator
Text HLabel 2150 4250 0    60   Input ~ 0
sda_3.3v
Text HLabel 2150 4150 0    60   Input ~ 0
scl_3.3v
NoConn ~ 3250 3850
NoConn ~ 3250 4050
NoConn ~ 5050 4250
$Comp
L CONN_01X04 P4
U 1 1 5B5C984B
P 9450 3250
F 0 "P4" H 9450 3500 50  0000 C CNN
F 1 "display" V 9550 3250 50  0000 C CNN
F 2 "Connectors_JST:JST_SH_SM04B-SRSS-TB_04x1.00mm_Angled" H 9450 3250 50  0001 C CNN
F 3 "" H 9450 3250 50  0000 C CNN
	1    9450 3250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 5B5C9941
P 9200 3050
F 0 "#PWR03" H 9200 2800 50  0001 C CNN
F 1 "GND" H 9200 2900 50  0000 C CNN
F 2 "" H 9200 3050 50  0000 C CNN
F 3 "" H 9200 3050 50  0000 C CNN
	1    9200 3050
	1    0    0    1   
$EndComp
$Comp
L ESP-12F U1
U 1 1 5B5ED08B
P 4150 4050
F 0 "U1" H 4150 3950 50  0000 C CNN
F 1 "ESP-12F" H 4150 4150 50  0000 C CNN
F 2 "ESP8266:ESP-12E_SMD" H 4150 4050 50  0001 C CNN
F 3 "" H 4150 4050 50  0001 C CNN
	1    4150 4050
	1    0    0    -1  
$EndComp
NoConn ~ 4400 4950
NoConn ~ 4300 4950
NoConn ~ 4200 4950
NoConn ~ 4000 4950
NoConn ~ 3900 4950
NoConn ~ 4100 4950
$Comp
L +3.3V #PWR04
U 1 1 5B686788
P 4300 2950
F 0 "#PWR04" H 4300 2800 50  0001 C CNN
F 1 "+3.3V" H 4300 3090 50  0000 C CNN
F 2 "" H 4300 2950 50  0000 C CNN
F 3 "" H 4300 2950 50  0000 C CNN
	1    4300 2950
	0    1    1    0   
$EndComp
$Comp
L +3.3V #PWR05
U 1 1 5B686941
P 2550 4450
F 0 "#PWR05" H 2550 4300 50  0001 C CNN
F 1 "+3.3V" H 2550 4590 50  0000 C CNN
F 2 "" H 2550 4450 50  0000 C CNN
F 3 "" H 2550 4450 50  0000 C CNN
	1    2550 4450
	0    -1   -1   0   
$EndComp
$Comp
L +3.3V #PWR06
U 1 1 5B686A28
P 5950 3500
F 0 "#PWR06" H 5950 3350 50  0001 C CNN
F 1 "+3.3V" H 5950 3640 50  0000 C CNN
F 2 "" H 5950 3500 50  0000 C CNN
F 3 "" H 5950 3500 50  0000 C CNN
	1    5950 3500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR07
U 1 1 5C54D5EA
P 5500 4950
F 0 "#PWR07" H 5500 4700 50  0001 C CNN
F 1 "GND" H 5500 4800 50  0000 C CNN
F 2 "" H 5500 4950 50  0000 C CNN
F 3 "" H 5500 4950 50  0000 C CNN
	1    5500 4950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR08
U 1 1 5C54E03F
P 3150 5200
F 0 "#PWR08" H 3150 4950 50  0001 C CNN
F 1 "GND" H 3150 5050 50  0000 C CNN
F 2 "" H 3150 5200 50  0000 C CNN
F 3 "" H 3150 5200 50  0000 C CNN
	1    3150 5200
	1    0    0    -1  
$EndComp
Text Notes 3000 3350 0    60   ~ 0
Reset is pulled up to VCC inside ESP-12 with 10k resistor
$Comp
L AMS1117 U2
U 1 1 5C83100C
P 1750 1200
F 0 "U2" H 1750 1400 60  0000 C CNN
F 1 "AMS1117" H 1750 1300 60  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-223" H 1750 1050 60  0001 C CNN
F 3 "" H 1750 1050 60  0001 C CNN
	1    1750 1200
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR09
U 1 1 5C8310C1
P 1750 1900
F 0 "#PWR09" H 1750 1650 50  0001 C CNN
F 1 "GND" H 1750 1750 50  0000 C CNN
F 2 "" H 1750 1900 50  0000 C CNN
F 3 "" H 1750 1900 50  0000 C CNN
	1    1750 1900
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR010
U 1 1 5C8310F1
P 950 1200
F 0 "#PWR010" H 950 1050 50  0001 C CNN
F 1 "+5V" H 950 1340 50  0000 C CNN
F 2 "" H 950 1200 50  0000 C CNN
F 3 "" H 950 1200 50  0000 C CNN
	1    950  1200
	0    -1   -1   0   
$EndComp
$Comp
L +3.3V #PWR011
U 1 1 5C83112F
P 2550 1200
F 0 "#PWR011" H 2550 1050 50  0001 C CNN
F 1 "+3.3V" H 2550 1340 50  0000 C CNN
F 2 "" H 2550 1200 50  0000 C CNN
F 3 "" H 2550 1200 50  0000 C CNN
	1    2550 1200
	0    1    1    0   
$EndComp
$Comp
L HRO-TYPE-C-31-M-12 USB1
U 1 1 5C8CF8ED
P 4150 1700
F 0 "USB1" H 3950 2350 60  0000 C CNN
F 1 "HRO-TYPE-C-31-M-12" V 3750 1650 60  0000 C CNN
F 2 "USB_Type_C:HRO-TYPE-C-31-M-12" H 4150 1700 60  0001 C CNN
F 3 "" H 4150 1700 60  0001 C CNN
	1    4150 1700
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR012
U 1 1 5C8CFA21
P 3900 2400
F 0 "#PWR012" H 3900 2150 50  0001 C CNN
F 1 "GND" H 3900 2250 50  0000 C CNN
F 2 "" H 3900 2400 50  0000 C CNN
F 3 "" H 3900 2400 50  0000 C CNN
	1    3900 2400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR013
U 1 1 5C8CFAF5
P 3900 800
F 0 "#PWR013" H 3900 550 50  0001 C CNN
F 1 "GND" H 3900 650 50  0000 C CNN
F 2 "" H 3900 800 50  0000 C CNN
F 3 "" H 3900 800 50  0000 C CNN
	1    3900 800 
	-1   0    0    1   
$EndComp
$Comp
L +5V #PWR014
U 1 1 5C8CFBFB
P 3550 1700
F 0 "#PWR014" H 3550 1550 50  0001 C CNN
F 1 "+5V" H 3550 1840 50  0000 C CNN
F 2 "" H 3550 1700 50  0000 C CNN
F 3 "" H 3550 1700 50  0000 C CNN
	1    3550 1700
	0    -1   -1   0   
$EndComp
$Comp
L +3.3V #PWR015
U 1 1 5C8D01F2
P 9050 3150
F 0 "#PWR015" H 9050 3000 50  0001 C CNN
F 1 "+3.3V" H 9050 3290 50  0000 C CNN
F 2 "" H 9050 3150 50  0000 C CNN
F 3 "" H 9050 3150 50  0000 C CNN
	1    9050 3150
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X04 P1
U 1 1 5C8D028A
P 9450 4150
F 0 "P1" H 9450 4400 50  0000 C CNN
F 1 "light sensor" V 9550 4150 50  0000 C CNN
F 2 "Connectors_JST:JST_SH_SM04B-SRSS-TB_04x1.00mm_Angled" H 9450 4150 50  0001 C CNN
F 3 "" H 9450 4150 50  0000 C CNN
	1    9450 4150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR016
U 1 1 5C8D0291
P 9200 3950
F 0 "#PWR016" H 9200 3700 50  0001 C CNN
F 1 "GND" H 9200 3800 50  0000 C CNN
F 2 "" H 9200 3950 50  0000 C CNN
F 3 "" H 9200 3950 50  0000 C CNN
	1    9200 3950
	1    0    0    1   
$EndComp
$Comp
L +3.3V #PWR017
U 1 1 5C8D029F
P 9050 4050
F 0 "#PWR017" H 9050 3900 50  0001 C CNN
F 1 "+3.3V" H 9050 4190 50  0000 C CNN
F 2 "" H 9050 4050 50  0000 C CNN
F 3 "" H 9050 4050 50  0000 C CNN
	1    9050 4050
	1    0    0    -1  
$EndComp
Text HLabel 9100 3300 0    60   Input ~ 0
scl_3.3v
Text HLabel 9100 4200 0    60   Input ~ 0
scl_3.3v
Text HLabel 9100 3400 0    60   Input ~ 0
sda_3.3v
Text HLabel 9100 4300 0    60   Input ~ 0
sda_3.3v
$Comp
L CONN_01X04 P3
U 1 1 5C8D0F51
P 10200 5400
F 0 "P3" H 10200 5650 50  0000 C CNN
F 1 "rotary encoder" V 10300 5400 50  0000 C CNN
F 2 "Connectors_JST:JST_SH_SM04B-SRSS-TB_04x1.00mm_Angled" H 10200 5400 50  0001 C CNN
F 3 "" H 10200 5400 50  0000 C CNN
	1    10200 5400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR018
U 1 1 5C8D0F58
P 9800 4900
F 0 "#PWR018" H 9800 4650 50  0001 C CNN
F 1 "GND" H 9800 4750 50  0000 C CNN
F 2 "" H 9800 4900 50  0000 C CNN
F 3 "" H 9800 4900 50  0000 C CNN
	1    9800 4900
	1    0    0    1   
$EndComp
$Comp
L nixie_psu_1 U3
U 1 1 5C8D1F14
P 6550 1650
F 0 "U3" H 6600 1600 60  0000 C CNN
F 1 "nixie_psu_1" H 6600 2100 60  0000 C CNN
F 2 "nixie_PSUs:Nixie_PSU_1" H 6550 1650 60  0001 C CNN
F 3 "" H 6550 1650 60  0001 C CNN
	1    6550 1650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR019
U 1 1 5C8D1F7C
P 6600 2000
F 0 "#PWR019" H 6600 1750 50  0001 C CNN
F 1 "GND" H 6600 1850 50  0000 C CNN
F 2 "" H 6600 2000 50  0000 C CNN
F 3 "" H 6600 2000 50  0000 C CNN
	1    6600 2000
	1    0    0    -1  
$EndComp
NoConn ~ 6100 1550
$Comp
L +5V #PWR020
U 1 1 5C8D2084
P 5700 1350
F 0 "#PWR020" H 5700 1200 50  0001 C CNN
F 1 "+5V" H 5700 1490 50  0000 C CNN
F 2 "" H 5700 1350 50  0000 C CNN
F 3 "" H 5700 1350 50  0000 C CNN
	1    5700 1350
	0    -1   -1   0   
$EndComp
$Comp
L CONN_01X05 P5
U 1 1 5C8D212B
P 8500 1600
F 0 "P5" H 8500 1900 50  0000 C CNN
F 1 "nixie board" V 8600 1600 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x05" V 8850 1600 50  0000 C CNN
F 3 "" H 8500 1600 50  0000 C CNN
	1    8500 1600
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR021
U 1 1 5C8D2235
P 8200 1300
F 0 "#PWR021" H 8200 1150 50  0001 C CNN
F 1 "+3.3V" H 8200 1440 50  0000 C CNN
F 2 "" H 8200 1300 50  0000 C CNN
F 3 "" H 8200 1300 50  0000 C CNN
	1    8200 1300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR022
U 1 1 5C8D225E
P 8150 2000
F 0 "#PWR022" H 8150 1750 50  0001 C CNN
F 1 "GND" H 8150 1850 50  0000 C CNN
F 2 "" H 8150 2000 50  0000 C CNN
F 3 "" H 8150 2000 50  0000 C CNN
	1    8150 2000
	-1   0    0    -1  
$EndComp
Text HLabel 8150 1600 0    60   Input ~ 0
scl_3.3v
Text HLabel 8150 1500 0    60   Input ~ 0
sda_3.3v
$Comp
L R R1
U 1 1 5C8D18FC
P 2300 3650
F 0 "R1" V 2380 3650 50  0000 C CNN
F 1 "4.7k" V 2300 3650 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 2230 3650 50  0001 C CNN
F 3 "" H 2300 3650 50  0000 C CNN
	1    2300 3650
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 5C8D194B
P 2550 3650
F 0 "R2" V 2630 3650 50  0000 C CNN
F 1 "4.7k" V 2550 3650 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 2480 3650 50  0001 C CNN
F 3 "" H 2550 3650 50  0000 C CNN
	1    2550 3650
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 5CE13669
P 7700 5000
F 0 "C1" H 7725 5100 50  0000 L CNN
F 1 "100nF" H 7725 4900 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 7400 5550 50  0000 C CNN
F 3 "" H 7700 5000 50  0000 C CNN
	1    7700 5000
	1    0    0    -1  
$EndComp
$Comp
L C C3
U 1 1 5CE136CA
P 8100 5000
F 0 "C3" H 8125 5100 50  0000 L CNN
F 1 "100nF" H 8125 4900 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 7400 5650 50  0000 C CNN
F 3 "" H 8100 5000 50  0000 C CNN
	1    8100 5000
	1    0    0    -1  
$EndComp
$Comp
L C C4
U 1 1 5CE1371F
P 8550 5000
F 0 "C4" H 8575 5100 50  0000 L CNN
F 1 "100nF" H 8575 4900 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 7400 5450 50  0000 C CNN
F 3 "" H 8550 5000 50  0000 C CNN
	1    8550 5000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR023
U 1 1 5CE137D2
P 8100 4500
F 0 "#PWR023" H 8100 4250 50  0001 C CNN
F 1 "GND" H 8100 4350 50  0000 C CNN
F 2 "" H 8100 4500 50  0000 C CNN
F 3 "" H 8100 4500 50  0000 C CNN
	1    8100 4500
	1    0    0    1   
$EndComp
$Comp
L +3.3V #PWR024
U 1 1 5CE1434B
P 6600 4450
F 0 "#PWR024" H 6600 4300 50  0001 C CNN
F 1 "+3.3V" H 6600 4590 50  0000 C CNN
F 2 "" H 6600 4450 50  0000 C CNN
F 3 "" H 6600 4450 50  0000 C CNN
	1    6600 4450
	1    0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 5CE1441B
P 6600 5000
F 0 "R4" V 6680 5000 50  0000 C CNN
F 1 "10k" V 6600 5000 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 6250 4100 50  0000 C CNN
F 3 "" H 6600 5000 50  0000 C CNN
	1    6600 5000
	1    0    0    -1  
$EndComp
$Comp
L R R5
U 1 1 5CE145A0
P 6950 5000
F 0 "R5" V 7030 5000 50  0000 C CNN
F 1 "10k" V 6950 5000 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 6250 3900 50  0000 C CNN
F 3 "" H 6950 5000 50  0000 C CNN
	1    6950 5000
	1    0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 5CE145F9
P 6250 5000
F 0 "R3" V 6330 5000 50  0000 C CNN
F 1 "10k" V 6250 5000 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 6250 4000 50  0000 C CNN
F 3 "" H 6250 5000 50  0000 C CNN
	1    6250 5000
	1    0    0    -1  
$EndComp
$Comp
L C C5
U 1 1 5CE1CF0F
P 2700 4900
F 0 "C5" H 2725 5000 50  0000 L CNN
F 1 "100uF" H 2725 4800 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 2738 4750 50  0000 C CNN
F 3 "" H 2700 4900 50  0000 C CNN
	1    2700 4900
	1    0    0    -1  
$EndComp
$Comp
L R R6
U 1 1 5CE3EC25
P 9000 5250
F 0 "R6" V 9080 5250 50  0000 C CNN
F 1 "430" V 9000 5250 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 9700 4850 50  0000 C CNN
F 3 "" H 9000 5250 50  0000 C CNN
	1    9000 5250
	0    1    1    0   
$EndComp
$Comp
L R R8
U 1 1 5CE3ED52
P 9450 5450
F 0 "R8" V 9530 5450 50  0000 C CNN
F 1 "430" V 9450 5450 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 10150 5050 50  0000 C CNN
F 3 "" H 9450 5450 50  0000 C CNN
	1    9450 5450
	0    1    1    0   
$EndComp
$Comp
L R R7
U 1 1 5CE3EDB5
P 9400 5700
F 0 "R7" V 9480 5700 50  0000 C CNN
F 1 "430" V 9400 5700 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 10100 5300 50  0000 C CNN
F 3 "" H 9400 5700 50  0000 C CNN
	1    9400 5700
	0    1    1    0   
$EndComp
Wire Wire Line
	5500 3950 6000 3950
Wire Wire Line
	6000 4050 5950 4050
Wire Wire Line
	5950 4050 5950 4100
Wire Wire Line
	5500 3950 5500 4150
Wire Wire Line
	5500 4150 5050 4150
Wire Wire Line
	5950 3500 5950 3550
Wire Wire Line
	5950 3550 6000 3550
Wire Wire Line
	2900 3950 3250 3950
Wire Wire Line
	2150 4150 3250 4150
Wire Wire Line
	9200 3050 9200 3100
Wire Wire Line
	9200 3100 9250 3100
Wire Wire Line
	9100 3300 9250 3300
Wire Wire Line
	9100 3400 9250 3400
Wire Wire Line
	5050 4450 5100 4450
Wire Wire Line
	5100 4450 5100 4500
Wire Wire Line
	2550 4450 3250 4450
Wire Wire Line
	9050 3150 9050 3200
Wire Wire Line
	9050 3200 9250 3200
Wire Wire Line
	2300 2950 4300 2950
Wire Wire Line
	2900 2950 2900 3950
Wire Wire Line
	5500 4350 5500 4950
Wire Wire Line
	5500 4350 5050 4350
Wire Wire Line
	5050 3750 5250 3750
Wire Wire Line
	5250 3750 5250 3650
Wire Wire Line
	5250 3650 6000 3650
Wire Wire Line
	5050 3850 5350 3850
Wire Wire Line
	5350 3850 5350 3750
Wire Wire Line
	5350 3750 6000 3750
Wire Wire Line
	3250 3400 5700 3400
Wire Wire Line
	5700 3400 5700 3850
Wire Wire Line
	5700 3850 6000 3850
Wire Wire Line
	3250 3400 3250 3750
Wire Wire Line
	2150 4250 3250 4250
Wire Wire Line
	1750 1550 1750 1900
Wire Wire Line
	2250 1200 2550 1200
Wire Wire Line
	1250 1200 950  1200
Wire Wire Line
	3900 2400 3900 2250
Wire Wire Line
	3900 2250 4050 2250
Wire Wire Line
	3900 1150 4050 1150
Wire Wire Line
	3900 800  3900 1150
Wire Wire Line
	4050 1050 3900 1050
Connection ~ 3900 1050
Wire Wire Line
	4050 1250 3750 1250
Wire Wire Line
	3750 1250 3750 2150
Wire Wire Line
	3750 2150 4050 2150
Wire Wire Line
	3550 1700 3750 1700
Connection ~ 3750 1700
Wire Wire Line
	9200 3950 9200 4000
Wire Wire Line
	9200 4000 9250 4000
Wire Wire Line
	9100 4200 9250 4200
Wire Wire Line
	9100 4300 9250 4300
Wire Wire Line
	9050 4050 9050 4100
Wire Wire Line
	9050 4100 9250 4100
Wire Wire Line
	9800 4900 9800 5250
Wire Wire Line
	9800 5250 10000 5250
Wire Wire Line
	5800 5350 7900 5350
Wire Wire Line
	5800 5350 5800 4000
Wire Wire Line
	5800 4000 5350 4000
Wire Wire Line
	5350 4000 5350 3950
Wire Wire Line
	5350 3950 5050 3950
Wire Wire Line
	5050 4050 5250 4050
Wire Wire Line
	5250 4050 5250 4100
Wire Wire Line
	5250 4100 5750 4100
Wire Wire Line
	5750 4100 5750 5450
Wire Wire Line
	5750 5450 9300 5450
Wire Wire Line
	3000 4600 3450 4600
Wire Wire Line
	3450 4600 3450 5550
Wire Wire Line
	3450 5550 9000 5550
Wire Wire Line
	3250 4350 3000 4350
Wire Wire Line
	3000 4350 3000 4600
Wire Wire Line
	6100 1450 5900 1450
Wire Wire Line
	5900 1450 5900 1900
Wire Wire Line
	5900 1900 7300 1900
Wire Wire Line
	7300 1900 7300 1450
Wire Wire Line
	7300 1450 7100 1450
Wire Wire Line
	6600 2000 6600 1900
Connection ~ 6600 1900
Wire Wire Line
	5700 1350 6100 1350
Wire Wire Line
	8150 2000 8150 1700
Wire Wire Line
	8150 1700 8300 1700
Wire Wire Line
	8200 1300 8200 1400
Wire Wire Line
	8200 1400 8300 1400
Wire Wire Line
	8300 1800 7450 1800
Wire Wire Line
	7450 1800 7450 1350
Wire Wire Line
	7450 1350 7100 1350
Wire Wire Line
	8150 1500 8300 1500
Wire Wire Line
	8150 1600 8300 1600
Wire Wire Line
	2300 3500 2300 2950
Connection ~ 2900 2950
Wire Wire Line
	2550 3500 2550 2950
Connection ~ 2550 2950
Wire Wire Line
	2300 3800 2300 4150
Connection ~ 2300 4150
Wire Wire Line
	2550 3800 2550 4250
Connection ~ 2550 4250
Wire Wire Line
	7700 4850 7700 4650
Wire Wire Line
	7700 4650 8550 4650
Wire Wire Line
	8550 4650 8550 4850
Connection ~ 8100 4650
Wire Wire Line
	8100 4500 8100 4850
Wire Wire Line
	7700 5350 7700 5150
Connection ~ 7700 5350
Wire Wire Line
	8100 5450 8100 5150
Connection ~ 8100 5450
Wire Wire Line
	8550 5550 8550 5150
Connection ~ 8550 5550
Wire Wire Line
	6250 4850 6250 4650
Wire Wire Line
	6250 4650 6950 4650
Wire Wire Line
	6950 4650 6950 4850
Wire Wire Line
	6600 4450 6600 4850
Connection ~ 6600 4650
Wire Wire Line
	6250 5150 6250 5350
Connection ~ 6250 5350
Wire Wire Line
	6600 5150 6600 5450
Connection ~ 6600 5450
Wire Wire Line
	6950 5150 6950 5550
Connection ~ 6950 5550
Wire Wire Line
	2700 5050 2700 5150
Wire Wire Line
	2700 5150 3150 5150
Wire Wire Line
	2700 4750 2700 4450
Connection ~ 2700 4450
Wire Wire Line
	10000 5450 9600 5450
Wire Wire Line
	9550 5700 9800 5700
Wire Wire Line
	9800 5700 9800 5550
Wire Wire Line
	9800 5550 10000 5550
Wire Wire Line
	10000 5350 9650 5350
Wire Wire Line
	9650 5350 9650 5250
Wire Wire Line
	9650 5250 9150 5250
Wire Wire Line
	8850 5250 7900 5250
Wire Wire Line
	7900 5250 7900 5350
Wire Wire Line
	9000 5550 9000 5700
Wire Wire Line
	9000 5700 9250 5700
Wire Wire Line
	3150 5150 3150 5200
$EndSCHEMATC
