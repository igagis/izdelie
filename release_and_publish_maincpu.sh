#!/bin/bash

# we want exit immediately if any command fails and we want error in piped commands to be preserved
set -eo pipefail

# Make sure there are no uncommitted changes.
git diff-index --quiet HEAD -- || (echo "ERROR: there are uncommitted changes." && exit 1)

git pull

version_file=src/maincpu/firmware_version.txt

cur_version=$(cat $version_file)

new_version=$((cur_version+1))

echo "new_version = $new_version"

echo -n $new_version > $version_file

./docker.sh make re

git commit -am"Bump version of 'maincpu' ($cur_version -> $new_version)"
git push

echo "{\"version\":$new_version}" > version

echo "Copying files to server"
scp src/maincpu/build/maincpu_1/eagle.app.flash.bin version ivan@izdelie.icu:/srv/izdelie/firmware
