#include <iostream>
#include <fstream>
#include <map>
#include <iomanip>
#include <string>
#include <vector>

int main(int argc, const char** argv){
	unsigned charWidth = 0;
	unsigned charHeight = 0;
	std::string outputFilename;
	uint8_t startAsciiCode = 0;
	uint8_t endAsciiCode = 0xff;
	std::string font_file;

	// parse command line args
	for(unsigned i = 1; i != argc; ++i){
		auto arg = std::string(argv[i]);

		if(arg.find('-') != 0){
			if(!font_file.empty()){
				throw std::invalid_argument("more than one font file given");
			}
			font_file = arg;
			continue;
		}

		++i;
		if(i == argc){
			throw std::invalid_argument("too few arguments given");
		}

		auto val = std::string(argv[i]);

		if(arg == "-w"){ // character width
			charWidth = strtoul(val.c_str(), nullptr, 0);
		}else if(arg == "-h"){ // character height
			charHeight = strtoul(val.c_str(), nullptr, 0);
		}else if(arg == "-o"){ // output filename
			outputFilename = std::move(val);
		}else if(arg == "-s"){ // starting ascii code
			startAsciiCode = strtoul(val.c_str(), nullptr, 0);
		}else if(arg == "-e"){ // ending ascii code
			endAsciiCode = strtoul(val.c_str(), nullptr, 0);
		}else{
			throw std::invalid_argument(std::string("unknown key given: ") + arg);
		}
	}

	if(outputFilename.size() == 0){
		std::cout << "\tERROR: output filename is not given" << std::endl;
		return 1;
	}

	if(font_file.empty()){
		std::cout << "\tERROR: no font input file or more than one input files given" << std::endl;
		return 1;
	}

	if(charWidth == 0){
		std::cout << "\tERROR: character width is not given or has invalid value" << std::endl;
		return 1;
	}

	if(charWidth > 8){
		std::cout << "\tERROR: character width cannot be greater than 8, because HEX font does not support characters wider than 8 pixels" << std::endl;
		return 1;
	}

	if(charHeight == 0){
		std::cout << "\tERROR: character height is not given or has invalid value" << std::endl;
		return 1;
	}

	std::ifstream infile(font_file);

	std::map<uint8_t, std::vector<uint8_t>> chars;
	{
		std::string line;
		while(std::getline(infile, line)){
			uint16_t unicode = strtoul(line.substr(0, 4).c_str(), nullptr, 16);
			if(unicode < startAsciiCode || endAsciiCode < unicode){
				continue;
			}
			std::vector<uint8_t> bytes;
			for(unsigned i = 5; i < line.size(); i += 2){
				bytes.push_back(strtoul(line.substr(i, 2).c_str(), nullptr, 16));
			}
			for(unsigned i = bytes.size(); i < charHeight; ++i){
				bytes.push_back(0);
			}

//			std::cout << "char = " << std::hex << unicode << " maxRow = " << ((charHeight + uint8_t(-charHeight) % 8) / 8) << std::endl;

			std::vector<uint8_t> ssd1306Bytes;
			for(unsigned row = 0; row != (charHeight + uint8_t(-charHeight) % 8) / 8; ++row){ //row is 8 pixels high
				for(unsigned x = 0; x != 8 && x != charWidth; ++x){ //HEX font has only one byte per row (we assume there is no double width cases)
					uint8_t b = 0;
					for(unsigned y = 0; y != 8 && row + y != charHeight; ++y){ //y is within row
//						std::cout << "(bytes[" << (row + y) << "]) = " << std::hex << unsigned(bytes[row + y]) << std::endl;
						b |= (((bytes[row + y]) >> (7 - x)) & 1) << (y % 8);
					}
					ssd1306Bytes.push_back(b);
				}
			}
			chars.insert(std::make_pair(uint8_t(unicode), std::move(ssd1306Bytes)));
		}
	}

	std::ofstream out(outputFilename);

	out << "#include <array>" << std::endl << std::endl;
	out << "namespace{" << std::endl;
	out << "const uint8_t glyphWidth = " << charWidth << ";" << std::endl;
	out << "const uint8_t glyphHeight = " << charHeight << ";" << std::endl;
	out << "const uint8_t startAsciiCode = 0x" << std::hex << unsigned(startAsciiCode) << ";" << std::endl;
	out << "const std::array<std::array<uint8_t, glyphWidth * (glyphHeight + uint8_t(-glyphHeight) % 8) / 8>, " << std::dec << chars.size() << "> font ICACHE_RODATA_ATTR = {{" << std::endl;
	for(auto& p : chars){
		out << "\t{{";
		for(auto& b : p.second){
			out << "0x" << std::setfill('0') << std::setw(2) << std::hex << unsigned(b) << ",";
		}
		out << "}}," << std::endl;
	}
	out << "}};" << std::endl; // ~array
	out << "}" << std::endl; // ~namespace

	return 0;
}
