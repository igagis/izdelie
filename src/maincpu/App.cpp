#include "App.hpp"

extern "C"{
#include <gpio.h>
#include <user_interface.h>
#include "esp/espmissingincludes.h"
}

#include "i2cFrequency.hpp"
#include "../nixiecpu/i2c_address.hxx"

const uint8_t ssd1306DisplayI2cAddress = 0x3c;

App* App::instance = nullptr;

namespace{
uint8_t memoryBuffer[sizeof(App)];
}

void ICACHE_FLASH_ATTR App::create(){
	os_printf("initializing...\n");
	esp::print_current_stack_usage();
	new (&memoryBuffer) App();
	esp::print_current_stack_usage();
	os_printf("application object created\n");
}

namespace{

const uint8_t scl_gpio_num = 14;
const uint8_t sda_gpio_num = 12;

enum class SystemEvent{
	UNKNOWN,
	ENCODER_CW,
	ENCODER_CCW,
	ENCODER_DOWN,
	ENCODER_UP,

	ENUM_SIZE
};

const unsigned systemEventHandlerPriority = 0;
std::array<os_event_t, 10> systemEventHandlerQueue;

const uint32_t longPressTimeoutMs = 1000;

bool ignoreNextEncoderUpEvent = false;

}

void ICACHE_FLASH_ATTR App::systemEventHandler(os_event_t *event){
	auto ev = SystemEvent(event->sig);

	switch(ev){
		case SystemEvent::ENCODER_DOWN:
			ignoreNextEncoderUpEvent = false;
			App::inst().longPressTimer.disarm(); // Just in case, for example if debouncing didn't work well.
			App::inst().longPressTimer.arm(longPressTimeoutMs);
//			os_printf("Encoder Down: %d\n", event->par);
			break;
		case SystemEvent::ENCODER_UP:
			if(!ignoreNextEncoderUpEvent){
				App::inst().longPressTimer.disarm();
				App::inst().gui.event(InputEvent::ACTION);
			}
//			os_printf("Encoder Up: %d\n", event->par);
			break;
		case SystemEvent::ENCODER_CW:
//				os_printf("CW: %d\n", event->par);
			App::inst().gui.event(InputEvent::DOWN);
			break;
		case SystemEvent::ENCODER_CCW:
//				os_printf("CCW: %d\n", event->par);
			App::inst().gui.event(InputEvent::UP);
			break;
		default:
			os_printf("Unknown signal: %d\n", event->par);
			break;
	}
}

void ICACHE_FLASH_ATTR App::on_long_press(){
	ignoreNextEncoderUpEvent = true;
	App::inst().gui.event(InputEvent::ESCAPE);
}

namespace{
volatile bool gpio4_state = true;
volatile bool gpio5_state = true;
volatile bool gpio13_state = true;

volatile uint32_t lastInterruptTime = 0;

const uint32_t buttonDebouncingDelayUs = 30000;

// Return true if debounced, false otherwise.
// This function should be in IRAM since it is called from interrupt handler.
inline bool debounce(uint32_t timestamp, uint32_t delay){
	if(timestamp - lastInterruptTime >= delay){
		lastInterruptTime = timestamp;
		return true;
	}
	return false;
}

}

namespace{
std::array<bool, 16> rotary_encoder_validity_table ICACHE_RODATA_ATTR = {{
	false, // 0000
	true, // 0001
	true, // 0010
	false, // 0011
	true, // 0100
	false, // 0101
	false, // 0110
	true, // 0111
	true, // 1000
	false, // 1001
	false, // 1010
	true, // 1011
	false, // 1100
	true, // 1101
	true, // 1110
	false, // 1111
}};

uint8_t rotary_encoder_prev_cur_state = 0x3;
uint16_t rotary_encoder_sequence = 0x3;
}

namespace{
// This function should be in IRAM since it is called from interrupt handler.
SystemEvent rotary_encoder_process(bool pin1, bool pin2){
	rotary_encoder_prev_cur_state <<= 2;
	if(pin1){
		rotary_encoder_prev_cur_state |= 0x1;
	}
	if(pin2){
		rotary_encoder_prev_cur_state |= 0x2;
	}

	if(rotary_encoder_validity_table[rotary_encoder_prev_cur_state & 0xf]){
		rotary_encoder_sequence <<= 2;
		rotary_encoder_sequence |= (rotary_encoder_prev_cur_state & 0x3);
		if((rotary_encoder_sequence & 0x3f) == 0x7){
			return SystemEvent::ENCODER_CW;
		}
		if((rotary_encoder_sequence & 0x3f) == 0xb){
			return SystemEvent::ENCODER_CCW;
		}
	}

	return SystemEvent::UNKNOWN;
}
}

namespace{
// This handler should be in IRAM.
void gpioInterruptHandler(void* userData){
	ETS_GPIO_INTR_DISABLE();

	uint32 gpioStatus = GPIO_REG_READ(GPIO_STATUS_ADDRESS);

	auto timestamp = system_get_time();

	bool is_encoder_pin = false;

	if(gpioStatus & BIT(4)){
		gpio4_state= !gpio4_state;

		GPIO_INT_TYPE t;

		if(gpio4_state){
			t = GPIO_PIN_INTR_LOLEVEL;
		}else{
			t = GPIO_PIN_INTR_HILEVEL;
		}
		gpio_pin_intr_state_set(GPIO_ID_PIN(4), t);

		is_encoder_pin = true;
	}
	if(gpioStatus & BIT(5)){
		gpio5_state = !gpio5_state;

		GPIO_INT_TYPE t;

		if(gpio5_state){
			t = GPIO_PIN_INTR_LOLEVEL;
		}else{
			t = GPIO_PIN_INTR_HILEVEL;
		}
		gpio_pin_intr_state_set(GPIO_ID_PIN(5), t);

		is_encoder_pin = true;
	}

	if(is_encoder_pin){
		auto rot_enc = rotary_encoder_process(gpio4_state, gpio5_state);
		if(rot_enc != SystemEvent::UNKNOWN){
			system_os_post(systemEventHandlerPriority, os_signal_t(rot_enc), timestamp);
		}
	}

	if(gpioStatus & BIT(13)){
		if(debounce(timestamp, buttonDebouncingDelayUs)){
			gpio13_state = !gpio13_state;

			os_signal_t s;
			GPIO_INT_TYPE t;

			if(gpio13_state){
				s = os_signal_t(SystemEvent::ENCODER_UP);
				t = GPIO_PIN_INTR_LOLEVEL;
			}else{
				s = os_signal_t(SystemEvent::ENCODER_DOWN);
				t = GPIO_PIN_INTR_HILEVEL;
			}
			system_os_post(systemEventHandlerPriority, s, timestamp);
			gpio_pin_intr_state_set(GPIO_ID_PIN(13), t);
		}
	}

	// Clear interrupt status of triggered pins.
	GPIO_REG_WRITE(GPIO_STATUS_W1TC_ADDRESS, gpioStatus);

	ETS_GPIO_INTR_ENABLE();
}
}

namespace{
void ICACHE_FLASH_ATTR configureRotaryEncoderGpioPins(){
	PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO4_U, FUNC_GPIO4);
	PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO5_U, FUNC_GPIO5);
	PIN_FUNC_SELECT(PERIPHS_IO_MUX_MTCK_U, FUNC_GPIO13);

	PIN_PULLUP_EN(PERIPHS_IO_MUX_GPIO4_U);
	PIN_PULLUP_EN(PERIPHS_IO_MUX_GPIO5_U);
	PIN_PULLUP_EN(PERIPHS_IO_MUX_MTCK_U);

	// Set GPIO pins 4,5,13 as inputs.
	gpio_output_set(0, 0, 0, GPIO_ID_PIN(4) | GPIO_ID_PIN(5) | GPIO_ID_PIN(13));

	ETS_GPIO_INTR_DISABLE();

	ETS_GPIO_INTR_ATTACH(gpioInterruptHandler, nullptr);

	// Clear interrupt status for GPIO pins.
	GPIO_REG_WRITE(GPIO_STATUS_W1TC_ADDRESS, BIT(4));
	GPIO_REG_WRITE(GPIO_STATUS_W1TC_ADDRESS, BIT(5));
	GPIO_REG_WRITE(GPIO_STATUS_W1TC_ADDRESS, BIT(13));

	gpio_pin_intr_state_set(GPIO_ID_PIN(4), GPIO_PIN_INTR_LOLEVEL);
	gpio_pin_intr_state_set(GPIO_ID_PIN(5), GPIO_PIN_INTR_LOLEVEL);
	gpio_pin_intr_state_set(GPIO_ID_PIN(13), GPIO_PIN_INTR_LOLEVEL);

	ETS_GPIO_INTR_ENABLE();
}
}

ICACHE_FLASH_ATTR App::App() :
		longPressTimer(*this, &App::on_long_press),
		i2cBus(i2cFrequency, scl_gpio_num, sda_gpio_num),
		display(this->i2cBus, ssd1306DisplayI2cAddress, ssd1306::DisplaySize::SIZE_128x64),
		gui(display.dim()),
		textInput(display.dim()),
		message_screen(display.dim()),
		lightSensor(this->i2cBus, true),
		nixieBoard(this->i2cBus, nixieBoardI2cAddr)
{
	// Configure system events queue and handler.
	system_os_task(
			&systemEventHandler,
			systemEventHandlerPriority,
			systemEventHandlerQueue.begin(),
			systemEventHandlerQueue.size()
		);
	os_printf("system events queue initizlized\n");

	configureRotaryEncoderGpioPins();
	os_printf("rotary encoder initizlized\n");

	os_printf("initialization completed\n");

	this->display.setOn(true);
	this->gui.render_gui();
}
