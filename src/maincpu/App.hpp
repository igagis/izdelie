#pragma once

#include "esp/util.hpp"
#include "esp/i2c/master.hpp"
#include "esp/bh1750fvi/light_sensor.hpp"

#include "ssd1306/Display.hpp"

#include "gui/MainScreen.hpp"
#include "gui/TextInputScreen.hpp"
#include "gui/SelectorScreen.hpp"
#include "gui/MessageScreen.hpp"

#include "Clock.hpp"
#include "NixieBoard.hpp"
#include "LuminanceManager.hpp"
#include "WiFi.hpp"
#include "Updater.hpp"
#include "ScreenSaver.hpp"
#include "Stats.hpp"
#include "Settings.hpp"

class App : public esp::singleton<App>{
	friend class singleton<App>;
	static App* instance;

	void* operator new(size_t size, void* p){
		return p;
	}

	local_timer_t<App> longPressTimer;

	void on_long_press();

	static void systemEventHandler(os_event_t *event);

public:
	Settings settings;
	esp::i2c::master i2cBus;
	ssd1306::Display display;

	// allocate framebuffer to avoid allocating it on stack later
	ssd1306::FrameBuffer<ssd1306::ScreenSize::SIZE_128x64> frame_buffer;

	MainScreen gui; // Should be created early enough, because logs may be written to it.
	TextInputScreen textInput;
	SelectorScreen item_selector;
	MessageScreen message_screen;
	esp::bh1750fvi::light_sensor lightSensor;
	NixieBoard nixieBoard;
	Clock clock;
	LuminanceManager luminanceManager;
	WiFi wifi;
	Updater updater;
	ScreenSaver screen_saver;
	Stats stats;

	App();

	static void create();

private:
};
