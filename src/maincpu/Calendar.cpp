#include "Calendar.hpp"

extern "C"{
#include <os_type.h>
}

#include "esp/util.hpp"

namespace{
bool ICACHE_FLASH_ATTR isLeapYear(uint32_t year){
	return year % 4 == 0;
}

uint8_t ICACHE_FLASH_ATTR numDaysInMonth(uint8_t month, uint32_t year){
	switch(month){
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			return 31;
		case 2:
			return isLeapYear(year) ? 29 : 28;
		case 4:
		case 6:
		case 9:
		case 11:
			return 30;
		default:
			ASSERT(false)
			return 0;

	}
}
}

void ICACHE_FLASH_ATTR Calendar::addSecond(){
	++this->seconds;

	if(this->seconds < 60){
		return;
	}

	this->seconds = 0;
	++this->minutes;

	if(this->minutes < 60){
		return;
	}

	this->minutes = 0;
	++this->hours;

	if(this->hours < 24){
		return;
	}

	this->hours = 0;
	++this->day;

	if(this->day <= numDaysInMonth(this->month, this->year)){
		return;
	}

	this->day = 1;
	++this->month;

	if(this->month <= 12){
		return;
	}

	this->month = 1;
	++this->year;
}

void ICACHE_FLASH_ATTR Calendar::set(uint32_t year, uint8_t month, uint8_t day, uint8_t hours, uint8_t minutes, uint8_t seconds){
	this->year = year;
	this->month = month;
	this->day = day;
	this->hours = hours;
	this->minutes = minutes;
	this->seconds = seconds;
}
