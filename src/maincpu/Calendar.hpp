#pragma once

#include <cstdint>

class Calendar{
	uint8_t seconds = 0;
	uint8_t minutes = 0;
	uint8_t hours = 0;

	uint8_t day = 1;
	uint8_t month = 1;
	uint32_t year = 2019;

public:
	void addSecond();

	void set(uint32_t year, uint8_t month, uint8_t day, uint8_t hours, uint8_t minutes, uint8_t seconds);

	uint8_t getSeconds()const noexcept{
		return this->seconds;
	}

	uint8_t getMinutes()const noexcept{
		return this->minutes;
	}

	uint8_t getHours()const noexcept{
		return this->hours;
	}

	uint8_t getDay()const noexcept{
		return this->day;
	}

	uint8_t getMonth()const noexcept{
		return this->month;
	}

	uint32_t getYear()const noexcept{
		return this->year;
	}
};
