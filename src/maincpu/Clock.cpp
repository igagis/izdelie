#include "Clock.hpp"

extern "C"{
#include <osapi.h>
#include <user_interface.h>
}

#include "esp/util.hpp"
#include "App.hpp"


namespace{

const uint32_t antipoisoning_procedure_period_ms = 5 * 60 * 1000 + 23 * 1000; //5 minutes 23 seconds

}


ICACHE_FLASH_ATTR Clock::Clock() :
		blinker_timer(*this, &Clock::onTick),
		cleaning_timer(*this, &Clock::cleaning_step)
{
	this->init_blinker();
	this->init_cleaner();
	this->trigger_cleaning();
}

void ICACHE_FLASH_ATTR Clock::reset(){
	this->blinker_timer.disarm();

	this->init_blinker();

	this->subsecondTicks = 0;
	this->cpuTicks = esp::get_cpu_ticks();

	this->onTick();
}


void ICACHE_FLASH_ATTR Clock::init_cleaner(){
	if(this->cleaner){
		this->cleaner->~cleaner_t();
	}

	switch(this->cleaning_type){
		default:
		case cleaning_type_t::plain:
			this->cleaner = new(&this->cleaner_instance.in_out) cleaner_in_out_t(0, 4, 100, {{true, true, true, true}});
			break;
		case cleaning_type_t::chess:
			this->cleaner = new(&this->cleaner_instance.in_out) cleaner_in_out_t(0, 4, 100, {{true, false, true, false}});
			break;
		case cleaning_type_t::middle:
			this->cleaner = new(&this->cleaner_instance.in_out) cleaner_in_out_t(0, 4, 100, {{true, false, false, true}});
			break;
		case cleaning_type_t::sides:
			this->cleaner = new(&this->cleaner_instance.in_out) cleaner_in_out_t(0, 4, 100, {{true, true, false, false}});
			break;
	}

	this->cleaner->reset();
}

void ICACHE_FLASH_ATTR Clock::init_blinker(){
	if(this->blinker){
		this->blinker->~blinker_t();
	}

	switch(App::inst().settings.blink_type){
		default:
		case BlinkType::dot:
			this->blinker = new(&this->blinker_instance.two_state) two_state_blinker_t(0x10, 0);
			break;
		case BlinkType::two_dots:
			this->blinker = new(&this->blinker_instance.two_state) two_state_blinker_t(0x18, 0x0);
			break;
		case BlinkType::pendulum_1:
			this->blinker = new(&this->blinker_instance.two_state) two_state_blinker_t(0x10, 0x08);
			break;
		case BlinkType::pendulum_2:
			this->blinker = new(&this->blinker_instance.two_state) two_state_blinker_t(0x60, 0x06);
			break;
		case BlinkType::pendulum_3:
			this->blinker = new(&this->blinker_instance.two_state) two_state_blinker_t(0x80, 0x01);
			break;
		case BlinkType::pendulum_4:
			this->blinker = new(&this->blinker_instance.two_state) two_state_blinker_t(0xf0, 0x0f);
			break;
		case BlinkType::pendulum_5:
			this->blinker = new(&this->blinker_instance.two_state) two_state_blinker_t(0xfc, 0x3f);
			break;
		case BlinkType::chess_1:
			this->blinker = new(&this->blinker_instance.two_state) two_state_blinker_t(0xaa, 0x55);
			break;
		case BlinkType::chess_2:
			this->blinker = new(&this->blinker_instance.two_state) two_state_blinker_t(0xa5, 0x5a);
			break;
		case BlinkType::sine_1_sec:
			this->blinker = new(&this->blinker_instance.sine) sine_1_sec_blinker_t();
			break;
		case BlinkType::sine_2_sec:
			this->blinker = new(&this->blinker_instance.sine) sine_2_sec_blinker_t();
			break;
		case BlinkType::eyes_1:
			this->blinker = new(&this->blinker_instance.two_state) two_state_blinker_t(0x77, 0xee);
			break;
		case BlinkType::eyes_2:
			this->blinker = new(&this->blinker_instance.two_state) two_state_blinker_t(0x36, 0x6c);
			break;
		case BlinkType::eyes_3:
			this->blinker = new(&this->blinker_instance.two_state) two_state_blinker_t(0x63, 0xc6);
			break;
		case BlinkType::eyes_4:
			this->blinker = new(&this->blinker_instance.two_state) two_state_blinker_t(0x28, 0x14);
			break;
		case BlinkType::eyes_5:
			this->blinker = new(&this->blinker_instance.two_state) two_state_blinker_t(0x44, 0x22);
			break;
		case BlinkType::eyes_6:
			this->blinker = new(&this->blinker_instance.two_state) two_state_blinker_t(0x82, 0x41);
			break;
	}
}

void ICACHE_FLASH_ATTR Clock::trigger_cleaning(){
	ASSERT(this->cleaner)
	this->cleaner->reset();
	this->cleaning_is_active = true;
	this->cleaning_step();
}

void ICACHE_FLASH_ATTR Clock::cleaning_step(){
	this->cleaning_timer.disarm();

	// os_printf("cleaning_step(): invoked\n");

	auto timeout = this->cleaner->step();
	// os_printf("cleaning_step(): timeout = %d\n", timeout);
	if(timeout){
		this->cleaning_is_active = true;
		this->cleaning_timer.arm(timeout);
	}else{
		this->cleaning_type = cleaning_type_t(int(this->cleaning_type) + 1);
		if(this->cleaning_type == cleaning_type_t::ENUM_SIZE){
			this->cleaning_type = cleaning_type_t::plain;
		}
		this->init_cleaner();
		this->cleaning_is_active = false;
		this->cleaning_timer.arm(antipoisoning_procedure_period_ms);
		this->set_current_time_display();
	}
}

void ICACHE_FLASH_ATTR Clock::set_current_time_display(){
	uint8_t hours_num = this->calendar.getHours();

	if(!App::inst().settings.is_24_hour_format){
		hours_num %= 12;
		if(hours_num == 0){
			hours_num = 12;
		}
	}

	uint8_t hours = (uint8_t(hours_num / 10) << 4) | uint8_t(hours_num % 10);
	if((hours & 0xf0) == 0 && !App::inst().settings.show_leading_zero){
		hours |= 0xa0; // set high digit to 11, which means do not display any digit
	}

	uint8_t minutes = (uint8_t(this->calendar.getMinutes() / 10) << 4) | uint8_t(this->calendar.getMinutes() % 10);

	App::inst().nixieBoard.setDisplay(
			uint16_t(hours) << 8 | uint16_t(minutes),
			this->blink_dots,
			pack_brightness(App::inst().luminanceManager.get_brightness())
		);
}

void ICACHE_FLASH_ATTR Clock::onTick(){
	{
		auto ticks = esp::get_cpu_ticks();
		this->subsecondTicks += (ticks - this->cpuTicks);
		this->cpuTicks = ticks;
//		os_printf("cpu ticks = %u\n", ticks);
	}

	while(this->subsecondTicks > esp::get_cpu_ticks_per_second()){
		App::inst().stats.uptime_add_second();
		this->calendar.addSecond();
		this->subsecondTicks -= esp::get_cpu_ticks_per_second();
	}

	auto blink_res = this->blinker->tick();
	this->blink_dots = blink_res.dots;
	this->blinker_timer.arm(blink_res.timeout);

	if(this->cleaning_is_active){
		return;
	}

	this->set_current_time_display();
}


