#pragma once

extern "C"{
#include <os_type.h>
}

#include "Timer.hpp"
#include "Calendar.hpp"

#include "cleaner_t.hpp"
#include "blinker_t.hpp"

class Clock{
	uint32_t cpuTicks;

	uint32_t subsecondTicks;

	local_timer_t<Clock> blinker_timer;

	// Blinker

	union blinker_union_t{
		blinker_union_t(){}
		two_state_blinker_t two_state;
		sine_blinker_t sine;
	} blinker_instance;

	blinker_t* blinker = nullptr;


	local_timer_t<Clock> cleaning_timer;

	enum class cleaning_type_t{
		plain,
		chess,
		middle,
		sides,

		ENUM_SIZE
	} cleaning_type = cleaning_type_t::plain;

	bool cleaning_is_active = true;

	void cleaning_step();

	union cleaner_union_t{
		cleaner_union_t(){}
		cleaner_asc_desc_t asc_desc;
		cleaner_in_out_t in_out;
	} cleaner_instance;

	cleaner_t* cleaner = nullptr;

	uint8_t blink_dots; //this holds last blink dots to restore it after cleaning procedure

	void set_current_time_display();
public:
	Calendar calendar;

	Clock();

	void init_cleaner();

	void init_blinker();

	void reset();

	void onTick();

	void trigger_cleaning();
};
