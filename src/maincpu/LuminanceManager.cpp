#include "LuminanceManager.hpp"

extern "C"{
#include <c_types.h>
}

#include <numeric>
#include <algorithm>

#include "App.hpp"

namespace{
const uint32_t timerPeriod_c = 500;

static_assert(timerPeriod_c >= 180, "timer period is less than time needed to measure the luminance by light sensor, see datasheet on bh1750fvi");
}

ICACHE_FLASH_ATTR LuminanceManager::LuminanceManager() :
		timer(*this, &LuminanceManager::onTick)
{
	std::fill(this->circular_buffer.begin(), this->circular_buffer.end(), max_luminance);

	this->onTick();
}

uint32_t ICACHE_FLASH_ATTR LuminanceManager::get_luminance()const noexcept{
	return this->current_luminance;
}

void ICACHE_FLASH_ATTR LuminanceManager::push_luminance_to_circular_buffer(uint32_t luminance){
	*this->circular_iter = luminance;
	++this->circular_iter;
	if(this->circular_iter == this->circular_buffer.end()){
		this->circular_iter = this->circular_buffer.begin();
	}
}

void ICACHE_FLASH_ATTR LuminanceManager::onTick(){
	if(this->measurementStarted){
		auto luminance = App::inst().lightSensor.read_measurement();
//		os_printf("Luminance: %d\n", luminance);
		if(luminance >= 0){
			this->current_luminance = uint32_t(luminance);
		}else{
			this->current_luminance = max_luminance;
		}
		this->push_luminance_to_circular_buffer(this->current_luminance);
		this->current_nixie_brightness = this->calc_brightness();
		App::inst().display.set_brightness(this->get_brightness());
	}else{
		App::inst().lightSensor.start_measurement();
	}
	this->measurementStarted = !this->measurementStarted;

	this->timer.arm(timerPeriod_c);
}

uint8_t ICACHE_FLASH_ATTR LuminanceManager::get_brightness()const noexcept{
	return this->current_nixie_brightness;
}

uint8_t ICACHE_FLASH_ATTR LuminanceManager::calc_brightness()const noexcept{
	// os_printf("\n");
	// for(auto&p : this->curve){
	// 	os_printf("%d\n", p);
	// }

	auto i = std::upper_bound(
			this->curve.begin(),
			this->curve.end(),
			std::accumulate(this->circular_buffer.begin(), this->circular_buffer.end(), 0)
		);

	return std::distance(this->curve.begin(), i);
}
