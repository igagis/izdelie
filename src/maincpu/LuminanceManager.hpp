#pragma once

extern"C"{
#include <os_type.h>
}

#include <array>
#include <cmath>

#include "Timer.hpp"

class LuminanceManager{
	static const uint32_t max_luminance = 1254;

	static const uint32_t max_brightness = 15; // Brightness is given as 4 bit value to Nixie board

	// The luminance to brightness curve is:
	//  y(x) = 9.9323 * ln(x) + 27.059		when x <= 1254 lux
	//  y(x) = 100 %						when x > 1254 lux
	//
	// Taken from https://www.maximintegrated.com/en/app-notes/index.mvp/id/4913

	constexpr static uint32_t brightness(uint32_t luminance_lux){
		using std::log;
		return ((9.9323 * log(luminance_lux) + 27.059) / 100) * max_brightness;
	}

	// reverse of brightness()
	constexpr static uint32_t luminance_lux(uint32_t brightness_value, uint32_t coefficient){
		using std::exp;
		using std::ceil;
		return ceil(exp((brightness_value * 100 / max_brightness - 27.059) / 9.9323) * coefficient);
	}

	static const uint32_t num_averaging_measurements = 8;

	std::array<uint32_t, max_brightness> curve = {{
		luminance_lux(1, num_averaging_measurements),
		luminance_lux(2, num_averaging_measurements),
		luminance_lux(3, num_averaging_measurements),
		luminance_lux(4, num_averaging_measurements),
		luminance_lux(5, num_averaging_measurements),
		luminance_lux(6, num_averaging_measurements),
		luminance_lux(7, num_averaging_measurements),
		luminance_lux(8, num_averaging_measurements),
		luminance_lux(9, num_averaging_measurements),
		luminance_lux(10, num_averaging_measurements),
		luminance_lux(11, num_averaging_measurements),
		luminance_lux(12, num_averaging_measurements),
		luminance_lux(13, num_averaging_measurements),
		luminance_lux(14, num_averaging_measurements),
		max_luminance * num_averaging_measurements
	}};

	local_timer_t<LuminanceManager> timer;

	bool measurementStarted = false;

	std::array<uint32_t, num_averaging_measurements> circular_buffer;
	decltype(circular_buffer)::iterator circular_iter = circular_buffer.begin();

	void push_luminance_to_circular_buffer(uint32_t luminance);

	uint32_t current_luminance = max_luminance;

	uint8_t current_nixie_brightness = 0xf;

	uint8_t calc_brightness()const noexcept;

public:
	LuminanceManager();

	uint32_t get_luminance()const noexcept;

	// Get nixie tube brightness for current luminance.
	uint8_t get_brightness()const noexcept;
private:
	void onTick();
};
