#include "NixieBoard.hpp"

extern "C"{
#include <c_types.h>
}

#include <array>

#include "App.hpp"

#include "../nixiecpu/I2cCommand.hpp"

ICACHE_FLASH_ATTR NixieBoard::NixieBoard(esp::i2c::master& i2c, uint8_t i2cAddress) :
		i2c(i2c),
		i2cAddress(i2cAddress)
{
	this->queryBoard();
}

void ICACHE_FLASH_ATTR NixieBoard::queryBoard(){
	std::array<uint8_t, 1> cmd = {{
		uint8_t(I2cCommand::GET_VERSION)
	}};
	std::array<uint8_t, 2> buf;
	if(this->i2c.send_recv(
			this->i2cAddress,
			cmd.begin(),
			cmd.end(),
			buf.begin(),
			buf.end()
		) != esp::i2c::status::ok)
	{
		os_printf("ERROR: NACK while querying nixie board version, is board connected?\n");
		this->boardType = 0;
		this->boardVersion = 0;
	}else{
		this->boardType = buf[0];
		this->boardVersion = buf[1];
	}
	os_printf("Nixie board type:version = %d:%d\n", this->boardType, this->boardVersion);
}

void ICACHE_FLASH_ATTR NixieBoard::setDisplay(uint16_t digits, uint8_t dots, uint16_t brightness){
	if(!this->isBoardPresent()){
		os_printf("ERROR: No nixie board present, trying to find the board...\n");
		// board not found
		this->queryBoard();
		if(!this->isBoardPresent()){
			os_printf("\tNixie board not found\n");
			return;
		}
		os_printf("\tNixie board found\n");
	}

	std::array<uint8_t, 6> buffer = {{
		uint8_t(I2cCommand::SET_DISPLAY),
		uint8_t(digits >> 8),
		uint8_t(digits & 0xff),
		dots,
		uint8_t(brightness >> 8),
		uint8_t(brightness & 0xff)
	}};

	if(this->i2c.send(this->i2cAddress, buffer.begin(), buffer.end()) != esp::i2c::status::ok){
		os_printf("ERROR: NACK while sending data to nixies\n");
	}
}
