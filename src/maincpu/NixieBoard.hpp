#pragma once

#include <cstdint>

#include "esp/i2c/master.hpp"

class NixieBoard{
	esp::i2c::master& i2c;

	const uint8_t i2cAddress;

	uint8_t boardType;
	uint8_t boardVersion;

	void queryBoard();
public:
	NixieBoard(esp::i2c::master& i2c, uint8_t i2cAddress);

	void setDisplay(uint16_t digits, uint8_t dots, uint16_t brightness);

	bool isBoardPresent()const noexcept{
		return this->boardType != 0;
	}
};

inline uint16_t ICACHE_FLASH_ATTR pack_brightness(uint8_t brightness){
	return uint16_t(brightness) | (uint16_t(brightness) << 4) | (uint16_t(brightness) << 8) | (uint16_t(brightness) << 12);
}
