#pragma once

extern"C"{
#include <user_interface.h>
}

enum class Partition{
	Ota1 = SYSTEM_PARTITION_OTA_1,
	Ota2 = SYSTEM_PARTITION_OTA_2,
	RfCal = SYSTEM_PARTITION_RF_CAL,
	PhyData = SYSTEM_PARTITION_PHY_DATA,
	SystemParameter = SYSTEM_PARTITION_SYSTEM_PARAMETER,

	Settings = SYSTEM_PARTITION_CUSTOMER_BEGIN

	// Add other custom partitions here, to the end of the enum
};
