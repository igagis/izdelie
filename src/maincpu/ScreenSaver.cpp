#include "ScreenSaver.hpp"

extern"C"{
#include <c_types.h>
}

#include "App.hpp"

namespace{
const uint32_t screensaver_timeout_ms = 10000;
const uint32_t autoclose_timeout_ms = 5 * 60 * 1000;
}

ICACHE_FLASH_ATTR ScreenSaver::ScreenSaver() :
		screensaver_timer(*this, &ScreenSaver::on_screensaver),
		autoclose_timer(*this, &ScreenSaver::on_autoclose)
{
	this->renew_timers();
}

bool ICACHE_FLASH_ATTR ScreenSaver::renew_timers(){
	this->screensaver_timer.disarm();
	this->screensaver_timer.arm(screensaver_timeout_ms);

	this->autoclose_timer.disarm();
	this->autoclose_timer.arm(autoclose_timeout_ms);

	if(!App::inst().display.is_on()){
		App::inst().display.setOn(true);
		return true;
	}
	
	return false;
}

void ICACHE_FLASH_ATTR ScreenSaver::on_screensaver(){
	App::inst().display.setOn(false);
}

void ICACHE_FLASH_ATTR ScreenSaver::on_autoclose(){
	App::inst().gui.closeChildren();
}
