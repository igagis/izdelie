#pragma once

#include "Timer.hpp"

class ScreenSaver{
	local_timer_t<ScreenSaver> screensaver_timer;
	local_timer_t<ScreenSaver> autoclose_timer;

public:
	ScreenSaver();

	void on_screensaver();

	void on_autoclose();

	// returns true if screen was off
	bool renew_timers();
};
