#include "Settings.hpp"

extern"C"{
#include <c_types.h>
#include <user_interface.h>
#include <osapi.h>
}

#include <array>

#include "esp/util.hpp"
#include "Partition.hpp"

namespace{

// ATTENTION: Do not remove or reorder setting types, only add new ones to the end of the enumeration.
enum class SettingType : uint8_t{
	end = 0,
	bool_24_hour_format,
	uint8_blink_type,
	bool_screen_on_when_sync,
	uint32_initial_uptime, // for saving uptime between power cycles due to upgrade
	bool_show_leading_zero,

	enum_size
};

const uint32_t settings_buffer_size = sizeof(Settings) + size_t(SettingType::enum_size);
// 4 kb = 1 sector in flash memory.
static_assert(settings_buffer_size <= 0x1000, "settings buffer size must be 4kb or less");

uint16_t settings_sector = 0;

}

ICACHE_FLASH_ATTR Settings::Settings(){
	os_printf("Settings::Settings(): invoked\n");
	
	partition_item_t partition;

	if(!system_partition_get_item(partition_type_t(Partition::Settings), &partition)){
		os_printf("ERROR: getting settings partition failed");
		ASSERT(false)
		return;
	}

	settings_sector = partition.addr / 0x1000;

	this->read();
}


void ICACHE_FLASH_ATTR Settings::read(){
	os_printf("Settings::read(): invoked\n");

	ASSERT(settings_sector != 0)

	std::array<uint8_t, settings_buffer_size> buf;

	os_printf("loading settings...\n");
	if(!system_param_load(settings_sector, 0, buf.begin(), buf.size())){
		os_printf("error: reading settings from flash failed");
		return;
	}
	os_printf("settings read\n");

	for(uint8_t* p = buf.begin(); p != buf.end(); ++p){
		switch(SettingType(*p)){
			default:
				// unknown setting type encountered, settings malformed, stop parsing
				os_printf("error: unknown setting type encountered, settings malformed\n");
				return;
			case SettingType::end:
				return;
			case SettingType::bool_24_hour_format:
				++p;
				ASSERT(p != buf.end())
				this->is_24_hour_format = bool(*p);
				break;
			case SettingType::uint8_blink_type:
				++p;
				ASSERT(p != buf.end())
				this->blink_type = BlinkType(*p);
				break;
			case SettingType::bool_screen_on_when_sync:
				++p;
				ASSERT(p != buf.end())
				this->screen_on_when_sync = bool(*p);
				break;
			case SettingType::uint32_initial_uptime:
				++p;
				ASSERT(p != buf.end())
				this->initial_uptime = *p;
				++p;
				ASSERT(p != buf.end())
				this->initial_uptime |= (uint32_t(*p) << 8);
				++p;
				ASSERT(p != buf.end())
				this->initial_uptime |= (uint32_t(*p) << 16);
				++p;
				ASSERT(p != buf.end())
				this->initial_uptime |= (uint32_t(*p) << 24);
				break;
			case SettingType::bool_show_leading_zero:
				++p;
				ASSERT(p != buf.end())
				this->show_leading_zero = bool(*p);
				break;
		}
	}
	os_printf("settings loaded\n");
}


void ICACHE_FLASH_ATTR Settings::save(){
	if(settings_sector == 0){
		return;
	}

	std::array<uint8_t, settings_buffer_size> buf = {{
		uint8_t(SettingType::bool_24_hour_format), uint8_t(this->is_24_hour_format),
		uint8_t(SettingType::uint8_blink_type), uint8_t(this->blink_type),
		uint8_t(SettingType::bool_screen_on_when_sync), uint8_t(this->screen_on_when_sync),
		uint8_t(SettingType::uint32_initial_uptime),
				uint8_t(this->initial_uptime & 0xff),
				uint8_t((this->initial_uptime >> 8) & 0xff),
				uint8_t((this->initial_uptime >> 16) & 0xff),
				uint8_t((this->initial_uptime >> 24) & 0xff),
		uint8_t(SettingType::bool_show_leading_zero), uint8_t(this->show_leading_zero),
		uint8_t(SettingType::end)
	}};

	if(!system_param_save_with_protect(settings_sector, buf.begin(), buf.size())){
		os_printf("ERROR: saving settings failed");
		return;
	}
}