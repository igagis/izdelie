#pragma once

extern"C"{
#include <c_types.h>
}

// WARNING: add new items to the end of enum!!!
enum class BlinkType : uint8_t{
	dot, // [  ][ .] [  ][  ] <-> [  ][  ] [  ][  ]
	two_dots, // [  ][ .] [. ][  ] <-> [  ][  ] [  ][  ]
	pendulum_1, // [  ][ .] [  ][  ] <-> [  ][  ] [. ][  ]
	pendulum_2, // [ .][. ] [  ][  ] <-> [  ][  ] [ .][. ]
	pendulum_3, // [. ][  ] [  ][  ] <-> [  ][  ] [  ][ .]
	pendulum_4, // [..][..] [  ][  ] <-> [  ][  ] [..][..]
	pendulum_5, // [  ][..] [..][..] <-> [..][..] [..][  ]
	chess_1, // [. ][. ] [. ][. ] <-> [ .][ .] [ .][ .]
	chess_2, // [. ][. ] [ .][ .] <-> [ .][ .] [. ][. ]
	sine_1_sec,
	sine_2_sec,
	eyes_1, // [ .][..] [ .][..] <-> [..][. ] [..][. ]
	eyes_2, // [  ][..] [ .][. ] <-> [ .][. ] [..][  ]
	eyes_3, // [ .][. ] [  ][..] <-> [..][  ] [ .][. ]
	eyes_4, // [  ][. ] [. ][  ] <-> [  ][ .] [ .][  ]
	eyes_5, // [ .][  ] [ .][  ] <-> [  ][. ] [  ][. ]
	eyes_6, // [. ][  ] [  ][. ] <-> [ .][  ] [  ][ .]

	ENUM_SIZE
};

class Settings{
	void read();

public:
	Settings();

	bool is_24_hour_format = true;

	bool show_leading_zero = false;

	BlinkType blink_type = BlinkType::dot;

	bool screen_on_when_sync = true;

	uint32_t initial_uptime = 0;

	void save();
};
