#include "Stats.hpp"

extern"C"{
#include <c_types.h>
#include <osapi.h>
}

#include "App.hpp"

ICACHE_FLASH_ATTR Stats::Stats(){
	this->uptime_sec = App::inst().settings.initial_uptime;

	App::inst().settings.initial_uptime = 0;
	App::inst().settings.save();
}

uint32_t ICACHE_FLASH_ATTR Stats::get_uptime()const noexcept{
	return this->uptime_sec;
}

void ICACHE_FLASH_ATTR Stats::uptime_add_second(){
//	os_printf("adding second to uptime: %d\n", this->uptime_sec);
	this->uptime_sec += 1;
}
