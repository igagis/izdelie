#pragma once

#include <array>

class Stats{
	uint32_t uptime_sec = 0;
public:

	Stats();

	static const int firmware_version =
#include "firmware_version.txt"
	;

	std::array<char, 128> country = {{'\0'}};
	std::array<char, 128> city = {{'\0'}};
	std::array<char, 128> timezone = {{'\0'}};

	uint32_t get_uptime()const noexcept;

	void uptime_add_second();
};
