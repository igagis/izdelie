#include "Timer.hpp"

extern"C"{
#include <c_types.h>
}

#include "esp/util.hpp"

void ICACHE_FLASH_ATTR Timer::callback(void *arg){
	ASSERT(arg)
	Timer& t = *reinterpret_cast<Timer*>(arg);

	auto ticks = esp::get_cpu_ticks();

	auto dticks = int32_t(ticks - t.end_cpu_ticks);

	if(dticks >= 0){
		t.on_expire();
	}else{
		uint32_t timeout_ms = uint32_t(-dticks) / (esp::get_cpu_ticks_per_second() / 1000);
		os_timer_arm(
				&t.timer,
				timeout_ms,
				false // non-repeating
			);
	}
}

ICACHE_FLASH_ATTR Timer::Timer(){
	// os_printf("Timer::Timer(): init timer\n");
	os_timer_setfn(&this->timer, &callback, this);
	// os_printf("Timer::Timer(): timer inited\n");
}

void ICACHE_FLASH_ATTR Timer::arm(uint32_t timeout_ms){
	this->disarm();

	this->end_cpu_ticks = esp::get_cpu_ticks() + timeout_ms * (esp::get_cpu_ticks_per_second() / 1000);

	os_timer_arm(
			&this->timer,
			timeout_ms,
			false // non-repeating
		);
}

void ICACHE_FLASH_ATTR Timer::disarm(){
	os_timer_disarm(&this->timer);
}
