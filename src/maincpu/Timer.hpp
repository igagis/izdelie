#pragma once

extern"C"{
#include <os_type.h>
}

class Timer{
	os_timer_t timer;

	uint32_t end_cpu_ticks;

	static void callback(void* arg);
public:

	Timer();

	void arm(uint32_t timeout_ms);

	void disarm();

	virtual void on_expire(){}
};

template <class T> class local_timer_t : public Timer{
	T& object;
	void (T::*object_on_expire)();
public:
	local_timer_t(T& object, void (T::*object_on_expire)()) :
			object(object),
			object_on_expire(object_on_expire)
	{}

	void on_expire()override{
		(this->object.*(this->object_on_expire))();
	}
};
