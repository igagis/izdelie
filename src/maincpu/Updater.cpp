#include "Updater.hpp"

extern"C"{
#include <c_types.h>
#include <user_interface.h>
#include <mem.h>
#include <json/jsonparse.h>
}

#include <cstdlib>

#include "esp/util.hpp"

#include "App.hpp"

namespace{

const uint32_t sync_period_ms_c = 60 * 60 * 1000;

const char* firmware_version_url1 ICACHE_RODATA_ATTR = "http://izdelie.icu/firmware/version";
const char* firmware_version_url2 ICACHE_RODATA_ATTR = "http://izdelie.hopto.org/firmware/version";
const char* location_by_ip_url ICACHE_RODATA_ATTR = "http://ip-api.com/json";
const char* current_time_url ICACHE_RODATA_ATTR = "http://api.timezonedb.com/v2.1/get-time-zone?key=YOY9X4UZ7RYC&format=json&by=zone&zone=%s";
const char* report_statistics_url ICACHE_RODATA_ATTR = "http://izdelie.icu/api/report";

}

ICACHE_FLASH_ATTR Updater::Updater() :
		timer(*this, &Updater::update)
{
	os_printf("FIRMWARE VERSION: %d\n", Stats::firmware_version);
}

void ICACHE_FLASH_ATTR Updater::update(){
	os_printf("updater::update(): enter\n");
	esp::print_current_stack_usage();

	// Check firmware update first.
	App::inst().gui.log("Checking for firmware updates");
	this->state = State::FIRMWARE_URL_1;
	this->get(firmware_version_url1);

	esp::print_current_stack_usage();
	os_printf("updater::update(): exit\n");
}

void ICACHE_FLASH_ATTR Updater::get(esp::string_view url){
	os_printf("updater::get(): enter\n");
	esp::print_current_stack_usage();

	this->http_req = std::unique_ptr<esp::httpc::request>(
			new (std::nothrow) esp::httpc::request(
					this,
					&Updater::callback
				)
		);
	
	this->http_req->set_url(url);
	this->http_req->start();

	esp::print_current_stack_usage();
	os_printf("updater::get(): exit\n");
}

void ICACHE_FLASH_ATTR Updater::post(
		esp::string_view url,
		esp::string&& body
	)
{
	os_printf("updater::post(): enter\n");
	esp::print_current_stack_usage();

	this->http_req = std::unique_ptr<esp::httpc::request>(
			new (std::nothrow) esp::httpc::request(
					this,
					&Updater::callback
				)
		);
	
	this->http_req->set_url(url);
	this->http_req->set_body(std::move(body));
	this->http_req->start();

	esp::print_current_stack_usage();
	os_printf("updater::post(): exit\n");
}

void ICACHE_FLASH_ATTR Updater::callback(void* user_data, const esp::httpc::request& req){
	auto& updater = *reinterpret_cast<Updater*>(user_data);

	const auto& resp = req.get_response();

	os_printf("HTTP response code: %d\n", resp.http_status_code);

	// In case of connection related error, just re-schedule update for later.
	if(resp.status_code != esp::httpc::status::ok){
		App::inst().gui.log("Connection failed");
		App::inst().gui.log(" status code = %d", unsigned(resp.status_code));

		// If error was due to WiFi not connected.
		if(wifi_station_get_connect_status() != STATION_GOT_IP){
			App::inst().gui.log("reason: WiFi down");
			os_printf("ERROR: HTTP request failed due to WiFi disconnect.\n");

			// assume that Wi-Fi reconnection will happen automatically by the Wi-Fi stack
			// TODO: request reconnection explicitly?

			// Set state to IDLE to try update right after WiFi is connected.
			updater.state = State::IDLE;
			return;
		}else{
			App::inst().gui.log("reason: TCP fail");
			os_printf("ERROR: HTTP request failed due to TCP connection has failed.\n");
		}
	}

	if(resp.http_status_code != 200){
		App::inst().gui.log("Failed due to server error");
		App::inst().gui.log(" HTTP code = %d", resp.http_status_code);
	}

	switch(updater.state){
		case State::FIRMWARE_URL_1:
			if(resp.http_status_code != 200){
				os_printf("Fetching latest firmware version from URL 1 failed, trying second URL.\n");
				App::inst().gui.log("Trying URL 2...");
				updater.state = State::FIRMWARE_URL_2;
				updater.get(firmware_version_url2);
			}else{
				updater.handle_firmware_version(
						resp.body,
						resp.remote_ip,
						req.get_host().c_str(),
						req.get_path().c_str()
					);
			}
			break;
		case State::FIRMWARE_URL_2:
			if(resp.http_status_code != 200){
				os_printf("Fetching latest firmware version from URL 2 failed.\n");
				updater.start_time_sync();
			}else{
				updater.handle_firmware_version(
						resp.body,
						resp.remote_ip,
						req.get_host().c_str(),
						req.get_path().c_str()
					);
			}
			break;
		case State::LOCATION_BY_IP:
			if(resp.http_status_code != 200){
				os_printf("Fetching location by IP failed.\n");
				updater.report_stats();
			}else{
				updater.handle_location_by_ip(resp.body);
			}
			break;
		case State::CURRENT_TIME:
			if(resp.http_status_code != 200){
				os_printf("Fetching current time failed.\n");
				updater.report_stats();
			}else{
				updater.handle_current_time(resp.body);
			}
			break;
		case State::REPORT_STATS:
			if(resp.http_status_code != 200){
				os_printf("Reporting statistics failed.\n");
			}
			updater.schedule_update();
			break;

		default:
			ASSERT(false)
			break;
	}
}

void ICACHE_FLASH_ATTR Updater::handle_firmware_version(
		esp::string_view body,
		std::array<uint8_t, 4> ip,
		const char* host,
		const char* path
	)
{
	jsonparse_state jps;

	jsonparse_setup(&jps, body.data(), body.size());

	enum class Field{
		UNKNOWN,
		VERSION
	} field = Field::UNKNOWN;
	int object_level = 0;

	int version = Stats::firmware_version;

	while(auto json_type = jsonparse_next(&jps)){
		switch (json_type){
			case JSON_TYPE_OBJECT:
				++object_level;
				break;
			case '}':
				--object_level;
				break;
			case JSON_TYPE_PAIR_NAME:
				field = Field::UNKNOWN;
				if(object_level == 1){
					if(jsonparse_strcmp_value(&jps, "version") == 0){
						field = Field::VERSION;
					}
				}
				break;
			case JSON_TYPE_NUMBER:
				switch(field){
					case Field::VERSION:
						version = jsonparse_get_value_as_int(&jps);
						os_printf("Version parsed: %d\n", version);
						break;
					default:
						break;
				}
				break;

			case JSON_TYPE_ERROR:
				os_printf("JSON parse Error.\n");
				break;

			default:
				// os_printf("Unknown type: %d\n", json_type);
				break;
		}
	}

	// Check version, if changed, start upgrade process.
	if(version > Stats::firmware_version){
		App::inst().gui.log("New firmware available");
		auto e = os_strstr(path, "/version");
		ASSERT(e)
		std::array<char, 256> buf;
		unsigned dir_len = e - path;
		ASSERT(dir_len < buf.size())
		os_strncpy(buf.begin(), path, dir_len);
		buf[dir_len] = '\0';

		this->start_upgrade(ip, host, buf.begin());
		return;
	}else{
		App::inst().gui.log("Current firmware is up to date");
	}

	// Version has not changed, continue updating.
	this->start_time_sync();
}

void ICACHE_FLASH_ATTR Updater::handle_location_by_ip(esp::string_view body){
	jsonparse_state jps;

	jsonparse_setup(&jps, body.data(), body.size());

	enum class Field{
		UNKNOWN,
		COUNTRY,
		CITY,
		TIMEZONE
	} field = Field::UNKNOWN;
	int object_level = 0;

	decltype(Stats::country) country;
	decltype(Stats::city) city;
	decltype(Stats::timezone) timezone;

	while(auto json_type = jsonparse_next(&jps)){
		switch (json_type){
			case JSON_TYPE_OBJECT:
				++object_level;
				break;
			case '}':
				--object_level;
				break;

			case JSON_TYPE_PAIR_NAME:
				field = Field::UNKNOWN;
				if(object_level == 1){
					if(jsonparse_strcmp_value(&jps, "country") == 0){
						field = Field::COUNTRY;
					}else if(jsonparse_strcmp_value(&jps, "city") == 0){
						field = Field::CITY;
					}else if(jsonparse_strcmp_value(&jps, "timezone") == 0){
						field = Field::TIMEZONE;
					}
				}
				break;

			case JSON_TYPE_STRING:
				switch(field){
					case Field::COUNTRY:
						jsonparse_copy_value(&jps, country.begin(), country.size());
						os_printf("Country parsed: %s\n", country.begin());
						break;
					case Field::CITY:
						jsonparse_copy_value(&jps, city.begin(), city.size());
						os_printf("City parsed: %s\n", city.begin());
						break;
					case Field::TIMEZONE:
						jsonparse_copy_value(&jps, timezone.begin(), timezone.size());
						os_printf("Timezone parsed: %s\n", timezone.begin());
						break;
					default:
						break;
				}
				break;

			case JSON_TYPE_ERROR:
				os_printf("JSON parse Error.\n");
				break;

			default:
				// os_printf("Unknown type: %d\n", json_type);
				break;
		}
	}

	// Null-terminate text buffers, just in case.
	*(country.rbegin()) = '\0';
	*(city.rbegin()) = '\0';
	*(timezone.rbegin()) = '\0';

	App::inst().gui.log("Location is");
	App::inst().gui.log(" %s/%s", city.begin(), country.begin());

	App::inst().stats.country = country;
	App::inst().stats.city = city;
	App::inst().stats.timezone = timezone;

	App::inst().gui.log("Getting time for");
	App::inst().gui.log(" %s", timezone.begin());

	std::array<char, 1024> url;
	int n = os_snprintf(url.begin(), url.size(), current_time_url, timezone.begin());
	if(n < 0 || unsigned(n) >= url.size()){
		App::inst().gui.log("preparing URL failed");
		this->report_stats();
		return;
	}

	this->state = State::CURRENT_TIME;
	this->current_time_fetch_start_timestamp_ticks = esp::get_cpu_ticks();
	this->get(url.begin());
}

// WORKAROUND: jsonparse version in NONOS SDK 3 cannot handle null value in JSON, it stops parsing.
//             So, we replace null values with just empty string.
namespace{
int jsonparse_skip_colon_and_whitespaces(jsonparse_state& jps){
	int pos = jps.pos;
	bool colon_skept = false;
	for(; pos != jps.len; ++pos){
		switch(jps.json[pos]){
			case ':':
				if(colon_skept){
					return pos;
				}
				colon_skept = true;
				break;
			case ' ':
			case '\n':
			case '\r':
			case '\t':
				break;
			default:
				return pos;
		}
	}
	return pos;
}

void jsonparse_replace_null_value_with_empty_string(jsonparse_state& jps){
	int pos = jsonparse_skip_colon_and_whitespaces(jps);

	const char* p = "null";
	while(*p != '\0' && pos != jps.len && jps.json[pos] != '\0' && *p == jps.json[pos]){
		++pos;
		++p;
	}

	if(*p == '\0'){
		const_cast<char*>(jps.json)[pos - 4] = '"';
		const_cast<char*>(jps.json)[pos - 3] = '"';
		const_cast<char*>(jps.json)[pos - 2] = ' ';
		const_cast<char*>(jps.json)[pos - 1] = ' ';
	}
}
}

void ICACHE_FLASH_ATTR Updater::handle_current_time(esp::string_view body){
	uint32_t latency_ms = (esp::get_cpu_ticks() - this->current_time_fetch_start_timestamp_ticks) / (esp::get_cpu_ticks_per_second() / 1000);

	jsonparse_state jps;

	jsonparse_setup(&jps, body.data(), body.size());

	enum class Field{
		UNKNOWN,
		FORMATTED
	} field = Field::UNKNOWN;
	int object_level = 0;

	std::array<char, 128> formatted = {{0}};

	while(auto json_type = jsonparse_next(&jps)){
		switch (json_type){
			case JSON_TYPE_OBJECT:
				// App::inst().gui.log("{");
				++object_level;
				break;
			case '}':
				// App::inst().gui.log("}");
				--object_level;
				break;

			case JSON_TYPE_PAIR_NAME:
				field = Field::UNKNOWN;
				// App::inst().gui.log(jps.json + jps.vstart);
				if(object_level == 1){
					if(jsonparse_strcmp_value(&jps, "formatted") == 0){
						// App::inst().gui.log("formatted found!");
						field = Field::FORMATTED;
					}
				}
				jsonparse_replace_null_value_with_empty_string(jps);
				break;

			case JSON_TYPE_STRING:
				switch(field){
					case Field::FORMATTED:
						jsonparse_copy_value(&jps, formatted.begin(), formatted.size());
						// App::inst().gui.log("Formatted=%s", formatted.begin());
						break;
					default:
						// App::inst().gui.log(jps.json + jps.vstart);
						break;
				}
				break;

			case JSON_TYPE_ERROR:
				App::inst().gui.log("JSON parse error.");
				os_printf("JSON parse Error.\n");
				break;

			case ',':
			case ':':
			case '0':
				break;
			default:
				App::inst().gui.log("Unkown parse type %d", json_type);
				// os_printf("Unknown type: %d\n", json_type);
				break;
		}
	}

	if(formatted[0] == 0){
		// timezonedb.com error, no "formatted" time string present in response.
		App::inst().gui.log("timezonedb.com failed.");
		App::inst().gui.log("Time is not updated.");

		// for(const char* p = body; p < body + body_len; p += 32){
		// 	App::inst().gui.log(p);
		// }
	}else{
		// Null-terminate text buffers, just in case.
		*(formatted.rbegin()) = '\0';

		// Set all non-digit characters to '\0'.
		for(auto& c : formatted){
			if(c < '0' || '9' < c){
				c = '\0';
			}
		}

		// Parse time string and set time to calendar.
		auto p = formatted.begin();

		uint32_t year = atoi(p);
		p += 5;
		uint8_t month = atoi(p);
		p += 3;
		uint8_t day = atoi(p);
		p += 3;
		uint8_t hours = atoi(p);
		p += 3;
		uint8_t minutes = atoi(p);
		p += 3;
		uint8_t seconds = atoi(p);

		App::inst().gui.log("Time got: %d-%02d-%02d %02d:%02d:%02d", year, month, day, hours, minutes, seconds);

		App::inst().clock.reset(); // Clear subsecond time.

	//	os_printf("Parsed time: %d-%d-%d %d:%d:%d\n", year, month, day, hours, minutes, seconds);

		App::inst().clock.calendar.set(year, month, day, hours, minutes, seconds);

	//	os_printf("time fetch latency = %dms\n", latency_ms / 2);

		App::inst().gui.log("Latency = %d ms", latency_ms);

		// Add fetch latency / 2.
		for(unsigned i = 0; i != (latency_ms / 2) / 1000; ++i){
			App::inst().clock.calendar.addSecond();
		}
	}

	this->report_stats();
}

void ICACHE_FLASH_ATTR Updater::schedule_update(){
	this->timer.arm(sync_period_ms_c);

	this->state = State::UPDATE_SCHEDULED;
	App::inst().gui.log("Next sync in %d minutes", sync_period_ms_c / 1000 / 60);
}


void ICACHE_FLASH_ATTR Updater::on_connected_to_network(){
	if(this->state == State::IDLE){
		this->update();
	}
}

void ICACHE_FLASH_ATTR Updater::report_stats(){
	std::array<char, 1024> body;

	auto mac = WiFi::get_mac_address();

	int len = os_snprintf(
			body.begin(),
			body.size(),
	 		"{"
				"\"mac\":\"%s\","
			 	"\"firmware_version\":%d,"
				"\"uptime_sec\":%d,"
				"\"country\":\"%s\","
				"\"city\":\"%s\","
				"\"timezone\":\"%s\""
			"}",
			mac.begin(),
			Stats::firmware_version,
			App::inst().stats.get_uptime(),
			App::inst().stats.country.begin(),
			App::inst().stats.city.begin(),
			App::inst().stats.timezone.begin()
		);
	
	ASSERT(len >= 0)
	ASSERT(size_t(len) < body.size())

	os_printf("uptime = %d\n", App::inst().stats.get_uptime());

	esp::string body_s(body.data(), len);

	// os_printf("POST request body = \n");
	// os_printf("%s\n", body_s.c_str());

	App::inst().gui.log("Reporting status to server");
	this->state = State::REPORT_STATS;
	this->post(
			report_statistics_url,
			std::move(body_s)
		);
}

void ICACHE_FLASH_ATTR Updater::start_upgrade(std::array<uint8_t, 4> ip, const char* host, const char* dir){
	App::inst().gui.log("Upgrading");

	if(!this->upgrade_info){
		this->upgrade_info = reinterpret_cast<upgrade_server_info*>(os_zalloc(sizeof(upgrade_server_info)));
	}
	os_memcpy(this->upgrade_info->ip, ip.begin(), ip.size());
	this->upgrade_info->port = 80;
	this->upgrade_info->pespconn = reinterpret_cast<espconn*>(this);
	this->upgrade_info->check_times = 120000; // set timeout
	this->upgrade_info->check_cb = &upgrade_callback;

	const unsigned url_buf_size = 2048;

	if(!this->upgrade_info->url){
		this->upgrade_info->url = reinterpret_cast<uint8_t*>(os_zalloc(url_buf_size));
	}
	os_snprintf(
			reinterpret_cast<char*>(this->upgrade_info->url),
			url_buf_size,
			"GET %s/eagle.app.flash.bin HTTP/1.1\r\n"
					"Host: %s\r\n"
					"Connection: Keep-alive\r\n"
					"Cache-Control: no-cache\r\n"
					"\r\n"
				,
			dir,
			host
		);

	os_printf("Upgrade HTTP request:\n%s\n", this->upgrade_info->url);

	system_upgrade_start(this->upgrade_info);

	this->state = State::FIRMWARE_UPGRADE;
}

void ICACHE_FLASH_ATTR Updater::upgrade_callback(void* arg){
	auto upgrade_info = reinterpret_cast<upgrade_server_info*>(arg);

	if(upgrade_info->upgrade_flag){
		// Upgrade successful.

		// Save current uptime to restore it after reboot.
		App::inst().settings.initial_uptime = App::inst().stats.get_uptime();
		App::inst().settings.save();

		// Reboot device to boot to an upgraded firmware.
		App::inst().gui.log("Upgrade succeeded, rebooting");
		os_printf("Upgrade successful! rebooting.\n");
		system_upgrade_reboot();
	}else{
		// Upgrade failed for whatever reason. Schedule normal update procedure for later time.
		App::inst().gui.log("Upgrade failed");
		os_printf("Upgrade failed!\n");
		auto& updater = *reinterpret_cast<Updater*>(upgrade_info->pespconn);
		updater.start_time_sync();
	}
}

void ICACHE_FLASH_ATTR Updater::start_time_sync(){
	App::inst().gui.log("Getting location by IP");
	this->state = State::LOCATION_BY_IP;
	this->get(location_by_ip_url);
}
