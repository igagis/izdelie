#pragma once

extern"C"{
#include <c_types.h>
#include <ip_addr.h>
#include <espconn.h>
#include <os_type.h>
#include <upgrade.h>
}

#include "esp/httpc/request.hpp"

#include <array>

#include "Timer.hpp"

class Updater{
	std::unique_ptr<esp::httpc::request> http_req;

	static void callback(void* user_data, const esp::httpc::request& req);

	enum class State{
		IDLE,
		FIRMWARE_UPGRADE,
		UPDATE_SCHEDULED,
		FIRMWARE_URL_1,
		FIRMWARE_URL_2,
		LOCATION_BY_IP,
		CURRENT_TIME,
		REPORT_STATS,

		ENUM_SIZE
	};
	State state = State::IDLE;

	void get(esp::string_view url);
	void post(esp::string_view url, esp::string&& body);

	void handle_firmware_version(
			esp::string_view body,
			std::array<uint8_t, 4> ip,
			const char* host,
			const char* path
		);
	void handle_location_by_ip(esp::string_view body);
	void handle_current_time(esp::string_view body);

	void update();

	void schedule_update();
	void report_stats();

	local_timer_t<Updater> timer;

	uint32_t current_time_fetch_start_timestamp_ticks;

	upgrade_server_info* upgrade_info = nullptr;

	void start_upgrade(std::array<uint8_t, 4> ip, const char* host, const char* dir);

	static void upgrade_callback(void *arg);

	void start_time_sync();

public:
	Updater();

	void on_connected_to_network();
};
