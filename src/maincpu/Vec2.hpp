#pragma once

template <class T> class Vec2{
public:
	T x, y;

	Vec2(T x, T y) :
			x(x),
			y(y)
	{}

	T& operator[](unsigned i)noexcept{	
		static_assert(&Vec2::x == 0, "Vec2 x location is wrong");
		static_assert(&Vec2::y == sizeof(int), "Vec2 y location is wrong");
		return *(reinterpret_cast<T*>(this) + i);
	}

	const T& operator[](unsigned i)const noexcept{
		return *(reinterpret_cast<T*>(this) + i);
	}

	Vec2 operator+(const Vec2& v)const noexcept{
		return Vec2(this->x + v.x, this->y + v.y);
	}

	Vec2 operator-(const Vec2& v)const noexcept{
		return Vec2(this->x - v.x, this->y - v.y);
	}

	Vec2 operator/(T s)const noexcept{
		return Vec2(this->x / s, this->y / s);
	}

	template <class Y> operator Vec2<Y>(){
		return Vec2<Y>(this->x, this->y);
	}
};

typedef Vec2<int> Vec2i;
typedef Vec2<unsigned> Vec2u;

static_assert(sizeof(Vec2i) == 2 * sizeof(int), "Vec2i size mismatch");
static_assert(sizeof(Vec2u) == 2 * sizeof(unsigned), "Vec2u size mismatch");
