#include "WiFi.hpp"

extern"C"{
#include <c_types.h>
#include <user_interface.h>
#include <osapi.h>
}

#include <array>

#include "App.hpp"

namespace{
static const uint32_t timeout_ms = 20000;

std::array<char, 8> hostname ICACHE_RODATA_ATTR = {{'i', 'z', 'd', 'e', 'l', 'i', 'e', '\0'}};

bool is_connected = false;

WiFi* inst = nullptr;
}

WiFi::Listener* WiFi::listener = nullptr;

ICACHE_FLASH_ATTR WiFi::WiFi() :
	timeout_timer(*this, &WiFi::on_timeout)
{
	inst = this;
	wifi_station_ap_number_set(5); // Save up to 5 connection credentials.
	wifi_station_set_hostname(hostname.begin()); // argument is not const, so have to use array.
	wifi_set_opmode_current(STATION_MODE);
	wifi_set_event_handler_cb(&wifi_callback);
	if(!wifi_station_set_reconnect_policy(true)){
		os_printf("FAILED to enable WiFi auto-reconnect.\n");
	}
}

void ICACHE_FLASH_ATTR WiFi::wifi_callback(System_Event_t* e){
	// os_printf("%s: %d\n", __FUNCTION__, e->event);
	os_printf("WiFi::wifi_callback(): enter\n");
	esp::print_current_stack_usage();

	switch(e->event){
		case EVENT_STAMODE_CONNECTED:
			os_printf(
					"connect to ssid %s, channel %d\n",
					e->event_info.connected.ssid,
					e->event_info.connected.channel
				);

			is_connected = true;

			App::inst().gui.log("WiFi connected to");
			App::inst().gui.log(" %s", e->event_info.connected.ssid);

			ASSERT(inst)
			inst->timeout_timer.arm(timeout_ms);
			break;

		case EVENT_STAMODE_DISCONNECTED:
			inst->timeout_timer.disarm();
			os_printf(
					"disconnect from ssid %s, reason %d\n",
					e->event_info.disconnected.ssid,
					e->event_info.disconnected.reason
				);

			if(is_connected){
				App::inst().gui.log("WiFi disconnected from");
			}else{
				App::inst().gui.log("WiFi could not connect to");
			}
			App::inst().gui.log(" %s", e->event_info.connected.ssid);

			is_connected = false;
			break;

		case EVENT_STAMODE_GOT_IP:
			inst->timeout_timer.disarm();
			os_printf(
					"ip:" IPSTR ", mask:" IPSTR ", gw:" IPSTR "\n",
					IP2STR(&e->event_info.got_ip.ip),
					IP2STR(&e->event_info.got_ip.mask),
					IP2STR(&e->event_info.got_ip.gw)
				);
			os_printf("WiFi::wifi_callback(): got IP\n");
			esp::print_current_stack_usage();

			App::inst().gui.log("WiFi got IP: " IPSTR, IP2STR(&e->event_info.got_ip.ip));

			App::inst().updater.on_connected_to_network();
			break;

		case EVENT_STAMODE_AUTHMODE_CHANGE:
			os_printf(
				"WiFi::wifi_callback(): mode: %d -> %d\n",
				e->event_info.auth_change.old_mode,
				e->event_info.auth_change.new_mode
			);
			App::inst().gui.log("WiFi authmode change");
			break;
		default:
			os_printf("WiFi::wifi_callback(): unknown event\n");
			ASSERT(false)
			break;
	}

	esp::print_current_stack_usage();
	os_printf("WiFi::wifi_callback(): exit\n");
}

void ICACHE_FLASH_ATTR WiFi::reconnect(){
	wifi_station_disconnect();
	wifi_station_connect();
}

void ICACHE_FLASH_ATTR WiFi::on_timeout(){
	App::inst().gui.log("WiFi connect timeout");
	WiFi::reconnect();
}

void ICACHE_FLASH_ATTR WiFi::connect(const char* ssid, const char* password){
	wifi_station_disconnect();

	station_config config;

	// It is very important to clear all struct fields, otherwise there can be failures while connecting to WiFi.
	os_memset(&config, 0, sizeof(config));

	config.bssid_set = 0;
	os_memcpy(&config.ssid, ssid, std::min(os_strlen(ssid) + 1, 32));
	os_memcpy(&config.password, password, std::min(os_strlen(password) + 1, 64));

	wifi_station_set_config(&config);

	if(*ssid == '\0'){
		App::inst().gui.log("WiFi SSID is empty");
		return;
	}

	App::inst().gui.log("WiFi connecting to");
	App::inst().gui.log("  %s", ssid);

	// os_printf("wifi login/password: '%s' / '%s' \n", reinterpret_cast<char*>(&config.ssid), reinterpret_cast<char*>(&config.password));

	wifi_station_connect();
}

void ICACHE_FLASH_ATTR WiFi::scan(WiFi::Listener& l){
	if(listener != nullptr){
		os_printf("ERROR: WiFi::scan while other wifi operation is in progress");
		return;
	}
	listener = &l;

	App::inst().gui.log("WiFi scanning for networks");
	wifi_station_scan(nullptr, &wifi_scan_callback);
}

void ICACHE_FLASH_ATTR WiFi::wifi_scan_callback(void* arg, STATUS status){
	ASSERT(listener)
	if(status == OK){
		listener->on_scan_complete(reinterpret_cast<bss_info*>(arg));
	}else{
		listener->on_scan_complete(nullptr);
	}
	listener = nullptr;
}

std::array<char, 3 * 6> ICACHE_FLASH_ATTR WiFi::get_mac_address(){
	std::array<char, 3 * 6> ret;
	std::array<uint8, 6> mac;

	if(wifi_get_macaddr(STATION_IF, mac.begin())){
		os_snprintf(ret.begin(), ret.size(), "%02x:%02x:%02x:%02x:%02x:%02x", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
	}else{
		os_snprintf(ret.begin(), ret.size(), "ERROR");
	}
	*ret.rbegin() = '\0'; // Null-terminate just in case.

	return ret;
}
