#pragma once

extern"C"{
#include <user_interface.h>
}

#include <array>

#include "Timer.hpp"

class WiFi{
	static void wifi_callback(System_Event_t *evt);

	static void wifi_scan_callback(void* arg, STATUS status);
public:

	class Listener{
	public:
		/**
		 * @param bss - linked list of wifi stations. If nullptr, then scanning has failed.
		 */
		virtual void on_scan_complete(bss_info* bss) = 0;
	};

private:
	static Listener* listener;

	local_timer_t<WiFi> timeout_timer;

	void on_timeout();
public:

	WiFi();

	static void reconnect();

	static void connect(const char* ssid, const char* password);

	static void scan(Listener& l);

	static std::array<char, 3 * 6> get_mac_address();
};
