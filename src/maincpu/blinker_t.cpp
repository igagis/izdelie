#include "blinker_t.hpp"

extern"C"{
#include <c_types.h>
}

#include <cmath>


ICACHE_FLASH_ATTR two_state_blinker_t::two_state_blinker_t(uint8_t dots_state_1, uint8_t dots_state_2) :
		dots_state_1(dots_state_1),
		dots_state_2(dots_state_2)
{}


blinker_t::ret_t ICACHE_FLASH_ATTR two_state_blinker_t::tick(){
	this->dots_on = !this->dots_on;
	return {
		500,
		this->dots_on ? this->dots_state_1 : this->dots_state_2
	};
}

/*
  y
    /\
    |
   A|------__            __
    |     /  \          /  \
    |    /    \        /    \
    |   |      |      |      |
    |   |      |      |      |      |
    |  /        \    /        \    /
   0|_/          \__/          \__/
----|---------------------------------------> x
    |             T

y = A/2(-cos(2pi*t/T) + 1), y => [0:A]

1 - 2y/A = cos(2pi*t/T)

acos(1-2y/A) = 2pi*t/T

t = T/2pi * acos(1-2y/A)

*/

namespace{

constexpr float A = 8;

constexpr float func(float y, float T){
	using std::acos;
	return T / (2 * acos(-1)) * acos(1.0f - 2 * y / A);
}

std::array<uint32_t, 4> sine_1_sec_intervals = {{
	uint32_t((func(0.5f, 1.0f) - func(0.0f, 1.0f)) * 1000 * 2),
	uint32_t((func(1.5f, 1.0f) - func(0.5f, 1.0f)) * 1000),
	uint32_t((func(2.5f, 1.0f) - func(1.5f, 1.0f)) * 1000),
	uint32_t((func(4.0f, 1.0f) - func(2.5f, 1.0f)) * 1000)
}};

std::array<uint32_t, 4> sine_2_sec_intervals = {{
	uint32_t((func(0.5f, 2.0f) - func(0.0f, 2.0f)) * 1000 * 2),
	uint32_t((func(1.5f, 2.0f) - func(0.5f, 2.0f)) * 1000),
	uint32_t((func(2.5f, 2.0f) - func(1.5f, 2.0f)) * 1000),
	uint32_t((func(4.0f, 2.0f) - func(2.5f, 2.0f)) * 1000)
}};

}

ICACHE_FLASH_ATTR sine_blinker_t::sine_blinker_t(std::array<uint32_t, 4> intervals) :
		intervals(intervals)
{}

ICACHE_FLASH_ATTR sine_1_sec_blinker_t::sine_1_sec_blinker_t() :
		sine_blinker_t(sine_1_sec_intervals)
{}

ICACHE_FLASH_ATTR sine_2_sec_blinker_t::sine_2_sec_blinker_t() :
		sine_blinker_t(sine_2_sec_intervals)
{}

blinker_t::ret_t ICACHE_FLASH_ATTR sine_blinker_t::tick(){
	unsigned interval_index;

	// sine oscillations are symmetrical relatively to equilibrium point,
	// so we store intervals for quarter period only
	if((this->dot_pos / this->intervals.size()) % 2 == 0){
		interval_index = this->dot_pos % this->intervals.size();
	}else{
		interval_index = this->intervals.size() - this->dot_pos % this->intervals.size() - 1;
	}

	blinker_t::ret_t ret = {
		this->intervals[interval_index],
		uint8_t(1 << this->dot_pos)
	};

	if(this->direction_left){
		if(this->dot_pos == 7){
			this->direction_left = false;
			--this->dot_pos;
		}else{
			++this->dot_pos;
		}
	}else{
		if(this->dot_pos == 0){
			this->direction_left = true;
			++this->dot_pos;
		}else{
			--this->dot_pos;
		}
	}

	return ret;
}
