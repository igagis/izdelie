#pragma once

#include <cstdint>
#include <array>

class blinker_t{
public:
	struct ret_t{
		uint32_t timeout;
		uint8_t dots;
	};

	// return milliseconds till next tick
	virtual ret_t tick() = 0;
};


class two_state_blinker_t : public blinker_t{
	uint8_t dots_state_1;
	uint8_t dots_state_2;
	bool dots_on = false;
public:
	two_state_blinker_t(uint8_t dots_state_1, uint8_t dots_state_2);

	ret_t tick()override;
};


// The model is that we have 9 equally distant dots. The middle dot is invisible.
//
//   _/\_  _/\_     _/\_  _/\_
//  |    ||    |   |    ||    |
//  | 1  || 2  |   | 3  || 4  |
//  |    ||    |   |    ||    |
//  |o__o||o__o| x |o__o||o__o|
//   ||||  ||||     ||||  ||||
//
//
//   o  o  o  o  x  o  o  o  o
//
//   7  6  5  4     3  2  1  0
//
class sine_blinker_t : public blinker_t{
	uint8_t dot_pos = 0; // from [0:7]
	bool direction_left = true;

	std::array<uint32_t, 4> intervals;
public:
	sine_blinker_t(std::array<uint32_t, 4> intervals);

	ret_t tick()override;
};

class sine_1_sec_blinker_t : public sine_blinker_t{
public:
	sine_1_sec_blinker_t();
};

class sine_2_sec_blinker_t : public sine_blinker_t{
public:
	sine_2_sec_blinker_t();
};
