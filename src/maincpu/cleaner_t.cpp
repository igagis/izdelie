#include "cleaner_t.hpp"

extern"C"{
#include <c_types.h>
}

#include "App.hpp"


ICACHE_FLASH_ATTR phased_cleaner_t::phased_cleaner_t(unsigned start_phase, unsigned end_phase, uint32_t delay) :
		start_phase(start_phase),
		end_phase(end_phase),
		delay(delay)
{}

void ICACHE_FLASH_ATTR phased_cleaner_t::reset(){
	this->cur_phase = this->start_phase;
	this->digit = this->cur_phase % 2 == 0 ? 0 : max_digit;
}

ICACHE_FLASH_ATTR cleaner_asc_desc_t::cleaner_asc_desc_t(unsigned start_phase, unsigned end_phase, uint32_t delay) :
		phased_cleaner_t(start_phase, end_phase, delay)
{}

ICACHE_FLASH_ATTR cleaner_in_out_t::cleaner_in_out_t(unsigned start_phase, unsigned end_phase, uint32_t delay, std::array<bool, 4> direction) :
		phased_cleaner_t(start_phase, end_phase, delay),
		direction(direction)
{}

namespace{
std::array<uint8_t, 10> in_out_table = {{
	1, 0, 2, 9, 3, 8, 4, 7, 5, 6
}};
}

uint8_t ICACHE_FLASH_ATTR cleaner_in_out_t::get_digit(unsigned num, uint8_t d){
	return this->direction[num] ? in_out_table[d] : in_out_table[max_digit - d];
}

// return milliseconds
uint32_t ICACHE_FLASH_ATTR phased_cleaner_t::step(){
	if(this->cur_phase % 2 == 0){
		if(this->digit > max_digit){
			++this->cur_phase;
			if(this->cur_phase == this->end_phase){
				this->reset();
				return 0;
			}
			this->digit = max_digit - 1;
		}
	}

	uint8_t dots = this->cur_phase % 2 == 0 ? 0xff : 0;

	auto d0 = this->get_digit(0, this->digit);
	auto d1 = this->get_digit(1, this->digit);
	auto d2 = this->get_digit(2, this->digit);
	auto d3 = this->get_digit(3, this->digit);

	App::inst().nixieBoard.setDisplay(
			uint16_t(d0) << 12 | uint16_t(d1) << 8 | uint16_t(d2) << 4 | uint16_t(d3),
			dots,
			pack_brightness(App::inst().luminanceManager.get_brightness())
		);
	
	if(this->cur_phase % 2 == 0){
		++this->digit;
	}else{
		if(this->digit == 0){
			++this->cur_phase;
			if(this->cur_phase == this->end_phase){
				this->reset();
				return 0;
			}
			this->digit = 1;
		}else{
			--this->digit;
		}
	}

	return this->delay;
}
