#pragma once

#include <cstdint>
#include <cstddef>

#include <array>

class cleaner_t{
protected:
	const unsigned max_digit = 9;
	unsigned digit;

	cleaner_t(){}
public:
	virtual uint32_t step() = 0;

	virtual void reset() = 0;

	void* operator new(size_t size, void* p){
		return p;
	}
};

class phased_cleaner_t : public cleaner_t{
protected:
	unsigned cur_phase;
	unsigned start_phase;
	unsigned end_phase;
	uint32_t delay;

	phased_cleaner_t(unsigned start_phase, unsigned end_phase, uint32_t delay);

	void reset()override;

	// return milliseconds
	uint32_t step()override;

	virtual uint8_t get_digit(unsigned num, uint8_t d){
		return d;
	}
};

class cleaner_asc_desc_t : public phased_cleaner_t{
	bool asc = true;
public:
	cleaner_asc_desc_t(unsigned start_phase, unsigned end_phase, uint32_t delay);
};


class cleaner_in_out_t : public phased_cleaner_t{
	std::array<bool, 4> direction;
public:
	cleaner_in_out_t(unsigned start_phase, unsigned end_phase, uint32_t delay, std::array<bool, 4> direction);

	uint8_t get_digit(unsigned num, uint8_t d)override;
};
