/**
 * This HTTP client code is based on the original code by Martin d'Allens <martin.dallens@gmail.com>.
 * The original License notice from Martin d'Allens:
 * 
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * Martin d'Allens <martin.dallens@gmail.com> wrote this file. As long as you retain
 * this notice you can do whatever you want with this stuff. If we meet some day,
 * and you think this stuff is worth it, you can buy me a beer in return.
 * ----------------------------------------------------------------------------
 */

// FIXME: sprintf->snprintf everywhere.

extern"C"{
#include "osapi.h"
#include "user_interface.h"
#include "espconn.h"
#include "mem.h"
#include "limits.h"
}

#include "../util.hpp"

#include "request.hpp"

#include <cstdlib>

// Uncomment to enable HTTPS.
// #define HTTPS_SUPPORT

using namespace esp::httpc;

namespace{
static const size_t max_http_response_size = 5000;
static const uint32_t connection_timeout_ms = 20000;
}

namespace{
int ICACHE_FLASH_ATTR esp_isupper(char c){
    return (c >= 'A' && c <= 'Z');
}
}

namespace{
int ICACHE_FLASH_ATTR esp_isalpha(char c){
    return ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'));
}
}

namespace{
int ICACHE_FLASH_ATTR esp_isspace(char c){
    return (c == ' ' || c == '\t' || c == '\n' || c == '\12');
}
}

namespace{
int ICACHE_FLASH_ATTR esp_isdigit(char c){
    return (c >= '0' && c <= '9');
}
}

/*
 * Convert a string to a long integer.
 *
 * Ignores `locale' stuff.  Assumes that the upper and lower case
 * alphabets and digits are each contiguous.
 */
namespace{
long ICACHE_FLASH_ATTR esp_strtol(const char* nptr, char** endptr, int base){
	const char *s = nptr;
	unsigned long acc;
	int c;
	unsigned long cutoff;
	int neg = 0, any, cutlim;

	/*
	 * Skip white space and pick up leading +/- sign if any.
	 * If base is 0, allow 0x for hex and 0 for octal, else
	 * assume decimal; if base is already 16, allow 0x.
	 */
	do {
		c = *s++;
	} while (esp_isspace(c));
	if (c == '-') {
		neg = 1;
		c = *s++;
	} else if (c == '+')
		c = *s++;
	if ((base == 0 || base == 16) &&
	    c == '0' && (*s == 'x' || *s == 'X')) {
		c = s[1];
		s += 2;
		base = 16;
	} else if ((base == 0 || base == 2) &&
	    c == '0' && (*s == 'b' || *s == 'B')) {
		c = s[1];
		s += 2;
		base = 2;
	}
	if (base == 0)
		base = c == '0' ? 8 : 10;

	/*
	 * Compute the cutoff value between legal numbers and illegal
	 * numbers. That is the largest legal value, divided by the
	 * base. An input number that is greater than this value, if
	 * followed by a legal input character, is too big. One that
	 * is equal to this value may be valid or not; the limit
	 * between valid and invalid numbers is then based on the last
	 * digit. For instance, if the range for longs is
	 * [-2147483648..2147483647] and the input base is 10,
	 * cutoff will be set to 214748364 and cutlim to either
	 * 7 (neg==0) or 8 (neg==1), meaning that if we have accumulated
	 * a value > 214748364, or equal but the next digit is > 7 (or 8),
	 * the number is too big, and we will return a range error.
	 *
	 * Set any if any `digits' consumed; make it negative to indicate
	 * overflow.
	 */
	cutoff = neg ? -(unsigned long)LONG_MIN : LONG_MAX;
	cutlim = cutoff % (unsigned long)base;
	cutoff /= (unsigned long)base;
	for (acc = 0, any = 0;; c = *s++) {
		if (esp_isdigit(c))
			c -= '0';
		else if (esp_isalpha(c))
			c -= esp_isupper(c) ? 'A' - 10 : 'a' - 10;
		else
			break;
		if (c >= base)
			break;
		if (any < 0 || acc > cutoff || (acc == cutoff && c > cutlim))
			any = -1;
		else {
			any = 1;
			acc *= base;
			acc += c;
		}
	}
	if (any < 0) {
		acc = neg ? LONG_MIN : LONG_MAX;
//		errno = ERANGE;
	} else if (neg)
		acc = -acc;
	if (endptr != 0)
		*endptr = (char *)(any ? s - 1 : nptr);
	return (acc);
}
}

namespace{
size_t ICACHE_FLASH_ATTR chunked_decode(char* chunked, size_t size){
	char* src = chunked;
	char* end = chunked + size;
	size_t i, dst = 0;

	do
	{
		// [chunk-size]
		i = esp_strtol(src, nullptr, 16);
		os_printf("Chunk Size:%d\n", i);
		if (i <= 0)
			break;
		// [chunk-size-end-ptr]
		src = (char *)os_strstr(src, "\r\n") + 2;
		// [chunk-data]
		os_memmove(&chunked[dst], src, i);
		src += i + 2; /* CRLF */
		dst += i;
	} while (src < end);

	//
	// footer CRLF
	//

	/* decoded size */
	return dst;
}
}

void ICACHE_FLASH_ATTR request::conn_receive_callback(void* arg, char* buf, unsigned short len){
	ASSERT(arg)
	auto *conn = reinterpret_cast<espconn*>(arg);
	ASSERT(conn->reverse)
	auto req = reinterpret_cast<esp::httpc::request*>(conn->reverse);
	req->on_conn_receive(buf, len);
}

void ICACHE_FLASH_ATTR request::on_conn_receive(const char* buf, unsigned short len){
	os_printf("on_conn_receive(): invoked, len = %d\n", len);
	auto new_size = this->resp_buf.size() + len;
	if(new_size > max_http_response_size){
		os_printf("on_conn_receive(): response too long (%d)\n", new_size);
		this->resp_buf.clear(); // Discard the buffer to avoid using an incomplete response.
		this->disconnect();
		return; // The disconnect callback will be called.
	}

	os_printf("free heap = %d\n", system_get_free_heap_size());
	ASSERT(len)
	this->resp_buf.append(buf, len);
	ASSERT(!this->resp_buf.empty())
}

void ICACHE_FLASH_ATTR request::disconnect(){
	sint8 res;
#ifdef HTTPS_SUPPORT
	if(this->secure){
		res = espconn_secure_disconnect(&this->conn);
	}else
#endif
	{
		res = espconn_disconnect(&this->conn);
	}

	switch(res){
		case 0:
			// success
			break;
		case ESPCONN_ARG:
			os_printf("on_conn_receive(): espconn_disconnect() failed, could not find TCP connection\n");
			break;
		default:
			os_printf("on_conn_receive(): espconn_disconnect() failed, unknown error\n");
			break;
	}
}

void ICACHE_FLASH_ATTR request::conn_sent_callback(void* arg){
	ASSERT(arg)
	auto conn = reinterpret_cast<espconn*>(arg);
	ASSERT(conn->reverse)
	auto req = reinterpret_cast<esp::httpc::request*>(conn->reverse);
	req->on_conn_sent();
}

void ICACHE_FLASH_ATTR request::on_conn_sent(){
	if(this->request_header.empty()){
		// body sent
		this->body.clear();
		os_printf("on_conn_sent(): all sent\n");
		return;
	}else{
		// request header sent
		this->request_header.clear();
	}

	// The headers were sent, now send the contents.
	os_printf("on_conn_sent(): sending request body\n");
	sint8 res;
#ifdef HTTPS_SUPPORT
	if(this->secure){
		res = espconn_secure_send(
				&this->conn,
				reinterpret_cast<uint8_t*>(this->body.data()),
				this->body.size()
			);
	}else
#endif
	{
		res = espconn_send(
				&this->conn,
				reinterpret_cast<uint8_t*>(this->body.data()),
				this->body.size()
			);
	}

	switch(res){
		case 0:
			// success
			break;
		case ESPCONN_MEM:
			os_printf("on_conn_sent(): espconn_sent() failed, out of memory\n");
			break;
		case ESPCONN_ARG:
			os_printf("on_conn_sent(): espconn_sent() failed, could not find TCP connection\n");
			break;
		case ESPCONN_MAXNUM:
			os_printf("on_conn_sent(): espconn_sent() failed, sending buffer is full\n");
			break;
		case ESPCONN_IF:
			os_printf("on_conn_sent(): espconn_sent() failed, UDP data sending failed\n");
			break;
		default:
			os_printf("on_conn_sent(): espconn_sent() failed, unknown error\n");
			break;
	}
	
	os_printf("on_conn_sent(): sending body");
}

void ICACHE_FLASH_ATTR request::conn_connect_callback(void* arg){
	os_printf("request::conn_connect_callback(): enter\n");
	ASSERT(arg)
	auto conn = reinterpret_cast<espconn*>(arg);
	ASSERT(conn->reverse)
	auto req = reinterpret_cast<esp::httpc::request*>(conn->reverse);

	req->on_conn_connect();

	os_printf("request::conn_connect_callback(): exit\n");
}

void ICACHE_FLASH_ATTR request::on_conn_connect(){
	os_printf("on_conn_connect(): enter\n");
	esp::print_current_stack_usage();

	{
		sint8 res = espconn_regist_recvcb(&this->conn, &request::conn_receive_callback);
		switch(res){
			case 0:
				// success
				break;
			case ESPCONN_ARG:
				os_printf("on_conn_connect(): espconn_regist_recvcb() failed, could not find TCP connection\n");
				break;
			default:
				os_printf("on_conn_connect(): espconn_regist_recvcb() failed, unknown error\n");
				break;
		}
	}
	{
		sint8 res = espconn_regist_sentcb(&this->conn, &request::conn_sent_callback);
		switch(res){
			case 0:
				// success
				break;
			case ESPCONN_ARG:
				os_printf("on_conn_connect(): espconn_regist_sentcb() failed, could not find TCP connection\n");
				break;
			default:
				os_printf("on_conn_connect(): espconn_regist_sentcb() failed, unknown error\n");
				break;
		}
	}

	{
		esp::string_view method_sv;
		std::array<char, 32> post_headers;

		if(!this->body.empty()){ // If there is body, then this is a POST request.
			method_sv = "POST";
			os_sprintf(post_headers.data(), "Content-Length: %d\r\n", this->body.size());

			os_printf("on_conn_connect(): post_headers =\n");
			os_printf("%s\n", post_headers.data());
			os_printf("on_conn_connect(): body =\n");
			os_printf("%s\n", this->body.c_str());
		}else{
			method_sv = "GET";
			post_headers[0] = '\0';
		}

		esp::string_view format = 
				"%s %s HTTP/1.1\r\n"
				"Host: %s:%d\r\n"
				"Connection: close\r\n"
				"User-Agent: ESP8266\r\n"
				"%s"
				"%s"
				"\r\n"
			;

		esp::print_current_stack_usage(); // looks like no stack overflow here
		char buf[format.size() + method_sv.size() + this->path.size() + this->host.size() + 5 +
				this->headers.size() + post_headers.size()];
		esp::print_current_stack_usage();
		
		os_printf("on_conn_connect(): sizeof(buf) = %d\n", sizeof(buf));

		esp::print_current_stack_usage();

#ifdef DEBUG
		int len = 
#endif
		os_sprintf(
				buf,
				format.data(),
				method_sv.data(),
				this->path.c_str(),
				this->host.c_str(),
				this->port,
				this->headers.c_str(),
				post_headers.data()
			);
		ASSERT(len < int(sizeof(buf)))

		esp::print_current_stack_usage();
		os_printf("on_conn_connect(): &buf = %x\n", uintptr_t(buf));
		os_printf("on_conn_connect(): &this->conn = %x\n", uintptr_t(&this->conn));
		os_printf("on_conn_connect(): buf =\n");
		os_printf("%s\n", buf);

		esp::print_current_stack_usage();

		this->request_header = esp::string(buf);
		this->headers.clear();
	}

	os_printf("on_conn_connect(): sending request header\n");
	sint8 res;
#ifdef HTTPS_SUPPORT
	if(this->secure){
		res = espconn_secure_send(
				&this->conn,
				reinterpret_cast<uint8_t*>(this->request_header.data()),
				this->request_header.size()
			);
	}else
#endif
	{
		res = espconn_send(
				&this->conn,
				reinterpret_cast<uint8_t*>(this->request_header.data()),
				this->request_header.size()
			);
	}

	switch(res){
		case 0:
			// success
			break;
		case ESPCONN_MEM:
			os_printf("on_conn_connect(): espconn_send() failed, out of memory\n");
			break;
		case ESPCONN_ARG:
			os_printf("on_conn_connect(): espconn_send() failed, could not find TCP connection\n");
			break;
		case ESPCONN_MAXNUM:
			os_printf("on_conn_connect(): espconn_send() failed, sending buffer is full\n");
			break;
		case ESPCONN_IF:
			os_printf("on_conn_connect(): espconn_send() failed, sending UDP data failed\n");
			break;
		default:
			os_printf("on_conn_connect(): espconn_send() failed, unknown error\n");
			break;
	}
	
	esp::print_current_stack_usage();
	os_printf("on_conn_connect(): exit\n");
}

void ICACHE_FLASH_ATTR request::conn_disconnect_callback(void * arg){
	os_printf("Disconnected\n");

	ASSERT(arg)
	auto conn = reinterpret_cast<espconn*>(arg);

	ASSERT(conn->reverse)
	auto req = reinterpret_cast<esp::httpc::request*>(conn->reverse);

	req->on_conn_disconnect();
}

void ICACHE_FLASH_ATTR request::on_conn_disconnect(){
	os_printf("on_conn_disconnect(): enter\n");

	this->timeout_timer.disarm();

	// TODO: espconn_delete() always fails here with ESPCONN_ARG, perhaps it is not needed here?
	switch(espconn_delete(&this->conn)){
		case 0:
			// no error
			break;
		case ESPCONN_ARG:
			os_printf("on_conn_disconnect(): espconn_delete() failed, could not find transmission\n");
			break;
		case ESPCONN_INPROGRESS:
			os_printf("on_conn_disconnect(): espconn_delete() failed, transmission is in progress\n");
			break;
		default:
			os_printf("on_conn_disconnect(): espconn_delete() failed, unknown error\n");
			break;
	}

	if(this->resp.status_code != status::undefined){
		os_printf("on_conn_disconnect(): status code is already set: %d\n", unsigned(this->resp.status_code));
		this->on_done();
		return;
	}

	if(this->resp_buf.empty()){
		os_printf("on_conn_disconnect(): response buffer is empty, reporting error\n");
		this->resp.status_code = status::network_error;
		this->on_done();
		return;
	}

	esp::string_view resp_sv(this->resp_buf);

	// FIXME: make sure this is not a partial response, using the Content-Length header.

	esp::string_view http_1_0_sv("HTTP/1.0 ");
	esp::string_view http_1_1_sv("HTTP/1.1 ");

	if(
			resp_sv.find(http_1_0_sv) != 0 &&
			resp_sv.find(http_1_1_sv) != 0
		)
	{
		os_printf("on_conn_disconnect(): invalid version in %s\n", this->resp_buf.c_str());
		this->resp.status_code = status::network_error;
		this->on_done();
		return;
	}

	// cut away http version
	resp_sv = resp_sv.substr(http_1_1_sv.size());

	int http_status = atoi(resp_sv.data()); // TODO: get rid of atoi()?

	// cut away http status code
	resp_sv = resp_sv.substr(resp_sv.find('\n') + 1);

	esp::string_view rnrn("\r\n\r\n");

	esp::string_view headers_sv = resp_sv.substr(0, resp_sv.find(rnrn));

	if(http_status != 200){
		os_printf("on_conn_disconnect(): HTTP status code = %d\n", http_status);
		os_printf("on_conn_disconnect(): headers = \n");
		os_printf("%s\n", esp::string(headers_sv).c_str());
	}

	// cut away headers
	resp_sv = resp_sv.substr(headers_sv.size() + rnrn.size());

	// now resp_sv contains only body
	this->resp.body = resp_sv;

	esp::string_view transfer_encoding_sv("Transfer-Encoding: chunked");

	if(headers_sv.find(transfer_encoding_sv) != esp::string_view::npos){
		size_t body_size = chunked_decode(this->resp.body.data(), this->resp.body.size());
		if(body_size < this->resp.body.size()){
			this->resp.body = this->resp.body.substr(0, body_size);
		}
	}

	this->resp.status_code = status::ok;
	this->resp.http_status_code = http_status;

	this->on_done();
}

void ICACHE_FLASH_ATTR request::on_timeout(){
	os_printf("connection timeout hit\n");
	this->disconnect();
	this->resp.status_code = esp::httpc::status::timeout;
}

void ICACHE_FLASH_ATTR request::conn_error_callback(void *arg, sint8 error){
	ASSERT(arg)
	auto req = reinterpret_cast<esp::httpc::request*>(arg);
	req->on_conn_error(error);
}

void ICACHE_FLASH_ATTR request::on_conn_error(sint8 error){
	os_printf("on_conn_error(): disconnected with error: %d\n", error);
	this->on_conn_disconnect();
}

void ICACHE_FLASH_ATTR request::dns_callback(const char* hostname, ip_addr_t* addr, void* arg){
	os_printf("dns_callback(): enter, arg = %x\n", uintptr_t(arg));
	auto req = reinterpret_cast<esp::httpc::request*>(arg);

	req->on_dns(hostname, addr);
}

void ICACHE_FLASH_ATTR request::on_dns(const char* hostname, ip_addr_t* addr){
	if(addr == nullptr){
		os_printf("on_dns(): DNS failed for %s\n", hostname);
		this->resp.status_code = status::network_error;
		this->on_done();
		return;
	}
	
	os_printf("on_dns(): DNS found %s " IPSTR "\n", hostname, IP2STR(addr));

	os_memcpy(this->conn.proto.tcp->remote_ip, addr, 4);

	// save remote IP into the response structure
	ASSERT(this->resp.remote_ip.size() == 4)
	os_memcpy(this->resp.remote_ip.data(), addr, 4);

	switch(espconn_regist_connectcb(&this->conn, &request::conn_connect_callback)){
		case 0:
			// success
			break;
		case ESPCONN_ARG:
			os_printf("on_dns(): espconn_regist_connectcb() failed, could not find TCP connection\n");
			break;
		default:
			os_printf("on_dns(): espconn_regist_connectcb() failed, unknown error\n");
			break;
	}
	switch(espconn_regist_disconcb(&this->conn, &request::conn_disconnect_callback)){
		case 0:
			// success
			break;
		case ESPCONN_ARG:
			os_printf("on_dns(): espconn_regist_disconcb() failed, could not find TCP connection\n");
			break;
		default:
			os_printf("on_dns(): espconn_regist_disconcb() failed, unknown error\n");
			break;
	}
	switch(espconn_regist_reconcb(&this->conn, &request::conn_error_callback)){
		case 0:
			// success
			break;
		case ESPCONN_ARG:
			os_printf("on_dns(): espconn_regist_reconcb() failed, could not find TCP connection\n");
			break;
		default:
			os_printf("on_dns(): espconn_regist_reconcb() failed, unknown error\n");
			break;
	}

	sint8 res;
#ifdef HTTPS_SUPPORT
	if(req->secure){
		// set SSL buffer size
		if(!espconn_secure_set_size(ESPCONN_CLIENT, 5120)){
			os_printf("on_dns(): espconn_secure_set_size(): failed\n");
		}

		res = espconn_secure_connect(&this->conn);
	}else
#endif
	{
		res = espconn_connect(&this->conn);
	}

	switch(res){
		case 0:
			// success
			break;
		case ESPCONN_RTE:
			os_printf("on_dns(): espconn_connect(): failed, routing problem\n");
			break;
		case ESPCONN_MEM:
			os_printf("on_dns(): espconn_connect(): failed, out of memory\n");
			break;
		case ESPCONN_ISCONN:
			os_printf("on_dns(): espconn_connect(): failed, already connected\n");
			break;
		case ESPCONN_ARG:
			os_printf("on_dns(): espconn_connect(): failed, could not find TCP connection\n");
			break;
		default:
			os_printf("on_dns(): espconn_connect(): failed, unknown error\n");
			break;
	}

	os_printf("on_dns(): exit\n");
}

ICACHE_FLASH_ATTR request::request(void* user_data, decltype(callback) callback) :
		user_data(user_data),
		callback(callback),
		timeout_timer(*this, &request::on_timeout)
{
	ASSERT(this->callback)

	// zero out C structures just in case
	os_memset(&this->conn, 0, sizeof(this->conn));
	os_memset(&this->tcp, 0, sizeof(this->tcp));

	this->conn.type = ESPCONN_TCP;
	this->conn.state = ESPCONN_NONE;
	this->conn.proto.tcp = &this->tcp;
	this->conn.reverse = this;

	this->tcp.local_port = espconn_port();
	this->tcp.remote_port = this->port;
}

void ICACHE_FLASH_ATTR request::start(){
	os_printf("request::start(): enter\n");
	esp::print_current_stack_usage();

	ASSERT(!this->in_progress)
	this->in_progress = true;

	ip_addr_t addr;
	err_t error = espconn_gethostbyname(
			&this->conn, // It seems we don't need a real espconn pointer here, but pass the one we have. At least to provide user data for callback.
			this->host.c_str(),
			&addr,
			&request::dns_callback
		);

	switch(error){
		case ESPCONN_INPROGRESS:
			os_printf("request::start(): DNS pending\n");
			break;
		case ESPCONN_OK:
			// Already in the local names table (or hostname was an IP address), execute the callback ourselves.
			request::dns_callback(this->host.c_str(), &addr, this);
			break;
		default:
			if(error == ESPCONN_ARG){
				os_printf("request::start(): DNS arg error %s\n", this->host.c_str());
			}else{
				os_printf("request::start(): DNS error code %d\n", error);
			}
			request::dns_callback(this->host.c_str(), nullptr, this); // Handle all DNS errors the same way.
			break;
	}

	this->timeout_timer.arm(connection_timeout_ms);

	esp::print_current_stack_usage();
	os_printf("request::start(): exit\n");
}

void ICACHE_FLASH_ATTR request::set_url(esp::string_view url_sv){
	ASSERT(!this->in_progress)
	
	// FIXME: handle HTTP auth with http://user:pass@host/
	// FIXME: get rid of the #anchor part if present.

	esp::string_view http_sv("http://");
	esp::string_view https_sv("https://");

	bool is_http = url_sv.find(http_sv) == 0;
#ifdef HTTPS_SUPPORT
	bool is_https = url_sv.find(https_sv) == 0;
#endif

	if(is_http){
		// get rid of the protocol
		url_sv = url_sv.substr(http_sv.size());
		this->secure = false;
#ifdef HTTPS_SUPPORT
	}else if(is_https){
		this->secure = true;
		url_sv = url_sv.substr(https_sv.size());
#endif
	}else{
		os_printf("request::set_url(): unknown protocol");
		esp::abort();
	}

	auto path_pos = url_sv.find('/');
	esp::string_view host_port_sv = url_sv.substr(0, path_pos);
	this->path = url_sv.substr(path_pos);

	auto colon_pos = host_port_sv.find(':');
	this->host = host_port_sv.substr(0, colon_pos);
	esp::string_view port_sv = host_port_sv.substr(colon_pos).substr(1);

	if(this->host.empty()){
		os_printf("request::set_url(): host is empty");
		return;
	}

	if(!port_sv.empty()){
		this->port = atoi(port_sv.data()); // port_sv refers to url which is null-terminated, so safe to call atoi
		if(this->port == 0){
			os_printf("request::set_url(): failed parsing port");
			return;
		}
	}else{
		if(this->secure){
			this->port = 443;
		}else{
			this->port = 80;
		}
	}
}

void ICACHE_FLASH_ATTR request::on_done(){
	this->in_progress = false;

	if(this->callback){
		this->callback(this->user_data, *this);
	}
}
