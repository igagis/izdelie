/**
 * This HTTP client code is based on the original code by Martin d'Allens <martin.dallens@gmail.com>.
 * The original License notice from Martin d'Allens:
 * 
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * Martin d'Allens <martin.dallens@gmail.com> wrote this file. As long as you retain
 * this notice you can do whatever you want with this stuff. If we meet some day,
 * and you think this stuff is worth it, you can buy me a beer in return.
 * ----------------------------------------------------------------------------
 */

#pragma once

#include "../util.hpp"
#include "../string.hpp"

#include "../../Timer.hpp"

namespace esp{
namespace httpc{

enum class status{
	undefined,
	ok,
	network_error,
	timeout,
	authentication_failed
};

struct response{
	status status_code = status::undefined;
	std::array<uint8_t, 4> remote_ip;
	unsigned http_status_code = 0;
	esp::string headers;
	esp::string body;
};

class request{
	// TCP connection context
	espconn conn;
	esp_tcp tcp;

	// string buffer to accumulate http response
	esp::string resp_buf;

	response resp;

	bool in_progress = false;

	void* user_data;
	void(*callback)(void*, const request&);

	esp::string host;
	int port = 80;
	esp::string path;
	bool secure = false;

	esp::string request_header;
	esp::string body;
	esp::string headers;

	local_timer_t<request> timeout_timer;
public:
	request(void* user_data, decltype(callback) callback);

	const response& get_response()const{
		ASSERT(!this->in_progress)
		return this->resp;
	}

	void start();

	void set_url(esp::string_view url);

	void set_body(esp::string&& body){
		this->body = std::move(body);
	}

	const esp::string& get_host()const{
		return this->host;
	}

	const esp::string& get_path()const{
		return this->path;
	}
private:
	void on_done();

	static void dns_callback(const char* hostname, ip_addr_t* addr, void* arg);
	void on_dns(const char* hostname, ip_addr_t* addr);

	static void conn_error_callback(void *arg, sint8 errType);
	void on_conn_error(sint8 error);

	static void conn_disconnect_callback(void * arg);
	void on_conn_disconnect();

	static void conn_connect_callback(void * arg);
	void on_conn_connect();

	static void conn_receive_callback(void* arg, char* buf, unsigned short len);
	void on_conn_receive(const char* buf, unsigned short len);

	static void conn_sent_callback(void* arg);
	void on_conn_sent();

	void on_timeout();

	void disconnect();
};
}
}
