#include "string.hpp"

#include <algorithm>

using namespace esp;

ICACHE_FLASH_ATTR string_view::string_view() = default;

ICACHE_FLASH_ATTR string_view::string_view(const char* buf, size_type len) :
		buf(buf),
		len(len)
{}

ICACHE_FLASH_ATTR string_view::string_view(const esp::string& str) :
		string_view(str.data(), str.size())
{}

ICACHE_FLASH_ATTR string::string(const char* s, size_t len) :
		buf(new (std::nothrow) char[len + 1]),
		len(len)
{
	if(!this->buf){
		this->len = 0;
		return;
	}
	os_memcpy(this->buf.get(), s, this->len);
	this->buf[this->len] = '\0';
}

ICACHE_FLASH_ATTR string::string(const string_view& str) :
		string(str.data(), str.size())
{}

void ICACHE_FLASH_ATTR string::clear(){
	this->len = 0;
	this->buf.reset();
}

const char* ICACHE_FLASH_ATTR string::c_str()const{
	if(this->empty()){
		return "";
	}
	return this->data();
}

char* ICACHE_FLASH_ATTR string::data(){
	return this->buf.get();
}

const char* ICACHE_FLASH_ATTR string::data()const{
	return this->buf.get();
}

string& ICACHE_FLASH_ATTR string::operator=(const string& str){
	this->operator=(esp::string(str));
	return *this;
};

string& ICACHE_FLASH_ATTR string::operator=(const char* s){
	if(!s){
		this->clear();
		return *this;
	}
	this->len = os_strlen(s);
	this->buf = std::unique_ptr<char[]>(new (std::nothrow) char[this->len + 1]);
	if(!this->buf){
		this->len = 0;
		return *this;
	}

	os_memcpy(this->buf.get(), s, this->len);
	this->buf[this->len] = '\0';
	return *this;
}

string& ICACHE_FLASH_ATTR string::append(const char* s, size_t len){
	if(len == 0){
		return *this;
	}
	std::unique_ptr<char[]> new_buf(new (std::nothrow) char[this->len + len + 1]);
	if(!new_buf){
		this->buf.reset();
		this->len = 0;
		return *this;
	}
	os_memcpy(new_buf.get(), this->buf.get(), this->len);
	os_memcpy(new_buf.get() + this->len, s, len);
	this->len += len;
	this->buf = std::move(new_buf);
	this->buf[this->len] = '\0';
	return *this;
}

string ICACHE_FLASH_ATTR string::substr(size_type pos, size_type count)const{
	if(pos >= this->size()){
		return string();
	}
	using std::min;
	return string(this->data() + pos, min(this->size() - pos, count));
}

ICACHE_FLASH_ATTR string_view::string_view(const char* str) :
		buf(str),
		len(os_strlen(str))
{}

string_view::size_type ICACHE_FLASH_ATTR string_view::find(string_view str, size_type pos)const{
	if(pos >= this->size()){
		return npos;
	}

	auto ret = pos;

	auto str_i = str.begin();
	for(auto i = std::next(this->begin(), pos); i != this->end(); ++i){
		if(str_i == str.end()){
			return ret;
		}
		if(*i == *str_i){
			++str_i;
		}else{
			str_i = str.begin();
			ret = std::distance(this->begin(), i) + 1;
		}
	}

	return npos;
}

string_view::size_type ICACHE_FLASH_ATTR string_view::find(value_type ch, size_type pos)const{
	if(pos >= this->size()){
		return npos;
	}
	auto i = std::find(this->begin(), this->end(), ch);
	if(i == this->end()){
		return npos;
	}
	return std::distance(this->begin(), i);
}

string_view ICACHE_FLASH_ATTR string_view::substr(size_type pos, size_type count)const{
	if(pos >= this->size()){
		return string_view();
	}

	using std::min;

	return string_view(this->data() + pos, min(count, this->size() - pos));
}
