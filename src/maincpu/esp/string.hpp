#pragma once

#include <memory>

extern"C"{
#include "osapi.h"
}

namespace esp{

class string_view;

class string{
	std::unique_ptr<char[]> buf;
	size_t len = 0;
public:
	typedef size_t size_type;

	static constexpr size_type npos ICACHE_RODATA_ATTR = size_type(-1);

	string() = default;

	string(const string& str) :
			string(str.data(), str.size())
	{}

	string& operator=(const string& str);

	string(string&&) = default;
	string& operator=(string&&) = default;

	string(const char* s, size_t len);

	string(const string_view& str);

	void clear();

	const char* c_str()const;

	char* data();

	const char* data()const;

	bool empty()const{
		return this->size() == 0;
	}

	size_t size()const{
		return this->len;
	}

	string& operator=(const char* s);

	string& append(const char* s, size_t len);
	
	string& append(const esp::string& str){
		return this->append(str.data(), str.size());
	}

	string substr(size_type pos = 0, size_type count = npos)const;
};

class string_view{
public:
	typedef char value_type;
	typedef value_type* pointer;
	typedef const value_type* const_pointer;
	typedef size_t size_type;
	typedef const char* const_iterator;
	typedef const_iterator iterator;
private:
	const char* buf = nullptr;
	size_type len = 0;
public:

	static constexpr size_type npos ICACHE_RODATA_ATTR = size_type(-1);

	ICACHE_FLASH_ATTR string_view();
	ICACHE_FLASH_ATTR string_view(const char* buf, size_type len);
	ICACHE_FLASH_ATTR string_view(const esp::string& str);
	ICACHE_FLASH_ATTR string_view(const char* str);
	ICACHE_FLASH_ATTR string_view(std::nullptr_t) = delete;

	size_type ICACHE_FLASH_ATTR size()const{
		return this->len;
	}

	bool ICACHE_FLASH_ATTR empty()const{
		return this->size() == 0;
	}

	const char* ICACHE_FLASH_ATTR data()const{
		return this->buf;
	}

	iterator ICACHE_FLASH_ATTR begin()const{
		return this->buf;
	}

	iterator ICACHE_FLASH_ATTR end()const{
		return this->buf + this->len;
	}

	size_type ICACHE_FLASH_ATTR find(string_view str, size_type pos = 0)const;
	size_type ICACHE_FLASH_ATTR find(value_type str, size_type pos = 0)const;

	string_view ICACHE_FLASH_ATTR substr(size_type pos = 0, size_type count = npos)const;
};

}
