#include "util.hpp"

#include <new>

extern"C"{
#include <user_interface.h>
#include <mem.h>
}

using namespace esp;

void ICACHE_FLASH_ATTR esp::abort(){
	while(true){} // infinite loop should trigger the watchdog
}

uint32_t ICACHE_FLASH_ATTR esp::get_cpu_ticks_per_second(){
	auto freq = system_get_cpu_freq();
	switch(freq){
		default:
			os_printf("WARNING: unknown CPU frequency: %d. Will be using 80 MHz for calculations.\n", freq);
			// fall-through
		case 80:
			return 80 * 1000000;
		case 160:
			return 160 * 1000000;
	}
}

void ICACHE_FLASH_ATTR __cxa_pure_virtual() {
	os_printf("FATAL: pure virtual function is called");
	while(1){}
}

void* ICACHE_FLASH_ATTR operator new(size_t count, const std::nothrow_t&){
	void* ret = os_malloc(count);
	ASSERT(ret)
	return ret;
}

void ICACHE_FLASH_ATTR operator delete(void* p)noexcept{
	os_free(p);
}

void* ICACHE_FLASH_ATTR operator new[](size_t count, const std::nothrow_t&){
	void* ret = os_malloc(count);
	ASSERT(ret)
	return ret;
}

void ICACHE_FLASH_ATTR operator delete[](void* p)noexcept{
	os_free(p);
}

namespace std{
const std::nothrow_t nothrow ICACHE_RODATA_ATTR;
}

ICACHE_FLASH_ATTR interrupts_guard::interrupts_guard(){
	this->interrupt_level = disable_interrupts();
}

ICACHE_FLASH_ATTR interrupts_guard::~interrupts_guard(){
	enable_interrupts(this->interrupt_level);
}

namespace{
char* initial_stack_pointer = nullptr;
}

void ICACHE_FLASH_ATTR esp::init_stack_usage_monitor(){
	// char var = 0;
	// initial_stack_pointer = &var;
	// TODO: no idea what is the correct stack base address
	initial_stack_pointer = reinterpret_cast<char*>(0x3fffffff);
	// os_printf("stack pointer initialized to 0x%x\n", unsigned(&var));
}

void ICACHE_FLASH_ATTR esp::print_current_stack_usage(){
	char var;
	ptrdiff_t diff = initial_stack_pointer - &var; // stack grows downwards
	uint32_t free_heap = system_get_free_heap_size();
	os_printf("stack usage = %d (0x%x) bytes, free heap = %d (0x%x) bytes\n", unsigned(diff), unsigned(diff), free_heap, free_heap);
}
