#include "Container.hpp"

#include "../esp/util.hpp"

extern "C"{
#include <c_types.h>
}

Vec2u ICACHE_FLASH_ATTR Container::calcChildrenRect(decltype(Container::begin) begin, decltype(Container::end) end){
//	os_printf("Container::calcChildrenRect(): enter\n");
	Vec2i ret(0, 0);

	for(auto i = begin; i != end; ++i){
		Vec2i br = (*i)->bottomRight();
		ret.x = std::max(ret.x, br.x);
		ret.y = std::max(ret.y, br.y);
	}

//	os_printf("Container::calcChildrenRect(): exit\n");
	return Vec2u(
			std::max(ret.x, 0),
			std::max(ret.y, 0)
		);
}

ICACHE_FLASH_ATTR Container::Container(Container& parent, Vec2i pos) :
		Widget(parent, pos, Vec2i(0, 0))
{
}

ICACHE_FLASH_ATTR Container::Container(Container& parent) :
		Widget(parent, Vec2i(0, 0), Vec2i(0, 0))
{
}

ICACHE_FLASH_ATTR Container::Container() :
		Widget()
{
}

void ICACHE_FLASH_ATTR Container::setChildren(decltype(begin) begin, decltype(end) end){
	this->begin = begin;
	this->end = end;
	this->dim = Container::calcChildrenRect(begin, end);
}

void ICACHE_FLASH_ATTR Container::render(ssd1306::FrameBufferBase& fb, Vec2i basePos)const{
//	os_printf("Container::render(): invoked\n");
	for(auto i = this->begin; i != this->end; ++i){
		ASSERT(*i)
		if((*i)->isVisible){
			(*i)->render(fb, basePos + (*i)->pos);
		}
	}
}
