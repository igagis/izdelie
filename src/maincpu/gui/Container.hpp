#pragma once

#include "Widget.hpp"

#include <array>

class Container : public Widget{
	friend class Screen;

	std::array<Widget*, 1>::const_iterator begin;
	decltype(begin) end;

	Container();
public:

	Container(Container& parent, Vec2i pos);
	Container(Container& parent);

	void setChildren(decltype(begin) begin, decltype(end) end);

	void render(ssd1306::FrameBufferBase& fb, Vec2i basePos)const override;
private:
	static Vec2u calcChildrenRect(decltype(begin) begin, decltype(end) end);
};
