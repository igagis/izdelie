#include "InfoScreen.hpp"

extern"C"{
#include <c_types.h>
#include <osapi.h>
#include <user_interface.h>
}

#include "../Stats.hpp"
#include "../WiFi.hpp"
#include "../App.hpp"

#include "MainMenuScreen.hpp"

namespace{
char* ICACHE_FLASH_ATTR fill_firmware_version_text(std::array<char,1>::iterator begin, std::array<char,1>::iterator end){
	os_snprintf(begin, end - begin, "Firmware version: %d", Stats::firmware_version);
	return begin;
}

const char* luminance_format ICACHE_RODATA_ATTR = "Luminance: %d lux";
const char* brightness_format ICACHE_RODATA_ATTR = "Brightness: %d / 15";
const char* mac_address_title_text ICACHE_RODATA_ATTR = "MAC address:";
const char* uptime_sec_format ICACHE_RODATA_ATTR = "%ds";
const char* uptime_min_sec_format ICACHE_RODATA_ATTR = "%dm %ds";
const char* uptime_hour_min_format ICACHE_RODATA_ATTR = "%dh %dm";
const char* uptime_day_hour_format ICACHE_RODATA_ATTR = "%dd %dh";
const char* uptime_year_day_format ICACHE_RODATA_ATTR = "%dy %dd";

const uint32_t timerPeriod_c = 1000;
}

ICACHE_FLASH_ATTR InfoScreen::InfoScreen(Vec2u dim) :
		title(*this, Vec2i(0, 0), "Info"),
		firmware_version(*this, this->title.bottom_left() + Vec2i(0, 3), fill_firmware_version_text(this->firmware_version_text.begin(), this->firmware_version_text.end())),
		mac_address_title(*this, Vec2i(0, this->firmware_version.bottomRight().y + 1), mac_address_title_text),
		mac_address(*this, Vec2i(10, this->mac_address_title.bottomRight().y + 1), this->mac_address_text.begin()),
		uptime_title(*this, Vec2i(0, this->mac_address.bottom_left().y + 1), "Uptime: "),
		uptime_value(*this, this->uptime_title.top_right(), this->uptime_text.begin()),
		luminance(*this, this->uptime_title.bottom_left() + Vec2i(0, 1), this->luminance_text.begin()),
		brightness(*this, Vec2i(0, this->luminance.bottomRight().y + 1), this->brightness_text.begin()),
		back(*this, Vec2i(0, this->brightness.bottomRight().y + 1), MainMenuScreen::back_text),
		timer(*this, &InfoScreen::timer_callback)
{
	this->title.pos.x = (dim.x - this->title.dim.x ) / 2;

	this->mac_address_text = WiFi::get_mac_address();

	this->setChildren(this->widgets.begin(), this->widgets.end());
	this->setFocusList(this->selectable_widgets.begin(), this->selectable_widgets.end());
}


void ICACHE_FLASH_ATTR InfoScreen::update_values(){
	os_snprintf(this->luminance_text.begin(), this->luminance_text.size(), luminance_format, App::inst().luminanceManager.get_luminance());
	*this->luminance_text.rbegin() = '\0';

	os_snprintf(this->brightness_text.begin(), this->brightness_text.size(), brightness_format, App::inst().luminanceManager.get_brightness());
	*this->brightness_text.rbegin() = '\0';

	uint32_t uptime = App::inst().stats.get_uptime();

	const uint32_t sec_per_minute = 60;
	const uint32_t sec_per_hour = 60 * sec_per_minute;
	const uint32_t sec_per_day = 24 * sec_per_hour;
	const uint32_t sec_per_year = sec_per_day * 36525 / 100;

	if(uptime < sec_per_minute){
		os_snprintf(this->uptime_text.begin(), this->uptime_text.size(), uptime_sec_format, uptime);
	}else if(uptime < sec_per_hour){
		os_snprintf(this->uptime_text.begin(), this->uptime_text.size(), uptime_min_sec_format, uptime / sec_per_minute, uptime % sec_per_minute);
	}else if(uptime < sec_per_day){
		os_snprintf(this->uptime_text.begin(), this->uptime_text.size(), uptime_hour_min_format, uptime / sec_per_hour, uptime % sec_per_hour / sec_per_minute);
	}else if(uptime < sec_per_year){
		os_snprintf(this->uptime_text.begin(), this->uptime_text.size(), uptime_day_hour_format, uptime / sec_per_day, uptime % sec_per_day / sec_per_hour);
	}else{
		os_snprintf(this->uptime_text.begin(), this->uptime_text.size(), uptime_year_day_format, uptime / sec_per_year, uptime % sec_per_year / sec_per_day);
	}

	// Null-terminate the string just in case.
	*this->uptime_text.rbegin() = '\0';
}

void ICACHE_FLASH_ATTR InfoScreen::timer_callback(){
	this->update_values();
	App::inst().gui.render_gui();
	this->timer.arm(timerPeriod_c);
}

void ICACHE_FLASH_ATTR InfoScreen::onOpen(){
	this->timer_callback();
}

void ICACHE_FLASH_ATTR InfoScreen::onClose(){
	this->timer.disarm();
}

bool ICACHE_FLASH_ATTR InfoScreen::onEvent(InputEvent e){
	switch (e){
		case InputEvent::ACTION:
			{
				auto cf = this->currentFocus();
				if(cf == &this->back){
					this->close();
				}
			}
			return true;
		default:
			break;
	}
	return this->Screen::onEvent(e);
}