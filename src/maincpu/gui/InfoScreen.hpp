#pragma once

extern"C"{
#include <osapi.h>
}

#include "Screen.hpp"
#include "Text.hpp"
#include "../Timer.hpp"

#include <array>

class InfoScreen : public Screen{
	Text title;

	std::array<char, 18 + 11> firmware_version_text;
	Text firmware_version;

	Text mac_address_title;
	std::array<char, 3 * 6> mac_address_text = {{'\0'}};
	Text mac_address;

	Text uptime_title;
	std::array<char, 3 + 2 + 2 + 3 + 2 + 1> uptime_text = {{'\0'}};
	Text uptime_value;

	std::array<char, 11 + 4 + 4 + 1> luminance_text = {{'\0'}};
	Text luminance;

	std::array<char, 12 + 2 + 5 + 1> brightness_text = {{'\0'}};
	Text brightness;

	Text back;

	std::array<Widget*, 9> widgets = {{
		&this->title,
		&this->firmware_version,
		&this->mac_address_title,
		&this->mac_address,
		&this->uptime_title,
		&this->uptime_value,
		&this->luminance,
		&this->brightness,
		&this->back
	}};

	std::array<Widget*, 1> selectable_widgets = {{
		&this->back
	}};

	void update_values();

	local_timer_t<InfoScreen> timer;

	void timer_callback();
public:
	InfoScreen(Vec2u dim);

	void onOpen()override;

	void onClose()override;

	bool onEvent(InputEvent e)override;
};
