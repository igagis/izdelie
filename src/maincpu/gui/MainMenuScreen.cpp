#include "MainMenuScreen.hpp"

extern"C"{
#include <c_types.h>
}

namespace{
const char* title_text ICACHE_RODATA_ATTR = "Main menu";
const char* setup_wifi_text ICACHE_RODATA_ATTR = "Setup WiFi";
const char* info_text ICACHE_RODATA_ATTR = "Info";
const char* settings_text ICACHE_RODATA_ATTR = "Settings";
}

ICACHE_FLASH_ATTR MainMenuScreen::MainMenuScreen(Vec2u dim) :
		wifiSetupScreen(dim),
		info_screen(dim),
		settings_screen(dim),
		title(*this, Vec2i(0, 0), title_text),
		setup_wifi(*this, this->title.bottom_left() + Vec2i(0, 3), setup_wifi_text),
		info(*this, Vec2i(0, this->setup_wifi.bottomRight().y + 1), info_text),
		settings(*this, this->info.bottom_left() + Vec2i(0, 1), settings_text),
		back(*this, this->settings.bottom_left() + Vec2i(0, 1), back_text)
{
	this->title.pos.x = (dim.x - this->title.dim.x ) / 2;

	this->setChildren(this->widgets.begin(), this->widgets.end());
	this->setFocusList(this->selectable_widgets.begin(), this->selectable_widgets.end());
}


bool ICACHE_FLASH_ATTR MainMenuScreen::onEvent(InputEvent e){
	switch (e){
		case InputEvent::ACTION:
			{
				auto cf = this->currentFocus();
				if(cf == &this->setup_wifi){
					this->open(this->wifiSetupScreen);
				}else if(cf == &this->info){
					this->open(this->info_screen);
				}else if(cf == &this->settings){
					this->open(this->settings_screen);
				}else if(cf == &this->back){
					this->close();
				}
			}
			return true;
		default:
			break;
	}
	return this->Screen::onEvent(e);
}
