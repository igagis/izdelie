#pragma once

#include "Screen.hpp"
#include "Text.hpp"
#include "WifiSetupScreen.hpp"
#include "InfoScreen.hpp"
#include "SettingsScreen.hpp"

class MainMenuScreen : public Screen{
	WifiSetupScreen wifiSetupScreen;
	InfoScreen info_screen;
	SettingsScreen settings_screen;

	Text title;

	Text setup_wifi;
	Text info;
	Text settings;
	Text back;

	std::array<Widget*, 5> widgets = {{
		&this->title,
		&this->setup_wifi,
		&this->info,
		&this->settings,
		&this->back
	}};

	std::array<Widget*, 4> selectable_widgets = {{
		&this->setup_wifi,
		&this->info,
		&this->settings,
		&this->back
	}};
public:
	static constexpr const char* back_text ICACHE_RODATA_ATTR = "Back";

	MainMenuScreen(Vec2u dim);

	bool onEvent(InputEvent e)override;
};
