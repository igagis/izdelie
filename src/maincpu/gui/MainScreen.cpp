#include "MainScreen.hpp"

extern "C"{
#include <c_types.h>
}

#include "../App.hpp"


ICACHE_FLASH_ATTR MainScreen::MainScreen(Vec2u dim) :
		main_menu_screen(dim)
{}

bool ICACHE_FLASH_ATTR MainScreen::onEvent(InputEvent e){
	switch(e){
		case InputEvent::ACTION:
			this->open(this->main_menu_screen);
			return true;
		case InputEvent::UP:
			if(this->pos < this->lines.size() - num_lines_per_screen){
				++this->pos;
			}
			return true;
		case InputEvent::DOWN:
			if(pos > 0){
				--this->pos;
			}
			return true;
		default:
			break;
	}
	return this->Screen::onEvent(e);
}

bool ICACHE_FLASH_ATTR MainScreen::event(InputEvent e){
	// if screen was off, then ignore the input event
	if(App::inst().screen_saver.renew_timers()){
		return true;
	}

	bool ret = this->Screen::event(e);
	this->render_gui();
	return ret;
}

void ICACHE_FLASH_ATTR MainScreen::render(ssd1306::FrameBufferBase& fb)const{
//	os_printf("MainScreen::render(fb): invoked\n");
	int y = fb.size().y - fb.glyphSize().y - 1;

	unsigned start_pos = this->first_line + this->pos;
	if(start_pos < this->lines.size()){
		for(auto i = this->lines.begin() + start_pos; i != this->lines.end(); ++i, y -= fb.glyphSize().y){
//			os_printf("Render %s at %d\n", i->begin(), y);
			fb.text(i->begin(), Vec2i(0, y), ssd1306::Color::WHITE);
		}
		start_pos = 0;
	}else{
		start_pos -= this->lines.size();
	}
	for(auto i = this->lines.begin() + start_pos; i != this->lines.begin() + this->first_line; ++i, y -= fb.glyphSize().y){
//		os_printf("Render %s at %d\n", i->begin(), y);
		fb.text(i->begin(), Vec2i(0, y), ssd1306::Color::WHITE);
	}

	// Draw scroll bar.
	fb.fillRect(Vec2i(fb.size().x - 1, fb.size().y - fb.size().y * (this->pos + num_lines_per_screen) / this->lines.size()), Vec2u(1, fb.size().y * num_lines_per_screen / this->lines.size()));
}

MainScreen::LogLine_t& ICACHE_FLASH_ATTR MainScreen::get_log_line(){
	if(App::inst().settings.screen_on_when_sync){
		App::inst().screen_saver.renew_timers();
	}

	if(this->first_line == 0){
		this->first_line = this->lines.size() - 1;
	}else{
		--this->first_line;
	}

	return this->lines[this->first_line];
}
