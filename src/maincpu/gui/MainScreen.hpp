#pragma once

#include "Screen.hpp"
#include "Text.hpp"
#include "MainMenuScreen.hpp"
#include "WifiSetupScreen.hpp"

#include <array>

class MainScreen : public Screen{
	MainMenuScreen main_menu_screen;

public:
	typedef std::array<char, 128 / 4 + 1> LogLine_t;
private:
	static const unsigned num_lines_per_screen = 64 / 6 + 1;

	std::array<LogLine_t, num_lines_per_screen * 4> lines = {{
		{{'H', 'e', 'l', 'l', 'o', ' ', 'i', 'z', 'd', 'e', 'l', 'i', 'e', '!', '\0'}},
		{{'\0'}}
	}};

	unsigned first_line = 0;

	unsigned pos = 0;

	LogLine_t& get_log_line();

public:
	MainScreen(Vec2u dim);

	void render(ssd1306::FrameBufferBase& fb)const override;

	bool onEvent(InputEvent e)override;

	bool event(InputEvent e);

	template <typename... Args> void log_no_redraw(const char* format, Args&&... args){
		auto& l = this->get_log_line();
		os_snprintf(l.begin(), l.size(), format, std::forward<Args>(args)...);
		*l.rbegin() = '\0';
	}

	template <typename... Args> void log(const char* format, Args&&... args){
		this->log_no_redraw(format, std::forward<Args>(args)...);
		this->render_gui();
	}
};
