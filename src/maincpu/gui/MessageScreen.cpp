#include "MessageScreen.hpp"

extern"C"{
#include <c_types.h>
}

ICACHE_FLASH_ATTR MessageScreen::MessageScreen(Vec2u dim) :
		message(*this, Vec2i(0, dim.y / 2), nullptr)
{
	this->setChildren(this->widgets.begin(), this->widgets.end());

	// set empty focus list
	this->setFocusList(this->widgets.end(), this->widgets.end());

	this->dim = dim;
}


void ICACHE_FLASH_ATTR MessageScreen::set_text(const char* str){
	this->message.setText(str);

	if(this->dim.x < this->message.dim.x){
		this->message.pos.x = 0;
	}else{
		this->message.pos.x = (this->dim.x - this->message.dim.x) / 2;
	}
}

bool ICACHE_FLASH_ATTR MessageScreen::onEvent(InputEvent e){
	switch (e){
		case InputEvent::ACTION:
			if(this->is_dismissable){
				this->close();
			}
			return true;
		case InputEvent::ESCAPE:
			if(!this->is_dismissable){
				return true;
			}
			break;
		default:
			break;
	}
	return this->Screen::onEvent(e);
}
