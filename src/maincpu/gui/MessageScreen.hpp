#pragma once

#include "Screen.hpp"
#include "Text.hpp"


class MessageScreen : public Screen{

	Text message;

	std::array<Widget*, 1> widgets = {{
		&this->message
	}};


public:

	bool is_dismissable = true;

	MessageScreen(Vec2u dim);

	void set_text(const char* str);

	bool onEvent(InputEvent e)override;
};
