#include "Screen.hpp"

extern "C"{
#include <c_types.h>
}

#include "../esp/util.hpp"
#include "../App.hpp"

ICACHE_FLASH_ATTR Screen::Screen() :
		Container()
{}


void ICACHE_FLASH_ATTR Screen::open(Screen& screen){
	ASSERT(!this->next)
	ASSERT(!screen.prev)
	ASSERT(!screen.next)

	this->next = &screen;
	screen.prev = this;

	this->onHide();
	screen.onOpen();
}

void ICACHE_FLASH_ATTR Screen::close(){
	ASSERT(!this->next)

	auto parent = this->prev;
	if(!parent){
		return; // Cannot close root screen.
	}

	parent->next = nullptr;

	this->prev = nullptr;

	this->onClose();
	parent->onShow();
}

void ICACHE_FLASH_ATTR Screen::closeChildren(){
	if(!this->next){
		return;
	}

	ASSERT(this->next->prev == this)

	Screen* top = this->next;

	while(top->next){
		ASSERT(top->next->prev == top)
		top = top->next;
	}

	ASSERT(top)
	ASSERT(top->prev)

	do{
		top = top->prev;
		top->next->close();
	}while(top != this);
}

void ICACHE_FLASH_ATTR Screen::render_gui()const{
//	os_printf("Screen::render(): invoked\n");
	App::inst().frame_buffer.clear();
	auto s = this;
	while(s->next){
		s = s->next;
	}
	s->render(App::inst().frame_buffer);
	App::inst().display.blit(App::inst().frame_buffer);
}

void ICACHE_FLASH_ATTR Screen::render(ssd1306::FrameBufferBase& fb)const{
//	os_printf("Screen::render(): invoked\n");
	this->Container::render(fb, Vec2i(0, 0));

	// Render selection.
	if(this->focusCurrent != this->focusEnd && (*this->focusCurrent)->isVisibleOnScreen()){
		fb.fillRect(
				(*this->focusCurrent)->posInAncestor(*this) - Vec2i(1, 1),
				(*this->focusCurrent)->dim + Vec2u(1, 1),
				ssd1306::Color::INVERSE
			);
	}
}

bool ICACHE_FLASH_ATTR Screen::event(InputEvent e){
	if(this->next){
		return this->next->event(e);
	}
	return this->onEvent(e);
}


bool ICACHE_FLASH_ATTR Screen::onEvent(InputEvent e){
	switch(e){
		case InputEvent::ESCAPE:
			this->close();
			return true;
		case InputEvent::DOWN:
			if(this->focusCurrent != this->focusEnd){
				++this->focusCurrent;
				if(this->focusCurrent == this->focusEnd){
					if(this->focusWrap){
						this->focusCurrent = this->focusBegin;
					}else{
						--this->focusCurrent;
					}
				}
				return true;
			}
			break;
		case InputEvent::UP:
			if(this->focusCurrent != this->focusEnd){
				if(this->focusCurrent == this->focusBegin){
					if(this->focusWrap){
						this->focusCurrent = this->focusEnd;
					}else{
						return true;
					}
				}
				--this->focusCurrent;
				return true;
			}
			break;
		default:
			break;
	}
	return false;
}

void ICACHE_FLASH_ATTR Screen::setFocusList(std::array<Widget*, 1>::const_iterator begin, std::array<Widget*, 1>::const_iterator end){
	this->focusBegin = begin;
	this->focusEnd = end;
	this->focusCurrent = this->focusBegin;
}

Widget* ICACHE_FLASH_ATTR Screen::currentFocus()const noexcept{
	if(this->focusCurrent == this->focusEnd){
		return nullptr;
	}
	return (*this->focusCurrent);
}

void ICACHE_FLASH_ATTR Screen::setFocus(unsigned index){
	ASSERT(this->focusEnd >= this->focusBegin)

	if(std::distance(this->focusBegin, this->focusEnd) == 0){
		return;
	}

	// os_printf("Screen::setFocus(): index = %d, distance = %d\n", index, std::distance(this->focusBegin, this->focusEnd));

	ASSERT(index < unsigned(std::distance(this->focusBegin, this->focusEnd)))

	this->focusCurrent = this->focusBegin + index;
}

void ICACHE_FLASH_ATTR Screen::onOpen(){
	this->setFocus(0);
}
