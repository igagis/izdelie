#pragma once

#include "Container.hpp"

enum class InputEvent{
	UP,
	DOWN,
	ACTION,
	ESCAPE
};

class Screen : protected Container{
	Screen* prev = nullptr;
	Screen* next = nullptr;

	std::array<Widget*, 1>::const_iterator focusBegin;
	decltype(begin) focusEnd;
	decltype(begin) focusCurrent = focusEnd;
	bool focusWrap = true;
protected:
	Widget* currentFocus()const noexcept;
	void setFocus(unsigned index);
	void setFocusWrap(bool wrap){
		this->focusWrap = wrap;
	}
public:
	Screen();

	void setFocusList(std::array<Widget*, 1>::const_iterator begin, std::array<Widget*, 1>::const_iterator end);

	void open(Screen& screen);

	// close this screen
	void close();

	void closeChildren();

	void render_gui()const;

	virtual void render(ssd1306::FrameBufferBase& fb)const;

	bool event(InputEvent e);

	virtual bool onEvent(InputEvent e);
	virtual void onOpen();
	virtual void onClose(){}
	virtual void onHide(){}
	virtual void onShow(){}
};
