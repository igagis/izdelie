#include "SelectorScreen.hpp"

extern "C"{
#include <c_types.h>
#include <osapi.h>
}

namespace{
const unsigned num_lines_on_screen = 9;
}

void ICACHE_FLASH_ATTR SelectorScreen::onOpen(){
	// os_printf("SelectorScreen::onOpen(): enter\n");
	if(!this->provider){
		return;
	}

	// Calculate list size.
	for(this->list_size = 0; this->provider->get_value(this->list_size);){
		++this->list_size;
	}

	// os_printf("this->list_size = %d\n", this->list_size);
}

bool ICACHE_FLASH_ATTR SelectorScreen::onEvent(InputEvent e){
	switch(e){
		case InputEvent::ACTION:
			this->close();
			return true;
		case InputEvent::DOWN:
			if(this->selected_index + 1 < this->list_size){
				++this->selected_index;
			}
			return true;
		case InputEvent::UP:
			if(this->selected_index != 0){
				--this->selected_index;
			}
			return true;
		default:
			break;
	}
	return this->Screen::onEvent(e);
}

void ICACHE_FLASH_ATTR SelectorScreen::render(ssd1306::FrameBufferBase& fb)const{
	if(this->provider){
		// os_printf("this->selected_index = %d, this->list_size = %d\n", this->selected_index, this->list_size);
		unsigned start_index;
		if(this->selected_index < num_lines_on_screen / 2 || this->list_size <= num_lines_on_screen){
			start_index = 0;
		}else if(this->list_size - this->selected_index <= num_lines_on_screen / 2){
			start_index = this->list_size - num_lines_on_screen;
		}else{
			start_index = this->selected_index - num_lines_on_screen / 2;
		}

		// os_printf("start_index = %d\n", start_index);

		unsigned i = 0;
		for(const char* s = this->provider->get_value(start_index); s && i != num_lines_on_screen; ++i, s = this->provider->get_value(start_index + i)){
			auto line_height = (fb.glyphSize().y + 1);
			Vec2i line_pos(0, i * line_height + 1);
			fb.text(s, line_pos);

			// os_printf("drawing line: %s\n", s);

			// Draw selection.
			if(start_index + i == this->selected_index){
				fb.fillRect(
						line_pos - Vec2i(0, 1),
						Vec2u(fb.size().x, line_height),
						ssd1306::Color::INVERSE
					);
			}
		}
	}

	this->Screen::render(fb);
}
