#pragma once

#include "Screen.hpp"

#include <array>

struct selector_provider_t{
	virtual const char* get_value(unsigned index)const = 0;
};


class SelectorScreen : public Screen{
	unsigned list_size = 0;
public:
	unsigned selected_index = 0;

	selector_provider_t* provider = nullptr;

	void onOpen()override;

	bool onEvent(InputEvent e)override;

	void render(ssd1306::FrameBufferBase& fb)const override;
};
