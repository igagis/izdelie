#include "SettingsScreen.hpp"

extern"C"{
#include <c_types.h>
}

#include "../App.hpp"

namespace{
const char* hour_format_24_text ICACHE_RODATA_ATTR = "24h";
const char* hour_format_12_text ICACHE_RODATA_ATTR = "12h";
const char* yes_text ICACHE_RODATA_ATTR = "Yes";
const char* no_text ICACHE_RODATA_ATTR = "No";

const char* blink_type_to_string(BlinkType blink_type){
	switch(blink_type){
		case BlinkType::dot:
			return "dot";
		case BlinkType::two_dots:
			return "two dots";
		case BlinkType::pendulum_1:
			return "pendulum 1";
		case BlinkType::pendulum_2:
			return "pendulum 2";
		case BlinkType::pendulum_3:
			return "pendulum 3";
		case BlinkType::pendulum_4:
			return "pendulum 4";
		case BlinkType::pendulum_5:
			return "pendulum 5";
		case BlinkType::chess_1:
			return "chess 1";
		case BlinkType::chess_2:
			return "chess 2";
		case BlinkType::sine_1_sec:
			return "sine 1 sec";
		case BlinkType::sine_2_sec:
			return "sine 2 sec";
		case BlinkType::eyes_1:
			return "eyes 1";
		case BlinkType::eyes_2:
			return "eyes 2";
		case BlinkType::eyes_3:
			return "eyes 3";
		case BlinkType::eyes_4:
			return "eyes 4";
		case BlinkType::eyes_5:
			return "eyes 5";
		case BlinkType::eyes_6:
			return "eyes 6";
		default:
			return nullptr;
	}
}
}

ICACHE_FLASH_ATTR SettingsScreen::SettingsScreen(Vec2u dim) :
		title(*this, Vec2i(0, 0), "Settings"),
		hour_format_title(*this, Vec2i(0, this->title.bottomRight().y) + Vec2i(0, 3), "Hour format: "),
		hour_format_value(*this, this->hour_format_title.top_right(), nullptr),
		leading_zero_title(*this, this->hour_format_title.bottom_left() + Vec2i(0, 1), "Show leading zero: "),
		leading_zero_value(*this, this->leading_zero_title.top_right(), nullptr),
		blink_type_title(*this, this->leading_zero_title.bottom_left() + Vec2i(0, 1), "Blink: "),
		blink_type_value(*this, this->blink_type_title.top_right(), nullptr),
		screen_on_when_sync_title(*this, this->blink_type_title.bottom_left() + Vec2i(0, 1), "Screen on when sync: "),
		screen_on_when_sync_value(*this, this->screen_on_when_sync_title.top_right(), nullptr),
		save(*this, this->screen_on_when_sync_title.bottom_left() + Vec2i(0, 1), "Save")
{
	this->title.pos.x = (dim.x - this->title.dim.x ) / 2;

	this->setChildren(this->widgets.begin(), this->widgets.end());
	this->setFocusList(this->selectable_widgets.begin(), this->selectable_widgets.end());
}

void ICACHE_FLASH_ATTR SettingsScreen::update_ui_from_settings(){
	this->hour_format_value.setText(App::inst().settings.is_24_hour_format ? hour_format_24_text : hour_format_12_text);
	this->leading_zero_value.setText(App::inst().settings.show_leading_zero ? yes_text : no_text);
	this->blink_type_value.setText(blink_type_to_string(App::inst().settings.blink_type));
	this->screen_on_when_sync_value.setText(App::inst().settings.screen_on_when_sync ? yes_text : no_text);
}

void ICACHE_FLASH_ATTR SettingsScreen::onOpen(){
	this->update_ui_from_settings();
	this->Screen::onOpen();
}

void ICACHE_FLASH_ATTR SettingsScreen::onClose(){
	App::inst().settings.save();
	this->Screen::onClose();
}

bool ICACHE_FLASH_ATTR SettingsScreen::onEvent(InputEvent e){
	switch (e){
		case InputEvent::ACTION:
			{
				auto cf = this->currentFocus();
				if(cf == &this->save){
					App::inst().settings.save();
					this->close();
				}else{
					if(cf == &this->hour_format_value){
						App::inst().settings.is_24_hour_format = !App::inst().settings.is_24_hour_format;
					}else if(cf == &this->leading_zero_value){
						App::inst().settings.show_leading_zero = !App::inst().settings.show_leading_zero;
					}else if(cf == &this->blink_type_value){
						uint8_t v = uint8_t(App::inst().settings.blink_type) + 1;
						if(v == uint8_t(BlinkType::ENUM_SIZE)){
							v = 0; // first element of the enum
						}
						App::inst().settings.blink_type = BlinkType(v);
						App::inst().clock.init_blinker();
					}else if(cf == &this->screen_on_when_sync_value){
						App::inst().settings.screen_on_when_sync = !App::inst().settings.screen_on_when_sync;
					}
					this->update_ui_from_settings();
				}
			}
			return true;
		default:
			break;
	}
	return this->Screen::onEvent(e);
}