#pragma once

#include "Screen.hpp"

#include "Text.hpp"

class SettingsScreen : public Screen{
	Text title;

	Text hour_format_title;
	Text hour_format_value;

	Text leading_zero_title;
	Text leading_zero_value;

	Text blink_type_title;
	Text blink_type_value;

	Text screen_on_when_sync_title;
	Text screen_on_when_sync_value;

	Text save;

	std::array<Widget*, 10> widgets = {{
		&this->title,
		&this->hour_format_title,
		&this->hour_format_value,
		&this->leading_zero_title,
		&this->leading_zero_value,
		&this->blink_type_title,
		&this->blink_type_value,
		&this->screen_on_when_sync_title,
		&this->screen_on_when_sync_value,
		&this->save
	}};

	std::array<Widget*, 5> selectable_widgets = {{
		&this->hour_format_value,
		&this->leading_zero_value,
		&this->blink_type_value,
		&this->screen_on_when_sync_value,
		&this->save
	}};

	void update_ui_from_settings();
public:
	SettingsScreen(Vec2u dim);

	void onOpen()override;

	void onClose()override;

	bool onEvent(InputEvent e)override;
};
