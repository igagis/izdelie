#include "Text.hpp"

extern "C"{
#include <c_types.h>
#include <osapi.h>
}

#include "../ssd1306/FrameBuffer.hpp"

namespace{
Vec2u ICACHE_FLASH_ATTR getStringDim(const char* str){
	Vec2u gd = ssd1306::FrameBufferBase::glyphSize();
	if(str){
		gd.x *= os_strlen(str);
	}else{
		gd.x = 2;
	}
	return gd;
}
}

ICACHE_FLASH_ATTR Text::Text(Container& parent, Vec2i pos, const char* str) :
		Widget(parent, pos, getStringDim(str)),
		str(str)
{}

void ICACHE_FLASH_ATTR Text::render(ssd1306::FrameBufferBase& fb, Vec2i basePos)const{
	if(!this->str){
		return;
	}

//	os_printf("Text::render(): drawing text '%s' at (%d, %d)\n", this->str, basePos.x, basePos.y);
	fb.text(this->str, basePos);
}

void ICACHE_FLASH_ATTR Text::setText(const char* str){
	this->str = str;
	this->dim = getStringDim(str);
}
