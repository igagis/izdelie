#pragma once

#include "Widget.hpp"

class Text : public Widget{
	const char* str;
public:
	Text(Container& parent, Vec2i pos, const char* str);

	void render(ssd1306::FrameBufferBase& fb, Vec2i basePos)const override;

	void setText(const char* str);
};
