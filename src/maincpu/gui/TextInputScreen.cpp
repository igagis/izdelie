#include "TextInputScreen.hpp"

extern "C"{
#include <c_types.h>
}

#include "../esp/util.hpp"
#include "../App.hpp"

namespace{
const char startChar = 0x20;
const char endChar = 0x7f;

const char* title ICACHE_RODATA_ATTR = "Enter text";
const char* hint ICACHE_RODATA_ATTR = "Long press for menu";

const char* inputBtn ICACHE_RODATA_ATTR = "ABC...";
const char* leftBtn ICACHE_RODATA_ATTR = "<-";
const char* rightBtn ICACHE_RODATA_ATTR = "->";
const char* backspaceBtn ICACHE_RODATA_ATTR = "<X";
const char* okBtn ICACHE_RODATA_ATTR = "Ok";

char ICACHE_FLASH_ATTR getNextChar(char c){
	++c;
	if(c == endChar){
		return startChar;
	}
	return c;
}

char ICACHE_FLASH_ATTR getPrevChar(char c){
	if(c == startChar){
		return endChar - 1;
	}
	--c;
	return c;
}

const unsigned menuSpacer = 4;

const uint32_t timerPeriod_c = 300;
}

ICACHE_FLASH_ATTR TextInputScreen::Menu::Menu(Container& parent) :
		Container(parent),
		input(*this, Vec2i(0, menuSpacer), inputBtn),
		leftArrow(*this, Vec2i(this->input.bottomRight().x + 2 * menuSpacer, menuSpacer), leftBtn),
		rightArrow(*this, Vec2i(this->leftArrow.bottomRight().x + 2 * menuSpacer, menuSpacer), rightBtn),
		backspace(*this, Vec2i(this->rightArrow.bottomRight().x + 2 * menuSpacer, menuSpacer), backspaceBtn),
		ok(*this, Vec2i(this->backspace.bottomRight().x + 2 * menuSpacer, menuSpacer), okBtn)
{
	this->setChildren(this->widgets.begin(), this->widgets.end());
}

ICACHE_FLASH_ATTR TextInputScreen::TextInputScreen(Vec2u dim):
		header(*this, Vec2i(0, 0), title),
		menuHint(*this, Vec2i(0, 0), hint),
		menu(*this),
		timer(*this, &TextInputScreen::onTick)
{
	this->header.pos.x = (dim.x - this->header.dim.x ) / 2;

	this->menuHint.pos = Vec2i(
			(dim.x - this->menuHint.dim.x) / 2,
			dim.y - this->menuHint.dim.y
		);

	this->menu.pos = Vec2i(
			(dim.x - this->menu.dim.x) / 2,
			dim.y - this->menu.dim.y
		);

	this->setChildren(this->widgets.begin(), this->widgets.end());
	this->setFocusList(this->selectableWidgets.begin(), this->selectableWidgets.end());
	this->setFocusWrap(false);
}

void ICACHE_FLASH_ATTR TextInputScreen::render(ssd1306::FrameBufferBase& fb)const{
	unsigned beforeCursorLength = fb.glyphSize().x * this->cursorPos;

	unsigned cursorLength = beforeCursorLength;
	if(this->isInputMode){
		cursorLength += fb.glyphSize().x;
	}

	int strY = (fb.size().y - fb.glyphSize().y) / 2;
	int strBottomY = strY + fb.glyphSize().y;

	// Draw text before cursor.
	int strX = int(fb.size().x) - int(cursorLength);
	if(strX >= 0){
		strX = 0;
	}
	fb.text(this->begin, this->cursorPos, Vec2i(strX, strY), ssd1306::Color::WHITE);

	int cursorX = strX + beforeCursorLength;

	// Draw current char.
	if(this->isInputMode){
		char prevChar = getPrevChar(this->curChar);
		char prevPrevChar = getPrevChar(prevChar);
		char prevPrevPrevChar = getPrevChar(prevPrevChar);
		char nextChar = getNextChar(this->curChar);
		char nextNextChar = getNextChar(nextChar);
		char nextNextNextChar = getNextChar(nextNextChar);
		fb.text(&prevPrevPrevChar, 1, Vec2i(cursorX, strY - 3 * fb.glyphSize().y), ssd1306::Color::WHITE);
		fb.text(&prevPrevChar, 1, Vec2i(cursorX, strY - 2 * fb.glyphSize().y), ssd1306::Color::WHITE);
		fb.text(&prevChar, 1, Vec2i(cursorX, strY - fb.glyphSize().y), ssd1306::Color::WHITE);
		if(this->cursorBlinkOn){
			fb.text(&this->curChar, 1, Vec2i(cursorX, strY), ssd1306::Color::WHITE);
		}
		fb.text(&nextChar, 1, Vec2i(cursorX, strY + fb.glyphSize().y), ssd1306::Color::WHITE);
		fb.text(&nextNextChar, 1, Vec2i(cursorX, strY + 2 * fb.glyphSize().y), ssd1306::Color::WHITE);
		fb.text(&nextNextNextChar, 1, Vec2i(cursorX, strY + 3 * fb.glyphSize().y), ssd1306::Color::WHITE);
	}

	int tailX = cursorX;
	if(this->isInputMode){
		tailX += fb.glyphSize().x;
	}

	// Draw text after cursor.
	fb.text(this->begin + this->cursorPos, this->end - this->begin - this->cursorPos, Vec2i(tailX, strY), ssd1306::Color::WHITE);

	// Draw cursor.
	if(!this->isInputMode && this->cursorBlinkOn){
		fb.fillRect(Vec2i(cursorX - 1, strY - 3), Vec2u(2, 3), ssd1306::Color::WHITE);
		fb.fillRect(Vec2i(cursorX - 1, strBottomY), Vec2u(2, 3), ssd1306::Color::WHITE);
	}

	this->Screen::render(fb);
}

void ICACHE_FLASH_ATTR TextInputScreen::setTextBuffer(std::array<char, 1>::iterator begin, std::array<char, 1>::iterator end){
	this->begin = begin;
	this->end = end;
	this->curChar = 'a';
	this->cursorPos = 0;

	this->setInputMode(false);

	unsigned length = this->end - this->begin;

	// Set curosr position to the end of the string.
	while(*(this->begin + this->cursorPos) != '\0' && this->cursorPos != length){
		++this->cursorPos;
	}
	// If there is no terminating 0 in the buffer, then set cursor to the beginning and add terminating zero.
	if(this->cursorPos == length){
		this->cursorPos = 0;
		*this->begin = '\0';
	}else{
		this->numChars = this->cursorPos;
	}

	this->setFocus(0);
}

bool ICACHE_FLASH_ATTR TextInputScreen::onEvent(InputEvent e){
	switch (e){
		case InputEvent::DOWN:
			if(this->isInputMode){
				this->curChar = getNextChar(this->curChar);
				return true;
			}
			break;
		case InputEvent::UP:
			if(this->isInputMode){
				this->curChar = getPrevChar(this->curChar);
				return true;
			}
			break;
		case InputEvent::ESCAPE:
			if(this->isInputMode){
				this->setInputMode(false);
			}
			// No return by long press from text input screen.
			// There is an "OK" button in menu for that.
			return true;
		case InputEvent::ACTION:
			if(this->isInputMode){
				this->putChar();
				return true;
			}else{
				auto cf = this->currentFocus();
				if(cf == &this->menu.input){
					if(!this->isFull()){
						this->setInputMode(true);
					}
				}else if(cf == &this->menu.leftArrow){
					this->cursorLeft();
				}else if(cf == &this->menu.rightArrow){
					this->cursorRight();
				}else if(cf == &this->menu.backspace){
					this->deleteChar();
				}else if(cf == &this->menu.ok){
					this->close();
				}
			}
			break;
		default:
			break;
	}

	return this->Screen::onEvent(e);
}

void ICACHE_FLASH_ATTR TextInputScreen::putChar(){
	if(this->isFull()){
		return;
	}

	auto dst = this->begin + this->cursorPos;
	ASSERT(dst + 1 < this->end)

	// Move string tail one char right.
	for(auto p = this->end - 1; p != dst; --p){
		*p = *(p - 1);
	}

	// Put current char to cursor position and update the cursor position and numebr of chars in the string.
	*dst = this->curChar;
	++this->cursorPos;
	++this->numChars;

	if(this->isFull()){
		this->setInputMode(false);
	}
}

void ICACHE_FLASH_ATTR TextInputScreen::deleteChar(){
	if(this->cursorPos == 0){
		return;
	}

	// Move string tail one char left.
	for(auto p = this->begin + this->cursorPos - 1; p != this->end - 1; ++p){
		*p = *(p + 1);
	}

	--this->cursorPos;
	--this->numChars;
}

void ICACHE_FLASH_ATTR TextInputScreen::cursorLeft(){
	if(this->cursorPos == 0){
		return;
	}
	--this->cursorPos;
}

void ICACHE_FLASH_ATTR TextInputScreen::cursorRight(){
	if(this->cursorPos == this->numChars){
		return;
	}
	++this->cursorPos;
}

void ICACHE_FLASH_ATTR TextInputScreen::setInputMode(bool inputMode){
	this->isInputMode = inputMode;
	this->menuHint.isVisible = this->isInputMode;
	this->menu.isVisible = !this->isInputMode;
}


void ICACHE_FLASH_ATTR TextInputScreen::onOpen(){
	this->timer.arm(timerPeriod_c);
	this->Screen::onOpen();
}

void ICACHE_FLASH_ATTR TextInputScreen::onClose(){
	this->timer.disarm();
}

void ICACHE_FLASH_ATTR TextInputScreen::onTick(){
	this->cursorBlinkOn = !this->cursorBlinkOn;
	App::inst().gui.render_gui();
	this->timer.arm(timerPeriod_c);
}
