#pragma once

extern"C"{
#include <os_type.h>
}

#include "Screen.hpp"
#include "Text.hpp"
#include "../Timer.hpp"

class TextInputScreen : public Screen{
	std::array<char, 1>::iterator begin;
	std::array<char, 1>::iterator end;

	char curChar = 'a';

	unsigned cursorPos = 0;

	unsigned numChars = 0;

	bool isInputMode = true;

	void setInputMode(bool inputMode);

	void putChar();
	void deleteChar();
	void cursorLeft();
	void cursorRight();

	bool isFull()const noexcept{
		return this->numChars + 1 == this->bufferSize();
	}

	unsigned bufferSize()const{
		return this->end - this->begin;
	}


	Text header;
	Text menuHint;

	class Menu : public Container{
	public:
		Menu(Container& parent);

		Text input;
		Text leftArrow;
		Text rightArrow;
		Text backspace;
		Text ok;

		std::array<Widget*, 5> widgets = {{
			&this->input,
			&this->leftArrow,
			&this->rightArrow,
			&this->backspace,
			&this->ok
		}};
	};

	Menu menu;

	std::array<Widget*, 3> widgets = {{
		&this->header,
		&this->menuHint,
		&this->menu
	}};

	std::array<Widget*, 5> selectableWidgets = {{
		&this->menu.input,
		&this->menu.leftArrow,
		&this->menu.rightArrow,
		&this->menu.backspace,
		&this->menu.ok
	}};

	local_timer_t<TextInputScreen> timer;

	void onTick();

	bool cursorBlinkOn = true;

public:
	TextInputScreen(Vec2u dim);

	void setTextBuffer(std::array<char, 1>::iterator begin, std::array<char, 1>::iterator end);

	void render(ssd1306::FrameBufferBase& fb)const override;

	bool onEvent(InputEvent e)override;

	void onOpen()override;
	void onClose()override;
};
