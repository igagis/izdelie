#include "Widget.hpp"

extern "C"{
#include <c_types.h>
}

#include "Container.hpp"

ICACHE_FLASH_ATTR Widget::Widget(Container& parent, Vec2i pos, Vec2u dim) :
		parent(&parent),
		pos(pos),
		dim(dim)
{}

ICACHE_FLASH_ATTR Widget::Widget() :
		parent(nullptr),
		pos(Vec2i(0, 0)),
		dim(Vec2u(0, 0))
{}

Vec2i ICACHE_FLASH_ATTR Widget::bottomRight()const noexcept{
	return Vec2i(this->pos.x + this->dim.x, this->pos.y + this->dim.y);
}

Vec2i ICACHE_FLASH_ATTR Widget::top_right()const noexcept{
	return Vec2i(this->pos.x + this->dim.x, this->pos.y);
}

Vec2i ICACHE_FLASH_ATTR Widget::bottom_left()const noexcept{
	return Vec2i(this->pos.x, this->pos.y + this->dim.y);
}

Vec2i ICACHE_FLASH_ATTR Widget::posInAncestor(const Container& a)const{
	if(!this->parent || this->parent == &a){
		return this->pos;
	}
	return this->parent->posInAncestor(a) + this->pos;
}

bool ICACHE_FLASH_ATTR Widget::isVisibleOnScreen()const noexcept{
	if(!this->isVisible){
		return false;
	}
	if(!this->parent){
		return this->isVisible;
	}
	return this->parent->isVisibleOnScreen();
}
