#pragma once

#include "../Vec2.hpp"

#include "../ssd1306/FrameBuffer.hpp"

class Container;

class Widget{
	friend class Container;

	Widget();

public:
	const Container* parent;

	Vec2i pos;
	Vec2u dim;

	bool isVisible = true;

	Vec2i bottomRight()const noexcept;

	Vec2i top_right()const noexcept;

	Vec2i bottom_left()const noexcept;

	Vec2i posInAncestor(const Container& a)const;

	bool isVisibleOnScreen()const noexcept;

	Widget(Container& parent, Vec2i pos, Vec2u dim);

	virtual void render(ssd1306::FrameBufferBase& fb, Vec2i basePos)const{}
};
