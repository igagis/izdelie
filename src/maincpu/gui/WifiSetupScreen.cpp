#include "WifiSetupScreen.hpp"

extern "C"{
#include <c_types.h>
#include <mem.h>
}

#include <algorithm>

#include "../App.hpp"

namespace{
const char* title ICACHE_RODATA_ATTR = "WiFi setup";
const char* ssidField ICACHE_RODATA_ATTR = "SSID:";
const char* passwordField ICACHE_RODATA_ATTR = "Password:";
const char* connectBtn ICACHE_RODATA_ATTR = "Connect";

const unsigned ui_gap_px = 5;
}

ICACHE_FLASH_ATTR WifiSetupScreen::WifiSetupScreen(Vec2u dim) :
		header(*this, Vec2i(0, 0), title),
		scan(*this, Vec2i(10, 11), "Scan"),
		ssidTitle(*this, this->scan.bottom_left() + Vec2i(0, ui_gap_px), ssidField),
		ssid(*this, Vec2i(0, this->ssidTitle.bottom_left().y + 1), this->ssidText.begin()),
		passwordTitle(*this, this->ssid.bottom_left() + Vec2i(10, ui_gap_px), passwordField),
		password(*this, Vec2i(0, this->passwordTitle.bottom_left().y + 1), this->passwordText.begin()),
		connect(*this, this->password.bottom_left() + Vec2i(10, ui_gap_px), connectBtn),
		back(*this, this->connect.top_right() + Vec2i(63, 0), MainMenuScreen::back_text)
{
	this->header.pos.x = (dim.x - this->header.dim.x) / 2;
	this->setChildren(this->widgets.begin(), this->widgets.end());
	this->setFocusList(this->focusable.begin(), this->focusable.end());
}

void ICACHE_FLASH_ATTR WifiSetupScreen::onOpen(){
	station_config config;

	if(wifi_station_get_config_default(&config)){
		os_memcpy(this->ssidText.begin(), &config.ssid, 32);
		*this->ssidText.rbegin() = '\0';
		os_memcpy(this->passwordText.begin(), &config.password, 64);
		*this->passwordText.rbegin() = '\0';
	}

	this->Screen::onOpen();
}

bool ICACHE_FLASH_ATTR WifiSetupScreen::onEvent(InputEvent e){
	switch (e){
		case InputEvent::ACTION:
			{
				auto& ti = App::inst().textInput;
				auto cf = this->currentFocus();
//				os_printf("WifiSetupScreen::event(): ACTION, cs = 0x%x\n", reinterpret_cast<uint32_t>(cs));
				if(cf == &this->ssidTitle){
//					os_printf("WifiSetupScreen::event(): ssid\n");
					this->open(ti);
					ti.setTextBuffer(this->ssidText.begin(), this->ssidText.end());
				}else if(cf == &this->passwordTitle){
//					os_printf("WifiSetupScreen::event(): password\n");
					this->state = state_e::password_input_shown;
					this->open(ti);
					ti.setTextBuffer(this->passwordText.begin(), this->passwordText.end());
				}else if(cf == &this->connect){
					if(*this->ssidText.begin() == '\0' && *this->passwordText.begin() != '\0'){
						auto& ms = App::inst().message_screen;
						ms.set_text("Error: SSID field is empty");
						ms.is_dismissable = true;
						this->open(ms);
					}else{
						App::inst().wifi.connect(this->ssidText.begin(), this->passwordText.begin());
						App::inst().gui.closeChildren();
					}
				}else if(cf == &this->scan){
					auto& ms = App::inst().message_screen;
					ms.set_text("Scanning for networks...");
					ms.is_dismissable = false;
					this->open(ms);

					App::inst().wifi.scan(*this);
				}else if(cf == &this->back){
					this->close();
				}
//				os_printf("WifiSetupScreen::event(): exit\n");
			}
			return true;
		default:
			break;
	}
	return this->Screen::onEvent(e);
}

void ICACHE_FLASH_ATTR WifiSetupScreen::show_scan_failed_message(){
	auto& ms = App::inst().message_screen;

	App::inst().gui.log("WiFi scanning failed");

	ms.set_text("Scanning for networks failed");
	ms.is_dismissable = true;
	this->open(ms);
}

void ICACHE_FLASH_ATTR WifiSetupScreen::onShow(){
	// os_printf("WifiSetupScreen::onShow(): this->state = %d\n", this->state);
	switch(this->state){
		case state_e::SSID_SELECTOR_SHOWN:
			{
				auto& sc = App::inst().item_selector;
				ASSERT(sc.selected_index < this->num_ssids)
				this->ssidText = this->ssids[sc.selected_index];
				// os_printf("ssid = %s\n", this->ssids[sc.selected_index].begin());
				os_free(this->ssids);
				this->ssids = nullptr;
				// os_printf("SSID selected: sc.selected_index = %d, ssid = %s\n", sc.selected_index, this->ssidText.begin());
				this->setFocus(std::distance(
						this->focusable.begin(),
						std::find(this->focusable.begin(), this->focusable.end(), &this->passwordTitle)
					));
			}
			break;
		case state_e::password_input_shown:
			this->setFocus(std::distance(
					this->focusable.begin(),
					std::find(this->focusable.begin(), this->focusable.end(), &this->connect)
				));
			break;
		default:
			break;
	}
	this->state = state_e::IDLE;
	this->Screen::onShow();
}

void ICACHE_FLASH_ATTR WifiSetupScreen::on_scan_complete(bss_info* bss){
	auto& ms = App::inst().message_screen;

	ms.close();

	if(!bss){
		// os_printf("WiFi scanning failed.\n");
		this->show_scan_failed_message();
	}else{
		// os_printf("WiFi scanning completed\n");
		App::inst().gui.log_no_redraw("WiFi scanning finished");

		ASSERT(!this->ssids)

		// Count number of ssids found.

		this->num_ssids = 0;
		for(bss_info* b = bss; b; b = STAILQ_NEXT(b, next)){
			++this->num_ssids;
		}

		this->ssids = (ssid_t*)os_malloc(sizeof(ssid_t) * this->num_ssids);
		if(!this->ssids){
			this->show_scan_failed_message();
			App::inst().gui.render_gui();
			return;
		}

		// Copy ssids to allocated array.

		unsigned i = 0;
		for(bss_info* b = bss; b; b = STAILQ_NEXT(b, next), ++i){
			os_memcpy(this->ssids[i].begin(), b->ssid, 32);
			this->ssids[i][b->ssid_len] = '\0';
			// os_printf("SSID: %s\n", this->ssids[i].begin());
			App::inst().gui.log_no_redraw("  %s", this->ssids[i].begin());
		}

		// Show selector screen.
		auto& sc = App::inst().item_selector;
		sc.selected_index = 0;
		sc.provider = this;
		this->open(sc);

		this->state = state_e::SSID_SELECTOR_SHOWN;

		App::inst().gui.render_gui();
	}
}

const char* ICACHE_FLASH_ATTR WifiSetupScreen::get_value(unsigned index)const{
	if(index >= this->num_ssids){
		return nullptr;
	}

	return this->ssids[index].begin();
}
