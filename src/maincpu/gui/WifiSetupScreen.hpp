#pragma once

extern"C"{
#include <user_interface.h>
}

#include "Screen.hpp"
#include "SelectorScreen.hpp"
#include "Text.hpp"

#include "../WiFi.hpp"

#include <array>

class WifiSetupScreen : public Screen, public selector_provider_t, public WiFi::Listener{
	typedef std::array<char, 32 + 1> ssid_t;
	ssid_t ssidText = {{'\0'}};
	std::array<char, 64 + 1> passwordText = {{'\0'}};

	Text header;

	Text scan;

	Text ssidTitle;
	Text ssid;
	Text passwordTitle;
	Text password;

	Text connect;
	Text back;

	std::array<Widget*, 8> widgets = {{
		&this->header,
		&this->scan,
		&this->ssidTitle,
		&this->ssid,
		&this->passwordTitle,
		&this->password,
		&this->connect,
		&this->back
	}};

	std::array<Widget*, 5> focusable = {{
		&this->scan,
		&this->ssidTitle,
		&this->passwordTitle,
		&this->connect,
		&this->back
	}};

	ssid_t* ssids = nullptr;
	unsigned num_ssids = 0;

	void show_scan_failed_message();

	enum class state_e{
		IDLE,
		SSID_SELECTOR_SHOWN,
		password_input_shown
	};

	state_e state = state_e::IDLE;

public:
	WifiSetupScreen(Vec2u dim);

	bool onEvent(InputEvent e)override;

	void onOpen()override;

	void onShow()override;

	const char* get_value(unsigned index)const override;

	void on_scan_complete(bss_info* bss)override;
};
