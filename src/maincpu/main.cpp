extern "C"{
#include <ets_sys.h>
#include <osapi.h>
#include <gpio.h>
#include <os_type.h>
#include <user_interface.h>
#include <version.h>

#include "user_config.h"
}

#include <array>

#include "App.hpp"
#include "Partition.hpp"

#ifdef DEBUG
#	include "../gdbstub/gdbstub.h"
#endif

#if ESP_SDK_VERSION_NUMBER >= 030000

namespace{

const uint32_t ota1PartitionAddress = 0x1000;

#if M_FLASH_MEMORY_MAP == 2
#error "izdelie does not support memory map 2"
const uint32_t flashSize = 0x100000; // 1 Mb
const uint32_t ota2PartitionAddress = 0x81000;
const uint32_t otaPartitionSize = 1024 * 488; // 488 kb

#elif M_FLASH_MEMORY_MAP == 6
const uint32_t flashSize = 0x100000 * 4; // 4 Mb
const uint32_t ota2PartitionAddress = 0x101000;
const uint32_t otaPartitionSize = 1024 * 1000; // 1000 kb

#else
#	error "Unknown ESP8266 flash memory map"
#endif

const std::array<partition_item_t, 6> partitionTable ICACHE_RODATA_ATTR = {{
	{
		partition_type_t(Partition::Ota1),
		ota1PartitionAddress,
		otaPartitionSize
	},
	{
		partition_type_t(Partition::Ota2),
		ota2PartitionAddress,
		otaPartitionSize
	},
	{
		partition_type_t(Partition::Settings),
		ota2PartitionAddress + otaPartitionSize,
		0x1000 * 3 // 3 sectors, 4kb each
	},
	{
		partition_type_t(Partition::RfCal),
		flashSize - 0x5000,
		0x1000
	},
	{
		partition_type_t(Partition::PhyData),
		flashSize - 0x4000,
		0x1000
	},
	{
		partition_type_t(Partition::SystemParameter),
		flashSize - 0x3000,
		0x3000
	}
}};
}

extern "C" void ICACHE_FLASH_ATTR user_pre_init(){
	if(!system_partition_table_regist(
			partitionTable.begin(),
			partitionTable.size(),
			M_FLASH_MEMORY_MAP
		))
	{
		// NOTE: uart0 is not initialized yet, so os_printf() goes nowhere
		// os_printf("system_partition_table_regist() failed\r\n");
		// NOTE: watchdog is not set up yet, so it will infinitely hang
		ASSERT_ALWAYS(false)
	}
}

#endif

extern "C" void ICACHE_FLASH_ATTR user_init(){
	// set UART0 speed to 115200, os_printf() outputs to UART0
	uart_div_modify(0, UART_CLK_FREQ / 115200);

#ifdef DEBUG
	// gdbstub_init();
#endif

	esp::init_stack_usage_monitor();

	os_printf("Preved izdelie!\n");

	// init gpio subsytem
	gpio_init();

	os_printf("gpio initialized!\n");

	system_print_meminfo();

	esp::print_current_stack_usage();

	App::create();
}
