#include "Display.hpp"

extern "C"{
#include <osapi.h>
}

using namespace ssd1306;

namespace{
const unsigned width_c = 128;
}

namespace Command{
const uint8_t
	SetMemoryMode = 0x20,
	SetColumnAddress = 0x21,
	SetPageAddress = 0x22,
	SetContrast = 0x81,
	SetChargePump = 0x8D,
	SegRemap = 0xA0,
	DisplayAllOnResume = 0xA4,
	DisplayAllOn = 0xA5,
	NormalDisplay = 0xA6,
	InvertDisplay = 0xA7,
	SetMultiplex = 0xA8,
	DisplayOff = 0xAE,
	DisplayOn = 0xAF,
	ComScanInc = 0xC0,
	ComScanDec = 0xC8,
	SetDisplayOffset = 0xD3,
	SetDisplayClockDiv = 0xD5,
	SetPrecharge = 0xD9,
	SetComPins = 0xDA,
	SetVComDeselect = 0xDB,

	SetLowColumn = 0x00,
	SetHighColumn = 0x10,
	SetStartLine = 0x40,

	ExternalVcc = 0x01, /// External display voltage source
	SwitchCapVcc = 0x02, // Generate display voltage from 3.3v

	RightHorizontalScroll = 0x26,
	LeftHorizontalScroll = 0x27,
	VerticalAndRightHorizontalScroll = 0x29, // diagonal scroll
	VerticalAndLeftHorizontalScroll = 0x2A, // diagonal scroll
	DeactivateScroll = 0x2E,
	ActivateScroll = 0x2F,
	SetVerticalScrollarea = 0xA3;
};


ICACHE_FLASH_ATTR Display::Display(esp::i2c::master& i2cBus, uint8_t i2cAddress, DisplaySize size) :
	i2cBus(i2cBus),
	i2cAddress(i2cAddress),
	size(size)
{
//	os_printf("Display::Display(): i2cAddress = 0x%x\n", this->i2cAddress);
	this->init();
}

Vec2u ICACHE_FLASH_ATTR Display::dim()const noexcept{
	switch (this->size){
	case DisplaySize::SIZE_128x32:
		return Vec2u(width_c, 32);
	case DisplaySize::SIZE_128x64:
		return Vec2u(width_c, 64);
	}
	return Vec2u(50, 30);
}

void ICACHE_FLASH_ATTR Display::sendCommand(uint8_t cmd){
	std::array<uint8_t, 2> buf = {
		0x0, //command mode
		cmd
	};

	if(this->i2cBus.send(this->i2cAddress, buf.begin(), buf.end()) != esp::i2c::status::ok){
		os_printf("ERROR while sending command 0x%x to ssd1306 display\n", cmd);
	}
}

void ICACHE_FLASH_ATTR Display::sendCommand(uint8_t cmd, uint8_t param){
	std::array<uint8_t, 3> buf = {
		0x0, //command mode
		cmd,
		param
	};

	if(this->i2cBus.send(this->i2cAddress, buf.begin(), buf.end()) != esp::i2c::status::ok){
		os_printf("ERROR while sending command 0x%x with parameter 0x%x to ssd1306 display\n", cmd, param);
	}
}

void ICACHE_FLASH_ATTR Display::sendCommand(uint8_t cmd, uint8_t param1, uint8_t param2){
	std::array<uint8_t, 4> buf = {
		0x0, //command mode
		cmd,
		param1,
		param2
	};

	if(this->i2cBus.send(this->i2cAddress, buf.begin(), buf.end()) != esp::i2c::status::ok){
		os_printf("ERROR while sending command 0x%x with parameters 0x%x, 0x%x to ssd1306 display\n", cmd, param1, param2);
	}
}

void ICACHE_FLASH_ATTR Display::reset(){
	this->sendCommand(Command::SetColumnAddress, 0, 127);
	this->sendCommand(Command::SetPageAddress, 0, 63);
}

void ICACHE_FLASH_ATTR Display::setOn(bool on){
	if(this->is_on_v == on){
		return;
	}
	this->is_on_v = on;
	this->sendCommand(on ? Command::DisplayOn : Command::DisplayOff);
}


void ICACHE_FLASH_ATTR Display::init(){
	this->sendCommand(Command::SetMultiplex, this->dim().y - 1);
	this->sendCommand(Command::SetDisplayOffset, 0x0); //no offset
	this->sendCommand(Command::SetStartLine | 0x0); // line 0

	this->sendCommand(Command::SegRemap | 0x1); // 0x1 = flip horizontally, 0x0 = no flip

	this->sendCommand(Command::ComScanDec); // flip vertically
//    this->sendCommand(Command::ComScanInc); //no flip vertically

	this->sendCommand(Command::SetComPins, 0x12);
	this->set_brightness(0xf);
	this->sendCommand(Command::DisplayAllOnResume);
	this->sendCommand(Command::NormalDisplay);
	this->sendCommand(Command::SetDisplayClockDiv, 0x80); // the suggested ratio 0x80
	this->sendCommand(Command::SetChargePump, 0x14); // use internal VCC step up (0x10 to not use it)
	this->sendCommand(Command::SetMemoryMode, 0x0); // 0x0 = horizontal, 0x1 = vertical
	this->sendCommand(Command::SetPrecharge, 0xF1);
	this->sendCommand(Command::SetVComDeselect, 0x40); //
	this->sendCommand(Command::DeactivateScroll);

	this->reset();
}

void ICACHE_FLASH_ATTR Display::blit(const FrameBufferBase& buf){
	if(this->i2cBus.send(this->i2cAddress, buf.beginIter, buf.endIter) != esp::i2c::status::ok){
		os_printf("ERROR while sending data to ssd1306 display, resetting ssd1306\n");
		this->reset();
	}
}

void ICACHE_FLASH_ATTR Display::set_brightness(uint8_t brightness){
	const uint32_t max_contrast = 0xcf; // for internal VCC step up use 0xCF as maximum contrast
	uint8_t b = uint8_t((max_contrast - 1) * uint32_t(brightness & 0xf) / 0xf + 1);
	this->sendCommand(Command::SetContrast, b);
}
