#pragma once

extern "C"{
#include <osapi.h>
}

#include <array>

#include "FrameBuffer.hpp"

#include "../esp/i2c/master.hpp"

namespace ssd1306{

enum class DisplaySize{
	SIZE_128x64,
	SIZE_128x32
};

class Display{
	esp::i2c::master& i2cBus;

	uint8_t i2cAddress;

	bool is_on_v = false;

	void init();

	void sendCommand(uint8_t cmd);
	void sendCommand(uint8_t cmd, uint8_t param);
	void sendCommand(uint8_t cmd, uint8_t param1, uint8_t param2);

	DisplaySize size;
public:
	Display(esp::i2c::master& i2cBus, uint8_t i2cAddress, DisplaySize size);

	void reset();

	void setOn(bool on);

	bool is_on(){
		return this->is_on_v;
	}

	void blit(const FrameBufferBase& buf);

	Vec2u dim()const noexcept;

	/**
	 * @param brightness - brightness from 0 to 15.
	 */
	void set_brightness(uint8_t brightness);
};



}
