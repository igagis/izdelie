#include "FrameBuffer.hpp"

extern "C"{
#include <osapi.h>
}

#include <cstdlib>

#include "../esp/util.hpp"

#include "../build/font.hxx"

using namespace ssd1306;

Vec2u ICACHE_FLASH_ATTR FrameBufferBase::glyphSize(){
	return Vec2u(glyphWidth, glyphHeight);
}

void ICACHE_FLASH_ATTR FrameBufferBase::drawGlyph(char g, Vec2i pos, Color color){
	if(g < startAsciiCode || g >= startAsciiCode + font.size()){
		os_printf("WARNING: no glyph for character %d\n", g);
		return;
	}

	unsigned inRowOffset;
	if(pos.y >= 0){
		inRowOffset = pos.y % std::numeric_limits<uint8_t>::digits;
	}else{
		inRowOffset = std::numeric_limits<uint8_t>::digits - ((-pos.y) % std::numeric_limits<uint8_t>::digits);
	}

//	os_printf("glyphHeight = %d\n", glyphHeight);
//	os_printf("adder = %u\n", unsigned(-glyphHeight) % std::numeric_limits<uint8_t>::digits);
//	os_printf("endFontRow = %d\n", (glyphHeight + (-glyphHeight) % std::numeric_limits<uint8_t>::digits) / std::numeric_limits<uint8_t>::digits);

	for(unsigned fontRow = 0;
			fontRow != (glyphHeight + unsigned(-glyphHeight) % std::numeric_limits<uint8_t>::digits) / std::numeric_limits<uint8_t>::digits;
			++fontRow
		)
	{
		int row = pos.y / std::numeric_limits<uint8_t>::digits;
		if(pos.y < 0){
			row -= 1;
		}
//		os_printf("row = %d\n", row);
		if(row >= 0 && row < int(height_c / std::numeric_limits<uint8_t>::digits)){
			auto i = this->beginIter + 1 + width_c * row;
			if(pos.x < int(width_c))
			{
				for(int srcX = 0, dstX = pos.x; srcX != glyphWidth && dstX != width_c; ++srcX, ++dstX){
					if(dstX < 0){
						continue;
					}
					uint8_t b = (font[g - startAsciiCode][fontRow * glyphWidth + srcX]) << inRowOffset;
					switch (color){
						case Color::WHITE:
							*(i + dstX) |= b;
							break;
						case Color::BLACK:
							*(i + dstX) &= ~b;
							break;
						case Color::INVERSE:
							*(i + dstX) ^= b;
							break;
					}
				}
			}
		}
		if(inRowOffset != 0){
			++row;
			if(row >= 0 && row < int(height_c / std::numeric_limits<uint8_t>::digits)){
				auto i = this->beginIter + 1 + width_c * row;
				if(pos.x < int(width_c)){
					for(int srcX = 0, dstX = pos.x; srcX != glyphWidth && dstX != width_c; ++srcX, ++dstX){
						if(dstX < 0){
							continue;
						}
						uint8_t b = (font[g - startAsciiCode][fontRow * glyphWidth + srcX]) >> (std::numeric_limits<uint8_t>::digits - inRowOffset);
						switch (color){
							case Color::WHITE:
								*(i + dstX) |= b;
								break;
							case Color::BLACK:
								*(i + dstX) &= ~b;
								break;
							case Color::INVERSE:
								*(i + dstX) ^= b;
								break;
						}
					}
				}
			}
		}
	}
}

void ICACHE_FLASH_ATTR FrameBufferBase::text(const char* str, Vec2i pos, Color color){
//	os_printf("FrameBufferBase::text(): drawing text '%s' at %d, %d\n", str, pos.x, pos.y);
	for(auto p = str; *p != '\0'; ++p, pos.x += glyphWidth){
		this->drawGlyph(*p, pos, color);
	}
}

void ICACHE_FLASH_ATTR FrameBufferBase::text(const char* str, unsigned numChars, Vec2i pos, Color color){
	unsigned i = 0;
	for(auto p = str; *p != '\0' && i != numChars; ++p, pos.x += glyphWidth, ++i){
		this->drawGlyph(*p, pos, color);
	}
}

void ICACHE_FLASH_ATTR FrameBufferBase::fillRect(Vec2i pos, Vec2u dim, Color color){
	if(pos.y < 0){
		dim.y += pos.y;
		pos.y = 0;
	}
	if(pos.x < 0){
		dim.x += pos.x;
		pos.x = 0;
	}

	Vec2i rb = pos + dim;

	if(rb.x > int(width_c)){
		dim.x -= (rb.x - width_c);
		rb.x = width_c;
	}
	if(rb.y > int(height_c)){
		dim.y -= (rb.y - height_c);
		rb.y = height_c;
	}
	if(pos.x == rb.x || pos.y == rb.y){
//		os_printf("x = %d, right = %d, y = %d, bottom = %d\n", pos.x, rb.x, pos.y, rb.y);
		return;
	}
	ASSERT(pos.y < rb.y)
	ASSERT(pos.x < rb.x)

	ASSERT(rb.y >= 1)

//	os_printf("y = %d, bottom = %d, digits = %d\n", x, bottom, std::numeric_limits<uint8_t>::digits);

	unsigned beginRow = pos.y / std::numeric_limits<uint8_t>::digits;
	unsigned endRow = (rb.y - 1) / std::numeric_limits<uint8_t>::digits;

//	os_printf("beginRow = %d, endRow = %d\n", beginRow, endRow);

	for(unsigned row = beginRow;
			row <= endRow && row < height_c / std::numeric_limits<uint8_t>::digits;
			++row
		)
	{
//		os_printf("row = %d\n", row);
		uint8_t fillByte = 0xff;
		if(row == beginRow){
			fillByte &= (0xff << (pos.y % std::numeric_limits<uint8_t>::digits));
		}
		if(row == endRow){
			fillByte &= (0xff >> (unsigned(-rb.y) % std::numeric_limits<uint8_t>::digits));
		}
		auto startIter = this->beginIter + 1 + row * width_c + pos.x;
		auto finishIter = this->beginIter + 1 + row * width_c + rb.x;
		for(auto i = startIter; i != finishIter; ++i){
			switch (color){
				case Color::WHITE:
					*i |= fillByte;
					break;
				case Color::BLACK:
					*i &= ~fillByte;
					break;
				case Color::INVERSE:
					*i ^= fillByte;
					break;
			}
		}
	}
}
