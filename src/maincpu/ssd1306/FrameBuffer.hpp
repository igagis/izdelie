#pragma once

#include <array>
#include <limits>

#include "../Vec2.hpp"

namespace ssd1306{

enum class ScreenSize{
	SIZE_128x32,
	SIZE_128x64
};

const unsigned width_c = 128;

constexpr unsigned frameBufferSize(ScreenSize s){
	return s == ScreenSize::SIZE_128x32 ?
			width_c * 32 / std::numeric_limits<uint8_t>::digits
		: s == ScreenSize::SIZE_128x64 ?
			width_c * 64 / std::numeric_limits<uint8_t>::digits
		: 0;
}

enum class Color{
	WHITE,
	BLACK,
	INVERSE
};

class FrameBufferBase{
	friend class Display;
	std::array<uint8_t, 1>::iterator beginIter, endIter;
	const unsigned height_c;
protected:
	FrameBufferBase(decltype(beginIter) beginIter, decltype(endIter) endIter) :
			beginIter(beginIter),
			endIter(endIter),
			height_c((endIter - beginIter - 1) * std::numeric_limits<uint8_t>::digits / width_c)
	{
		*this->beginIter = 0x40; // set data mode for SSD1306
	}

	void drawGlyph(char g, Vec2i pos, Color color);
public:
	void text(const char* str, Vec2i pos, Color color = Color::WHITE);
	void text(const char* str, unsigned numChars, Vec2i pos, Color color = Color::WHITE);

	void fillRect(Vec2i pos, Vec2u dim, Color color = Color::WHITE);

	void clear(){
		std::fill(this->beginIter + 1, this->endIter, 0);
	}

	void drawCheckers(uint8_t shift){
		uint8_t byteNum = shift;
		for(auto i = this->beginIter + 1; i != this->endIter; ++i, ++byteNum){
			*i = byteNum;
		}
	}

	static Vec2u glyphSize();

	Vec2u size()const{
		return Vec2u(width_c, this->height_c);
	}
};

template <ScreenSize screenSize> class FrameBuffer : public FrameBufferBase{
	std::array<uint8_t, frameBufferSize(screenSize) + 1> buf;
public:
	FrameBuffer() :
			FrameBufferBase(this->buf.begin(), this->buf.end())
	{}
};

}
