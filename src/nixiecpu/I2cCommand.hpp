#pragma once

enum class I2cCommand : uint8_t{
	UNKNOWN,
	GET_VERSION,
	SET_DISPLAY
};
