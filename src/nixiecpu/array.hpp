#pragma once

#include <stddef.h>

namespace std{

template <typename T, uint16_t S> struct array{
	typedef T value_type;
	typedef value_type* pointer;
	typedef volatile value_type* volatile_pointer;
	typedef const value_type* const_pointer;
	typedef value_type& reference;
	typedef volatile value_type& volatile_reference;
	typedef const value_type& const_reference;
	typedef volatile const value_type& volatile_const_reference;
	typedef value_type* iterator;
	typedef const value_type* const_iterator;
	typedef uint16_t size_type;
	typedef uint16_t difference_type;

	T buffer[S];

	// Iterators.
	iterator begin() noexcept{
		return iterator(data());
	}

	iterator begin() volatile noexcept{
		return iterator(this->data());
	}

	const_iterator begin() const noexcept{
		return const_iterator(data());
	}

	iterator end() noexcept{
		return iterator(data() + S);
	}

	iterator end() volatile noexcept{
		return iterator(this->data() + S);
	}

	const_iterator end() const noexcept{
		return const_iterator(data() + S);
	}

	const_iterator cbegin() const noexcept{
		return const_iterator(data());
	}

	const_iterator cend() const noexcept{
		return const_iterator(data() + S);
	}

	// Capacity.
	constexpr size_type size() const noexcept{
		return S;
	}

	constexpr size_type max_size() const noexcept{
		return S;
	}

	constexpr bool empty() const noexcept{
		return size() == 0;
	}

	// Element access.
	reference operator[](size_type n) noexcept{
		return this->buffer[n];
	}

	volatile_reference operator[](size_type n) volatile noexcept{
		return this->buffer[n];
	}

	constexpr volatile_const_reference operator[](size_type n) volatile const noexcept{
		return this->buffer[n];
	}

	constexpr const_reference operator[](size_type n) const noexcept{
		return this->buffer[n];
	}

	reference at(size_type n){
		return this->operator[](n);
	}

	constexpr const_reference at(size_type n) const{
		return this->operator[](n);
	}

	reference front() noexcept{
		return *this->begin();
	}

	constexpr const_reference front() const noexcept{
		return this->operator[](0);
	}

	reference back() noexcept{
		return S ? *(this->end() - 1) : *this->end();
	}

	constexpr const_reference back() const noexcept{
		return S ? this->operator[](S - 1) : this->operator[](0);
	}

	pointer data() noexcept{
		return this->buffer;
	}

	volatile_pointer data() volatile noexcept{
		return this->buffer;
	}

	const_pointer data() const noexcept{
		return this->buffer;
	}
};

}
