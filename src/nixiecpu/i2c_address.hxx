#pragma once

//NOTE: see https://learn.adafruit.com/i2c-addresses/the-list for list of known adresses
//      to avoid address collision.

static const uint8_t nixieBoardI2cAddr = 0x08;
