#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/wdt.h>
#include <util/twi.h>

#define F_CPU (M_CPU_FREQ)

#include <util/delay.h>

#include "array.hpp"

#include "../maincpu/i2cFrequency.hpp"
#include "i2c_address.hxx"
#include "I2cCommand.hpp"

static const uint8_t boardType = 1; // type 1 = 4 nixies
static const uint8_t boardVersion = 1;

static const uint64_t usPerSec = 1000000;

static const unsigned startupDelayMs = 10;

static const uint64_t cpuFreq = M_CPU_FREQ;

// ATmega8 datasheet requires that i2c frequency is at least 16 times less than CPU frquency
static_assert(i2cFrequency * 16 <= cpuFreq, "i2c frequency is too high, or cpuFreq is too low");

// Number of tube updates per second
static const uint64_t updateFreq = 60; // Hz

// Time for updating all tubes
static const uint64_t updateTime = usPerSec / updateFreq; // us

static const uint64_t numTubes = 4;

// Time for one tube within update
static const uint64_t tubeTime = updateTime / numTubes; // us

// Time needed for tube to ignite
static const uint64_t ignitionTime = 0;  // us

// Time neede for tube to shutdown
static const uint64_t blankingTime = 10; // us

static_assert(ignitionTime + blankingTime <= tubeTime, "ignition and blanking time do not fit into tube time");

// Times in CPU ticks
static const uint64_t tubeTicks = tubeTime * cpuFreq / usPerSec;
static_assert(tubeTicks <= uint16_t(~0), "tubeTicks does not fit in 16 bits");

static const uint64_t ignitionTicks = ignitionTime * cpuFreq / usPerSec;
static const uint64_t blankingTicks = blankingTime * cpuFreq / usPerSec;
static_assert(ignitionTicks + blankingTicks <= tubeTicks, "ignition and blanking ticks do not fit into tube ticks");

static const uint64_t tubeWorkTicks = tubeTicks - ignitionTicks - blankingTicks;

// Key point to set ports to a specific state and hold for specific time
struct KeyFrame{
	uint8_t portB;
	uint8_t portC;
	uint8_t portD;
	uint16_t durationTicks;

	// Set ports of key frame to turn on the tube
	// digit - [0:10], where 10 means no display
	// dots - 0th bit for right dot, 1st bit for left dot
	// tubeNum - tube number, starting from 0
	// workTicks - tube work time
	void setOn(uint8_t digit, uint8_t dots, uint8_t tubeNum, uint16_t workTicks)volatile{
		this->durationTicks = workTicks + ignitionTicks;

		// turn on digit cathode
		if(digit <= 7){
			this->portD = (1 << digit);
			this->portB = 0;
		}else if(digit <= 9){
			this->portB = (1 << (digit - 8));
			this->portD = 0;
		}else{
			// all digit catodes off
			this->portD = 0;
			this->portB = 0;
		}

		// turn on dot cathodes if needed
		this->portB |= (dots << 6);

		// turn on one anode
		this->portC = (1 << tubeNum);
	}

	// blankTicks - blank time within tubeWorkTicks
	// portB - value for PORTB
	// portD - value for PORTD
	// Note, values for PORTB and PORTD are needed to keep cathodes same as they were during 'ON' phase.
	void setOff(uint16_t blankTicks, uint8_t portB, uint8_t portD)volatile{
		this->portC = 0; // all anodes off

		this->portB = portB;
		this->portC = portC;

		this->durationTicks = blankTicks + blankingTicks;
	}
};

// Size of packet transmitted via i2c
static const uint8_t packetSize = 5;

static const uint32_t numKeyFramesPerTube = 2;

// sequence of KeyFrame's
class Sequence : public std::array<KeyFrame, numTubes * numKeyFramesPerTube>{
	// tubeNum - tube number, starting from 0
	// digit - digit to display, values above 9 => display nothing
	// dots - bit 0 for left dot, bit 1 for right dot
	// brightness - digit brightness from [0:15]
	void setDigit(uint8_t tubeNum, uint8_t digit, uint8_t dots, uint8_t brightness)volatile{
		uint16_t onTicks = tubeWorkTicks * (brightness + 1) / 16;
		auto i = this->begin() + tubeNum * numKeyFramesPerTube;
		i->setOn(digit, dots, tubeNum, onTicks);
		(i + 1)->setOff(tubeWorkTicks - onTicks, i->portB, i->portD);
	}
public:

	// Decode and set displayed digits sequence. Input format is as follows:
	// packet[0] = | digit 1 (4 bits) | digit 2 (4 bits) |
	// packet[1] = | digit 3 (4 bits) | digit 4 (4 bits) |
	// packet[2] = | dots 1 (2 bits) | dots 2 (2 bits) | dots 3 (2 bits) | dots 4 (2 bits) |
	// packet[3] = | brightness 1 (4 bits) | brightness 2 (4 bits) |
	// packet[4] = | brightness 3 (4 bits) | brightness 4 (4 bits) |
	void set(volatile const std::array<uint8_t, packetSize> &packet)volatile{
		std::array<uint8_t, 4> digit = {{
			uint8_t(packet[0] >> 4),
			uint8_t(packet[0] & 0xf),
			uint8_t(packet[1] >> 4),
			uint8_t(packet[1] & 0xf)
		}};
		std::array<uint8_t, 4> dots = {{
			uint8_t(packet[2] >> 6),
			uint8_t((packet[2] >> 4) & 0x3),
			uint8_t((packet[2] >> 2) & 0x3),
			uint8_t(packet[2] & 0x3)
		}};

		setDigit(0, digit[0], dots[0], packet[3] >> 4);
		setDigit(1, digit[1], dots[1], packet[3] & 0xf);
		setDigit(2, digit[2], dots[2], packet[4] >> 4);
		setDigit(3, digit[3], dots[3], packet[4] & 0xf);
	}
};

static volatile std::array<Sequence, 2> sequences;

static volatile auto frontSequence = sequences.begin();
static volatile auto backSequence = sequences.begin() + 1;

// This function should only be called when interrupts are disabled!!!
static void setDisplay(volatile const std::array<uint8_t, packetSize> &packet){
	backSequence->set(packet);

	// swap back and front sequences
	auto tmp = backSequence;
	backSequence = frontSequence;
	frontSequence = tmp;
}

static volatile auto curSequenceEnd = frontSequence->end();
static volatile auto curKeyFrame = frontSequence->begin();

// Timer 1 comparison match interrupt handler
ISR(TIMER1_COMPA_vect){
	OCR1A = curKeyFrame->durationTicks; // set new comparison value
	TCNT1 = 0;

	// Here we switch cathodes first, and only then we switch the anode.
	// During switching tube ON the anode was off, we set the cathode and only then turn the anode on.
	// During switching tube OFF the anode is on, the 'off' KeyFrame is supposed to contain same value for cathodes,
	// so setting the cathodes will not change anything, and only then we switch off the anode.
	PORTB = curKeyFrame->portB;
	PORTD = curKeyFrame->portD;
	PORTC = curKeyFrame->portC; // switch anode after all cathodes

	++curKeyFrame;

	if(curKeyFrame == curSequenceEnd){
		curKeyFrame = frontSequence->begin();
		curSequenceEnd = frontSequence->end();
	}
}

static void startTimer1(){
	TCCR1B |= (1 << WGM12); // Set CTC mode (interupt on compare)
	TIMSK |= (1 << OCIE1A); // Enable interrupt on Timer1 compare with OCR1A
	OCR1A = 0; // Set initial compare value (trigger immediately)

	// Set prescaler equal to 1, this starts the timer!
	TCCR1B |= (1 << CS10);
}

static volatile I2cCommand curCommand = I2cCommand::UNKNOWN;

static volatile std::array<uint8_t, packetSize> i2cDisplayBuffer;
static const std::array<uint8_t, 2> i2cVersionBuffer = {{boardType, boardVersion}};

static volatile std::array<uint8_t, 1>::iterator i2cReceiveBufferIter;
static volatile std::array<uint8_t, 1>::const_iterator i2cTransmitIter = i2cVersionBuffer.begin();
static volatile std::array<uint8_t, 1>::const_iterator i2cTransmitEndIter = i2cVersionBuffer.end();

static void initI2cControlRegister(){
	// enable acknowledge, enable TWI, enable TWI interrupts, activate TWINT
	// leave rest of the bits as 0
	TWCR =  (1 << TWEA) | (1 << TWEN) | (1 << TWIE) | (1 << TWINT);
}

// Return true if expect more bytes, false otherwise.
static bool handleI2cCommand(I2cCommand cmd){
	curCommand = cmd;
	switch (cmd){
		case I2cCommand::GET_VERSION:
			i2cTransmitIter = i2cVersionBuffer.begin();
			i2cTransmitEndIter = i2cVersionBuffer.end();
			return false;
		case I2cCommand::SET_DISPLAY:
			i2cReceiveBufferIter = i2cDisplayBuffer.begin();
			return true;
		default:
			curCommand = I2cCommand::UNKNOWN;
			return false;
	}
}

// Return true if expect more bytes, false otherwise.
static bool handleI2cDataReceived(uint8_t d){
	switch (curCommand){
		case I2cCommand::SET_DISPLAY:
			*i2cReceiveBufferIter = d;
			++i2cReceiveBufferIter;
			return i2cReceiveBufferIter != i2cDisplayBuffer.end();
		default:
			return false;
	}
}

static void handleI2cEndOfDataReceiving(){
	switch (curCommand){
		case I2cCommand::SET_DISPLAY:
			setDisplay(i2cDisplayBuffer);
			curCommand = I2cCommand::UNKNOWN;
			break;
		default:
			break;
	}
}

// i2c interrupt vector
ISR(TWI_vect){
	// we are only interested about SR mode (slave receiver)
	switch (TWSR & TW_STATUS_MASK){ // mask out the lower 2 bits (prescaler bits)
		case TW_ST_DATA_NACK:
			initI2cControlRegister();
			// fall-through
		case TW_SR_SLA_ACK: // master initiated transmission to us
			curCommand = I2cCommand::UNKNOWN;
			break;
		case TW_SR_DATA_ACK: // byte received from master
			if(curCommand == I2cCommand::UNKNOWN){
				if(!handleI2cCommand(I2cCommand(TWDR))){
					// clear acknowledge flag as we are not expecting more bytes
					TWCR &= ~(1 << TWEA);
				}
			}else{
				if(!handleI2cDataReceived(TWDR)){
					// clear acknowledge flag as we are not expecting more bytes
					TWCR &= ~(1 << TWEA);
				}
			}
			break;
		case TW_SR_STOP: // end of transmission from master
			handleI2cEndOfDataReceiving();
			// enable acknowledge to wait for next transmission from master
			TWCR |= (1 << TWEA);
			break;
		case TW_ST_SLA_ACK: // master requested data
		case TW_ST_DATA_ACK: // byte transmitted to master
			if(curCommand == I2cCommand::UNKNOWN){
				TWDR = 0;
				break;
			}
			TWDR = *i2cTransmitIter;
			++i2cTransmitIter;
			if(i2cTransmitIter == i2cTransmitEndIter){
				// clear acknowledge flag to indicate this is last byte to transmit
				TWCR &= ~(1 << TWEA);
			}
			break;
		case TW_ST_LAST_DATA:
			TWCR |= (1 << TWEA); // enable acknowledgement after last byte transmitted to master
			curCommand = I2cCommand::UNKNOWN;
			break;
		case TW_BUS_ERROR:
			curCommand = I2cCommand::UNKNOWN;
			TWCR = 0;
			initI2cControlRegister();
			TWCR |= (1 << TWSTO); // according to AVR spec we have to set TWSTO to recover from error state, STOP will not be transmitted.
			break;
		default:
			// ignore the rest of Slave Receiver statuses, because we don't respond to broadcasts and we don't own arbitration
			break;
	}
	TWCR |= (1 << TWINT); // release i2c line
}

static void startI2c(){
	// wait for i2c line to be initialized by main cpu, i.e. wait for both SDA and SCL lines to become high level
	while((PINC & 0x30) != 0x30){
		wdt_reset();
	} // wait for pins 4 and 5 of port C both become high level

	// set slave address
	TWAR = nixieBoardI2cAddr << 1; // set own address, leave lower bit 0, because we don't want to respond to broadcasts.

	// since we are in slave mode, we don't have to set TWBR and prescaler in TWSR

	initI2cControlRegister();
}

static void setupGpioPins(){
	// set all pins of ports A and B as output,
	DDRB = 0xff;
	DDRD = 0xff;
	// set all pins of port C as outputs except pins 4 and 5 which are used for i2c
	DDRC = 0xcf;

	// Clear all outputs to low level.
	PORTB = 0;

	// Since pins 4 and 5 are inputs, this also deactivates pull up resistors for PORTC pins 4 and 5,
	// just in case, because i2c requires open drain pins.
	PORTC = 0;

	PORTD = 0;
}

static void delayMs(unsigned ms){
	const unsigned delayStep = 10;
	for(; ms > delayStep; ms -= delayStep){
		_delay_ms(delayStep);
		wdt_reset();
	}
	_delay_ms(delayStep);
}

int main(){
	setupGpioPins();

	wdt_reset();
	wdt_enable(WDTO_30MS);

	// Delay before starting the nixies multiplexing, to allow high voltage PSU to start up.
	delayMs(startupDelayMs);

	wdt_reset();

	set_sleep_mode(SLEEP_MODE_IDLE);
	sleep_enable();

	// set nothing displayed initially
	std::array<uint8_t, packetSize> initialDisplayData = {
		uint8_t((10 << 4) | 10),
		uint8_t((10 << 4) | 10),
		0x0, // all dots off
		0xff, // max brightness for digits 1 and 2
		0xff  // max brightness for digits 2 and 4
	};

	setDisplay(initialDisplayData); // this is called before enabling interrupts

	sei(); // enable interrupts

	startTimer1();

	startI2c();

	while(true){
		wdt_reset();
		sleep_cpu();
	}

	return 0;
}
