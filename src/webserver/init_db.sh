#!/bin/bash

#we want exit immediately if any command fails and we want error in piped commands to be preserved
set -eo pipefail

#Script for quick deployment of debian package to bintray repo

while [[ $# > 0 ]] ; do
	case $1 in
		--help)
			echo "Usage:"
			echo "	$(basename $0) -p <mysql-root-password>"
			echo " "
			echo "Example:"
			echo "	$(basename $0) -p very_secure_password"
			exit 0
			;;
		-p)
			shift
			rootpw=$1
			shift
			;;
	esac
done

[ -z "$rootpw" ] && echo "Error: please give mysql root password with -p argument" && exit 1;

# Create 'izdelie' database. Ignore error in case database already exists.
mysql -u root -p$rootpw -e "CREATE DATABASE izdelie;" 2> /dev/null || true

# Create 'izdelie' user with password 'parol'. Ignore error in case user already exists.
mysql -u root -p$rootpw -e "CREATE USER 'izdelie'@'localhost' IDENTIFIED BY 'parol';" 2> /dev/null || true

# Grant the new user with all privileges for 'izdelie' database.
mysql -u root -p$rootpw -e "GRANT ALL PRIVILEGES ON izdelie . * TO 'izdelie'@'localhost';"

# Refresh privileges.
mysql -u root -p$rootpw -e "FLUSH PRIVILEGES;"

# Create tables. Ignore error if table already exists.
mysql -u izdelie -pparol -e "create table izdelie.izdelie ( mac varchar(17) not null, nickname varchar(32), description varchar(255), firmware_version int, uptime_sec int, country varchar(32), city varchar(32), timezone varchar(32), primary key (mac), unique(nickname) );" 2> /dev/null || true

# Add later added columns to the table (table may already be existing, so we do it separately here).
mysql -u izdelie -pparol -e "alter table izdelie.izdelie add first_report varchar(19);" 2> /dev/null || true
mysql -u izdelie -pparol -e "alter table izdelie.izdelie add last_report varchar(19);" 2> /dev/null || true
mysql -u izdelie -pparol -e "alter table izdelie.izdelie add max_uptime_sec int;" 2> /dev/null || true
