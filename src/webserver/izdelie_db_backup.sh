#!/bin/bash

git clone git@gitlab.com:igagis/izdelie_db_backup.git ~/izdelie_db_tmp

cd ~/izdelie_db_tmp

mysqldump -u izdelie -pparol izdelie > izdelie_db.sql

git add izdelie_db.sql

git diff-index --quiet HEAD --

if [ "$?" != "0" ]; then
	git commit -m"backup from $(date)"
	git push
else
	echo "Nothing to commit"
fi

cd ~

rm -rf izdelie_db_tmp
