var http = require('http');
var url = require('url');
var fs = require('fs');
var minimist = require('minimist');
var mysql = require('mysql');

var args = minimist(process.argv.slice(2), {
    alias: {
        h: 'help'
    }
});

if(args.help){
	console.log("izdelie web server");
	console.log("Arguments:");
	console.log("\t-d    root directory of the web server");
	process.exit(0);
}

if(args.d == null){
	console.log("Error: please specify root directory for web content with -d option.")
	process.exit(1);
}
console.log("Web content root directory = " + args.d);


// Connect to MySQL database.
var mysq_connection = mysql.createConnection({
	host: "localhost",
	user: "izdelie",
	password: "parol",
	database: "izdelie",
	multipleStatements: true
});

mysq_connection.connect(function(err) {
	if (err) throw err;
	console.log("MySQL connected!");
});


function get_ext(path){
	var i = path.lastIndexOf('.');
	return (i < 0) ? '' : path.substr(i);
}

function get_formatted_date() {
    var date = new Date();

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    return year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec;

}

function ext_to_mime_type(ext){
	switch(ext){
		case ".bin":
			return 'application/octet-stream';
		case ".pdf":
			return 'application/pdf';
		case ".html":
			return 'text/html';
		default:
		case "":
		case ".json":
			return 'application/json';
	}
}

var server = http.createServer(function (req, res) {
	var q = url.parse(req.url, true);

	if (req.method == 'POST') {
		var body = '';
        req.on('data', function (data) {
            body += data;

            // Too much POST data, kill the connection!
            if (body.length > 4096){
				req.connection.destroy();
			}
        });

        req.on('end', function () {
			try{
				var post = JSON.parse(body);
				console.log("post = " + JSON.stringify(post));
				console.log("q.pathname = " + q.pathname);
				if(q.pathname.startsWith("/api/")){
					if(q.pathname === "/api/report"){
						if(post['mac'] != null){
							var date_str = get_formatted_date();

							// Insert new row or update existing one.
							var sql = "insert into izdelie (mac, firmware_version, uptime_sec, max_uptime_sec, country, city, timezone, first_report, last_report) values("
										+ "\"" + post['mac'] + "\","
										+ "\"" + post['firmware_version'] + "\","
										+ "\"" + post['uptime_sec'] + "\","
										+ "\"" + 0 + "\","
										+ "\"" + post['country'] + "\","
										+ "\"" + post['city'] + "\","
										+ "\"" + post['timezone'] + "\","
										+ "\"" + date_str + "\","
										+ "\"" + date_str + "\""
									+ ") on duplicate key update "
										+ "firmware_version=\"" + post['firmware_version'] + "\","
										+ "uptime_sec=\"" + post['uptime_sec'] + "\","
										+ "country=\"" + post['country'] + "\","
										+ "city=\"" + post['city'] + "\","
										+ "timezone=\"" + post['timezone'] + "\","
										+ "last_report=\"" + date_str + "\""
									+ ";"
										+ "update izdelie set max_uptime_sec=\"" + post['uptime_sec'] + "\" where mac=\"" + post['mac'] + "\" and max_uptime_sec<" + post['uptime_sec']
									+ ";"
								;
							mysq_connection.query(sql, function (err, result) {
								if (err){
									console.log("ERROR: sql request failed: " + sql);
									res.writeHead(500, {'Content-Type': 'text/html'});
									res.end("500 Internal Server Error");
									throw err;
								}
								res.writeHead(200, {'Content-Type': 'text/html'});
								res.end("200 OK");
							});
						}else{
							console.log("ERROR: malformed POST request to /api/report: " + body);
							res.writeHead(400, {'Content-Type': 'text/html'});
							res.end("400 Bad Request");
						}
					}
				}
			}catch(e){
				if(e instanceof SyntaxError){
					res.writeHead(400, {'Content-Type': 'text/html'});
					res.end("400 Bad Request");
				}else{
					res.writeHead(500, {'Content-Type': 'text/html'});
					res.end("500 Internal Server Error");
				}
			}
        });
	}else{
		if(q.pathname.startsWith("/api/")){
			res.writeHead(404, {'Content-Type': 'text/html'});
			res.end("404 Not Found");
		}else{
			if(q.pathname === '/'){
				q.pathname += "index.html";
			}
			var filename = args.d + q.pathname;
			fs.readFile(filename, function(err, data) {
				if (err) {
					console.log("request: " + filename + " not found");
					res.writeHead(404, {'Content-Type': 'text/html'});
					return res.end("404 Not Found");
				}
				var ext = get_ext(q.pathname).toLowerCase();
				console.log(`ext = ${ext}`);

				res.writeHead(
						200,
						{
							'Content-Type': ext_to_mime_type(ext),
							'Content-Length': data.length
						}
					);
				
				res.write(data);
				return res.end();
			});
		}
	}
});

server.listen(3000);