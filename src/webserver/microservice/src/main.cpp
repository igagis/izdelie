#include <iostream>
#include <mutex>
#include <sstream>
#include <iomanip>

#include <utki/util.hpp>

#include <mariadb++/connection.hpp>

#include <pautina/server.hpp>

#include <jsondom/dom.hpp>

int main(int argc, const char** argv){
	std::cout << "Hello izdelie!" << std::endl;

	uint16_t port = 3000;

	// open connection to database
	mariadb::account_ref db_account_setup = mariadb::account::create(
		"127.0.0.1", // host
		"izdelie", // username
		"parol", // password
        "izdelie" // database name
	);

	if(!db_account_setup){
		throw std::runtime_error("could not create database account setup");
	}

 	mariadb::connection_ref dbconn = mariadb::connection::create(db_account_setup);
	if(!dbconn){
		throw std::runtime_error("could not create database connection");
	}

	if(!dbconn->connect()){
		throw std::runtime_error("could not connect to database");
	}
	utki::scope_exit db_conn_scope_exit([&dbconn](){
		dbconn->disconnect();
	});

	std::mutex dbconn_mutex;

	pautina::server server(
		pautina::server::configuration{{.port = port}},
		{
			{
				{"api", "report"},
				[&dbconn, &dbconn_mutex](const httpmodel::request& request, utki::span<const std::string> subpath){
					if(!subpath.empty()){
						return httpmodel::response{request, httpmodel::status::http_404_not_found};
					}

					std::cout << "/api/report" << std::endl;
					httpmodel::response resp(request);

					if(request.method != httpmodel::method::post){
						resp.status = httpmodel::status::http_404_not_found;
						return resp;
					}

					auto json = jsondom::read(request.body);

					std::cout << "body json = " << json.to_string() << std::endl;

					std::string mac, city, country, timezone;
					uint32_t uptime_sec, firmware_version;

					try{
						mac = json.object().at("mac").string();
						city = json.object().at("city").string();
						country = json.object().at("country").string();
						timezone = json.object().at("timezone").string();
						uptime_sec = json.object().at("uptime_sec").number().to_uint32();
						firmware_version = json.object().at("firmware_version").number().to_uint32();
					}catch(std::logic_error& e){
						resp.status = httpmodel::status::http_400_bad_request;
						return resp;
					}
					
					auto cur_time = std::time(nullptr);
					std::stringstream time_ss;
					time_ss << std::put_time(std::gmtime(&cur_time), "%Y-%m-%d %H-%M-%S");
					std::string date_str = time_ss.str();

					std::stringstream ss;

					ss << "insert into izdelie ("
						"mac, firmware_version, uptime_sec, max_uptime_sec, country, city, timezone, first_report, last_report"
					") values(";

					ss << '"' << mac << "\",";
					ss << '"' << firmware_version << "\",";
					ss << '"' << uptime_sec << "\",";
					ss << '"' << 0 << "\",";
					ss << '"' << country << "\",";
					ss << '"' << city << "\",";
					ss << '"' << timezone << "\",";
					ss << '"' << date_str << "\",";
					ss << '"' << date_str << "\"";
					ss << ") on duplicate key update ";
					ss << "firmware_version=\"" << firmware_version << "\",";
					ss << "uptime_sec=\"" << uptime_sec << "\",";
					ss << "country=\"" << country << "\",";
					ss << "city=\"" << city << "\",";
					ss << "timezone=\"" << timezone << "\",";
					ss << "last_report=\"" << date_str << "\"";
					ss << ";";
					ss << "update izdelie set max_uptime_sec=\"" << uptime_sec << "\" where mac=\"" << mac << "\" and max_uptime_sec<" << uptime_sec <<";";

					{
						std::lock_guard mutex_lock(dbconn_mutex);

						auto res = dbconn->execute(ss.str());

						if(res == 0){
							resp.status = httpmodel::status::http_500_internal_server_error;
							return resp;
						}
					}

					resp.status = httpmodel::status::http_200_ok;
					return resp;
				}
			}
		}
	);

	std::cout << "server is listening on port " << port << std::endl;

	server.run();

	std::cout << "exit" << std::endl;

	return 0;
}
